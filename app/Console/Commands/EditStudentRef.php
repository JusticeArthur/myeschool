<?php

namespace App\Console\Commands;

use App\Models\Student;
use Illuminate\Console\Command;
use Webpatser\Uuid\Uuid;

class EditStudentRef extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'students:ref';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates random student ref no';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $students = Student::all();
        foreach ($students as $student){
            $student->ref_no = (string)Uuid::generate();
            $student->save();
        }
    }
}
