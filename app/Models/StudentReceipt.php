<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentReceipt extends Model
{
    public function setReceiptNoAttribute($value){
        $this->attributes['receipt_no'] = strtoupper($value);
    }
}
