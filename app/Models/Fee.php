<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    protected $with = ['term','academicYear','item','class'];
    public function term(){
        return $this->belongsTo(Term::class);
    }
    public function academicYear(){
        return $this->belongsTo(AcademicYear::class);
    }
    public function item(){
        return $this->belongsTo(Item::class);
    }
    public function class(){
        return $this->belongsTo(NewClass::class,'class_id');
    }
}
