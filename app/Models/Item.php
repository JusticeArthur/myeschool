<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function getAmountAttribute($value){
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        if(isset($settings['currency'])){
            return $settings->currency. ' '.number_format($value,2);
        }
        return number_format($value,2);
    }
    public function getNameAttribute($value){
        return strtoupper($value);
    }
    public function term(){
        return $this->belongsTo(Term::class);
    }
    public function academicYear(){
        return $this->belongsTo(AcademicYear::class);
    }
    public function getValueAttribute(){
        $arr = explode(' ',$this->amount);
        if(isset($arr[1]))
            return $arr[1];
        return $this->amount;
    }
    protected $with = ['term','academicYear'];
    protected $appends = ['value'];
}
