<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class StudentWallet extends Model
{
    use SearchableTrait;
   /* public function getBalanceAttribute($value){
        return number_format($value,2);
    }*/
    protected $searchable = [
        'columns' => [
            'student_wallets.balance' => 10,
            'students.fname' => 10,
            'students.lname' => 10,
            'students.mname'=>10,
            'students.email'=>10,
            'students.phone'=>10,
            'students.ref_no'=>10,
            'students.religion'=>8,
            'students.school_id'=>1,
        ],
        'joins' => [
            'students' => ['student_wallets.student_id','students.id'],
        ],
    ];
    public function getOwingAttribute(){
        if(isset($this->balance))
        if((float)$this->balance >= 0){
            return false;
        }else{
            return true;
        }
    }
    public function getAmountAttribute(){
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        if(isset($settings)){
            if((float)$this->balance < 0 )
                return optional($settings)->currency . ' '. (number_format((float)$this->balance * -1,2));
            return optional($settings)->currency .' '. number_format((float)$this->balance,2);
        }
        return number_format((float)$this->balance,2);
    }
    public function student(){
        return $this->belongsTo(Student::class)->where('school_id',auth()->user()->school_id);
    }
    protected $appends = ['owing','amount'];
}
