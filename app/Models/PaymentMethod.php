<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name'] = strtoupper($value);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    protected $with = ['user'];
}
