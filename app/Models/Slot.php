<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }
}
