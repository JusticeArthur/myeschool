<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Transaction extends Model
{
    public function paymentMethod(){
        return $this->belongsTo(PaymentMethod::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function term(){
        return $this->belongsTo(Term::class);
    }
    public function academicYear(){
        return$this->belongsTo(AcademicYear::class);
    }
    public function student(){
        return $this->belongsTo(Student::class);
    }
    public function receipt(){
        return $this->belongsTo(StudentReceipt::class);
    }
    public function getAmountAttribute($value){
        $currency = InstitutionSetting::where('school_id',auth()->user()->school_id)->first()->currency;
        if(isset($currency)){
            return $currency .' '.number_format($value,2);
        }
        return number_format($value,2);
    }
    public function getDateAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
    protected $with = ['paymentMethod','user','term','academicYear','student','receipt'];


}
