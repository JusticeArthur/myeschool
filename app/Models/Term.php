<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $with = ['year'];
    public function year(){
        return $this->belongsTo(AcademicYear::class,'academic_year_id');
    }
    public function getNameAttribute(){
        return strtoupper($this->display_name);
    }
    protected $appends = ['name'];
    public function getDisplayNameAttribute($value){
        return strtoupper($value);
    }
    public function getStartDateAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
    public function getEndDateAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
    public function getPromotableAttribute($value){
        if($value)
            return true;
        return false;
    }
}
