<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class NewParent extends Model
{
    use SearchableTrait;
    protected $table = 'parents';
    protected $with = ['wards'];
    protected $fillable = ['school_id','lname','fname','mname','title','gender','email','contact','country_id',
        'state_id','city','address','temp_relation','occupation','pick'];

    protected  $searchable  = [
        'columns' => [
            'fname' => 10,
            'lname' => 10,
            'contact'=>10,
            'mname' => 2,
            'school_id'=>1,
        ],
    ];
    public function wards(){
        return $this
            ->belongsToMany(Student::class,
                'student_guardians','parent_id','student_id');
    }
    public function getNameAttribute(){
        return title_case($this->lname). ' '.title_case($this->mname).' '.title_case($this->fname);
    }
    public function getOnAttribute(){
        return true;
    }
    public function getTitleAttribute($value){
        return title_case($value);
    }
    public function getCityAttribute($value){
        return title_case($value);
    }
    protected $appends = [
      'on','name'
    ];
    public function getPickAttribute($value){
        if($value)
            return true;
        return false;
    }

}
