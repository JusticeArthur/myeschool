<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function getScoreAttribute(){
        return number_format($this->class_score + $this->exam_score,2);
    }
    public function getPositionAttribute(){
        return 0;
    }
    protected $appends = ['score','position','final_score','final_position'];
    public function getExamScoreAttribute($value){
        return number_format($value,2);
    }
    public function getClassScoreAttribute($value){
        return number_format($value,2);
    }
    public function getFinalScoreAttribute(){
        return 0;
    }
    public function getFinalPositionAttribute(){
        return 0;
    }
    public function student(){
        return $this->belongsTo(Student::class);
    }

}
