<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstitutionSetting extends Model
{
    protected $guarded = [];
    /*protected $fillable = ['name','abbr','contact_phone','contact_email','settlement_name','slogan','currency','logo','country',
        'website','school_id','address','assessment','examination'];*/
    public function getLogoAttribute($value){
        if (isset($value))
        return asset('storage/avatars/'.$value);
        return $value;
    }
    public function getNameAttribute($value){
        if(isset($value)){
            return strtoupper($value);
        }
        return $value;
    }
}
