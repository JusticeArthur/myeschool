<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewClass extends Model
{
    protected $table = 'classes';
    protected $with = ['teacher'];
    public function users()
    {
        return $this->belongsToMany(Course::class,'class_courses');
    }
    public function school(){
        return $this->belongsTo(School::class);
    }
    public function getNameAttribute($value){
        return title_case($value);
    }
    public function parents(){
        return $this->hasOne(NewParent::class);
    }
    public function students(){
        return $this->hasMany(Student::class,'class_id');
    }
    public function teacher(){
        return $this->belongsTo(Teacher::class);
    }
    public function courses(){
        return $this->hasMany(ClassCourse::class,'class_id');
    }

}
