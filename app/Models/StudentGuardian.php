<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentGuardian extends Model
{
    public function getRelationAttribute($value){
        return title_case($value);
    }
}
