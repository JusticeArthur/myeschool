<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsBill extends Model
{
    public function getCostAttribute(){
        return number_format((float)$this->amount,2);
    }
    protected $appends = ['cost'];
}
