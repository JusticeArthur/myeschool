<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Remark extends Model
{
    protected $appends = ['range'];
    public function setGradeAttribute($value){
        $this->attributes['grade'] = strtoupper($value);
    }
    public function setRemarkAttribute($value){
        $this->attributes['remark'] = strtoupper($value);
    }
    public function getRangeAttribute($value){
        return $this->start . ' - '.$this->end;
    }
}
