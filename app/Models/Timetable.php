<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{
    protected $with = ['slot','course','teacher','class'];
    public function getDayAttribute($value){
        return ucfirst($value);
    }

    public function slot(){
        return $this->belongsTo(Slot::class);
    }
    public function course(){
        return $this->belongsTo(Course::class);
    }
    public function teacher(){
        return $this->belongsTo(Teacher::class);
    }
    public function class(){
        return $this->belongsTo(NewClass::class);
    }
}
