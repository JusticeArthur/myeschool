<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $guarded = [];
    protected $with = ['category','user'];
    public function category (){
        return $this->belongsTo(ExpenseCategory::class,'expense_category_id');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getDateAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
    public function getTotalAttribute($value){
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        if(isset($settings))
            return $settings->currency. ' ' . number_format($value,2);
        return number_format($value,2);
    }
    public function getAmountAttribute($value){
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        if (isset($settings))
            return $settings->currency. ' ' . number_format($value,2);
        return number_format($value,2);
    }
}
