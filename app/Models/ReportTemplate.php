<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportTemplate extends Model
{
    public function getAvatarAttribute($value){
        return asset('templates/'.$value);
    }
    public function getNameAttribute($value){
        return title_case($value);
    }
    public function setNameAttribute($value){
        $this->attributes['name'] = strtolower($value);
    }

}
