<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamApproval extends Model
{
    public function academicYear(){
        return $this->belongsTo(AcademicYear::class);
    }
    public function term(){
        return $this->belongsTo(Term::class);
    }
}
