<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class IncomeCategory extends Model
{
    public function setNameAttribute($value){
        $this->attributes['name'] = title_case($value);
    }
    protected $guarded = [];
    public function user(){
        return $this->belongsTo(User::class);
    }
    protected $with = ['user'];
    public function getCategoryAttribute($value){
        return title_case($value);
    }
}
