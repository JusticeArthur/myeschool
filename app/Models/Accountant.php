<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accountant extends Model
{
    public function getFullNameAttribute(){
        return $this->lname. ' '.$this->mname.' '.$this->fname;
    }
    protected $appends = ['full_name'];
}
