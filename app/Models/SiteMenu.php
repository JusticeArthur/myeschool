<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class SiteMenu extends Model
{
    use SearchableTrait;
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'site_menus.name' => 10,
            'site_sub_menus.name' => 10,
        ],
        'joins' => [
            'site_sub_menus' => ['site_menus.id','site_sub_menus.site_menu_id'],
        ],
    ];
    public function getSubMenuAttribute(){
        return false;
    }
    protected $appends = ['sub_menu'];
    protected $hidden = ['created_at','updated_at'];
}
