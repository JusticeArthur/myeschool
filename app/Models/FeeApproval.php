<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FeeApproval extends Model
{
    protected $with = ['user','class','term','academicYear'];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function class(){
        return $this->belongsTo(NewClass::class,'class_id');
    }
    public function term(){
        return $this->belongsTo(Term::class);
    }
    public function academicYear(){
        return$this->belongsTo(AcademicYear::class);
    }
}
