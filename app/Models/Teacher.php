<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $appends = ['full_name'];
    public function courses(){
        return $this->hasMany(Course::class);
    }
    public function classes(){
        return $this->belongsToMany(NewClass::class,'class_courses','class_id','teacher_id');
        //return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }
    public function classTeacher(){
        return $this->hasOne(NewClass::class);
    }

    public function getFullNameAttribute(){
        return $this->fname . ' '.$this->mname . ' '.$this->lname;
    }
}
