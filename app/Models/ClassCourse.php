<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassCourse extends Model
{
    protected $with = ['teacher','course','class'];
    public function teacher(){
        return $this->belongsTo(Teacher::class);
    }
    public function course(){
        return $this->belongsTo(Course::class);
    }
    public function class(){
        return $this->belongsTo(NewClass::class,'class_id');
    }
}
