<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    use Notifiable;
    use SearchableTrait;
    protected  $searchable  = [
        'columns' => [
            'fname' => 10,
            'lname' => 10,
            'mname'=>10,
            'email'=>10,
            'phone'=>10,
            'ref_no'=>10,
            'religion'=>8,
            'school_id'=>1,
        ],
    ];
    protected $guard = "students";
    protected $appends = ['name','on','contact'];
    protected $with = ['studentClass','state','country'];

    public function guardians(){
        return $this->belongsToMany(NewParent::class,'student_guardians','student_id','parent_id')
            ->withPivot('relation','id');
    }

    public function region(){
        return $this->belongsTo(Religion::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function studentClass(){
        return $this->belongsTo(NewClass::class,'class_id');
    }
    protected $guarded = [];

    public function getNameAttribute(){
        return $this->lname. ' '.$this->mname.' '.$this->fname;
    }
    /*public function setFullNameAttribute(){
        $this->attributes['full_name'] = $this->lname. ' '.$this->mname.' '.$this->fname;
    }*/
    public function getContactAttribute(){
        return $this->phone;
    }
    public function getOnAttribute(){
        return true;
    }
    public function getAdmissionDateAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
    public function getBirthDateAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function state(){
        return $this->belongsTo(State::class);
    }
    public function transactions(){
        return $this->hasMany(Transaction::class);
    }
    public function wallet(){
        return $this->hasOne(StudentWallet::class);
    }
}
