<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class StudentBill extends Model
{
    protected $with = ['user','term','academicYear','class'];
    protected $appends = ['formatted_amount','bill_date'];
    public function term(){
        return $this->belongsTo(Term::class);
    }
    public function academicYear(){
        return $this->belongsTo(AcademicYear::class);
    }
    public function class(){
        return $this->belongsTo(NewClass::class,'class_id');
    }
    public function getFormattedAmountAttribute(){
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        if(isset($settings['currency'])){
            return $settings->currency. ' '.number_format($this->amount,2);
        }
        return number_format($this->amount,2);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getBillDateAttribute(){
        return Carbon::parse($this->created_at)->format('D, jS M Y');
    }
}
