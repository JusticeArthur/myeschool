<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Nicolaslopezj\Searchable\SearchableTrait;
class SiteSubMenu extends Model
{
    use SearchableTrait;
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'name' => 10,
            // 'site_sub_menus.name' => 10,
        ],
        /*'joins' => [
            'site_sub_menus' => ['site_menus.id','site_sub_menus.site_menu_id'],
        ],*/
    ];
    protected $hidden = ['created_at','updated_at'];
    public function menu(){
        return $this->belongsTo(SiteMenu::class,'site_menu_id');
    }
    protected $with = ['menu'];
    public function getSubMenuAttribute(){
        return true;
    }
    protected $appends = ['sub_menu'];
}
