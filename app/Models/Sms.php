<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $with = ['user'];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getTextAttribute($value){
        $length = 40;
        $formatted = $value;
        if(strlen($value) > $length){
            $formatted = substr($value,0,$length) . '...';
        }
        return $formatted;
    }
    public function getDataAttribute(){
        return $this->text;
    }
    protected $appends = ['data'];
}
