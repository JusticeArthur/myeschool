<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Role;
class School extends Model
{
    public function roles(){
    	return $this->hasMany(Role::class);
    }
    public function generalSettings(){
        return $this->hasOne(GeneralSetting::class);
    }
}
