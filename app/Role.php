<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;


class Role extends EntrustRole
{
    public function setNameAttribute($value){
        $this->attributes['name'] = strtolower($value);
    }
    public function setDisplayNameAttribute($value){
        $this->attributes['display_name'] = title_case($value);
    }
    public function getNameAttribute($value){
        return strtolower($value);
    }
    public function getDisplayNameAttribute($value){
        return title_case($value);
    }
}
