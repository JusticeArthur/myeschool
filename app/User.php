<?php

namespace App;

use App\Models\AcademicInfo;
use App\Models\Accountant;
use App\Models\Country;
use App\Models\GeneralSetting;
use App\Models\School;
use App\Models\State;
use App\Models\Teacher;
use App\Models\VerifyUser;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function school(){
        return $this->belongsTo(School::class);
    }
    public function isSuper(){
        $role =Role::where('name','super')->first();
        return Auth::user()->role_id == $role->id;
    }
    public function isAdmin(){
        $role =Role::where('school_id',\auth()->user()->school_id)
            ->where('name','admin')->first();
        return Auth::user()->role_id == $role->id;
    }
    public function isStudent(){
        $role =Role::where('school_id',\auth()->user()->school_id)
            ->where('name','student')->first();
        return Auth::user()->role_id == $role->id;
    }
    public function isTeacher(){
        $role =Role::where('school_id',\auth()->user()->school_id)
            ->where('name','teacher')->first();
        return Auth::user()->role_id == $role->id;
    }
    public function isAccountant(){
        $role =Role::where('school_id',\auth()->user()->school_id)
            ->where('name','accountant')->first();
        return Auth::user()->role_id == $role->id;
    }
    public function isGuardian(){
        $role =Role::where('school_id',\auth()->user()->school_id)
            ->where('name','guardian')->first();
        return Auth::user()->role_id == $role->id;
    }
    public function verifyUser(){
        return $this->hasOne(VerifyUser::class);
    }
    //teacher relationships
    public function teacher(){
        return $this->hasOne(Teacher::class);
    }
    public function accountant(){
        return $this->hasOne(Accountant::class);
    }
    protected $with = ['teacher'];
    protected $appends = ['on','fname','lname'];
    public function getOnAttribute(){
        return true;
    }
    public function getFnameAttribute(){
        $name = explode(' ',$this->name);
        if(isset($name[0]))
            return $name[0];
        return '';
    }
    public function getLnameAttribute(){
        $name = explode(' ',$this->name);
        if(isset($name[1]))
            return $name[1];
        return '';
    }
    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function state(){
        return $this->belongsTo(State::class);
    }
    public function getRoleIdAttribute($value){
        return (int) $value;
    }
    public function academicInfo(){
        return $this->hasOne(AcademicInfo::class);
    }
    public function getDobAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }

}
