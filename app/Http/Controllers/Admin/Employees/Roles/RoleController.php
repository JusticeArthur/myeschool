<?php

namespace App\Http\Controllers\Admin\Employees\Roles;

use App\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::where('school_id',auth()->user()->school_id)
            ->where('name','<>','guardian')
            ->orderBy('created_at','desc')
            ->get();
        return view('admin.employess.roles.roles',[
            'roles'=>$role
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'display_name'=>'required'
        ];
        $this->validate($request,$rules);
        $role = new Role();
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->school_id = auth()->user()->school_id;
        try{
            $role->save();
        }catch (QueryException $q){
            return response()->json([
                'status'=>'duplicate'
            ],501);
        }catch (\Exception $e){
            return response()->json([
                'status'=>'error'
            ],500);
        }
        return response()->json([
            'status'=>'success',
            'role'=>$role
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'=>'required',
            'display_name'=>'required'
        ];
        $this->validate($request,$rules);
        try{
            Role::where('id',$id)->update([
                'name'=>$request->name,
                'display_name'=>$request->display_name
            ]);
        }catch (QueryException $q){
            return response()->json([
                'status'=>'duplicate'
            ],501);
        }catch (\Exception $e){
            return response()->json([
                'status'=>'error'
            ],500);
        }
        return response()->json([
            'status'=>'success',
            'role'=>Role::find($id)
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::find($id)->delete();
        return response()->json([
            'status'=>'success'
        ],200);
    }
}
