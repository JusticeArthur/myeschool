<?php

namespace App\Http\Controllers\Admin\Employees;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(){
        return view('admin.employess.index');
    }
}
