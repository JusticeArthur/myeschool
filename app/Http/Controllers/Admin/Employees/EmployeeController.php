<?php

namespace App\Http\Controllers\Admin\Employees;

use App\Http\Requests\EmployeeForm;
use App\Models\AcademicInfo;
use App\Models\Country;
use App\Models\Employee;
use App\Models\Teacher;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = User::with(['role:id,display_name','country:id,name','state:id,name'])
            ->where('school_id',auth()->user()->school_id)
            ->orderBy('created_at','desc')
            ->get();

        return view('admin.employess.all',[
            'employees'=>$employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('school_id',auth()->user()->school_id)
            ->where('name','<>','guardian')
            ->get();
        $countries = Country::all();
        return view('admin.employess.add',[
            'countries'=>$countries,
            'roles'=>$roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeForm $request)
    {
        $role = Role::find($request->role_id);

        //create user
        $user = new User();
        //insert users
        $user->name = $request['fname'].' '.$request['lname'] . ' '.$request->mname;
        $user->email = $request['email'];
        $user->password = bcrypt('password');
        $user->school_id = auth()->user()->school_id;
        $user->role_id = $role->id;
        $user->mname = $request->mname;
        $user->lname = $request->lname;
        $user->fname = $request->fname;
        $user->gender = $request->gender;
        $user->alt_phone = $request->alt_phone;
        $user->religion = $request['religion'];
        $user->dob = Carbon::parse($request->dob)->toDateString();
        $user->verified = 1;
        $user->slug = str_replace('-','',(string) Uuid::generate());
        $user->country_id = $request->country_id;
        $user->state_id = $request->state_id;
        $user->contact = $request['phone'];
        $user->save();

        //academic details
        $academics = new AcademicInfo();
        $academics->degree = $request['degree'];
        $academics->university = $request->uni;
        $academics->user_id = $user->id;
        $academics->year = $request->uni_year;
        $academics->gpa = 0;
        $academics->save();

        if(strtolower($role->name) === 'teacher'){
            $teacher = new Teacher();
            $teacher->fname = $request->fname;
            $teacher->mname = $request->mname;
            $teacher->lname = $request->lname;
            $teacher->dob = Carbon::parse($request->dob)->toDateString();
            $teacher->user_id = $user->id;
            $teacher->email = $request->email;
            $teacher->gender = $request->gender;
            $teacher->contact = $request->alt_phone;
            $teacher->academic_info_id = $academics->id;
            $teacher->school_id = auth()->user()->school_id;
            $teacher->religion = $request['religion'];
            $teacher->save();
        }else{
            $employee = new Employee();
            $employee->fname = $request->fname;
            $employee->mname = $request->mname;
            $employee->lname = $request->lname;
            $employee->dob = Carbon::parse($request->dob)->toDateString();
            $employee->user_id = $user->id;
            $employee->email = $request->email;
            $employee->gender = $request->gender;
            $employee->contact = $request->alt_phone;
            $employee->academic_info_id = $academics->id;
            $employee->school_id = auth()->user()->school_id;
            $employee->religion = $request['religion'];
            $employee->save();
        }
        //attach role
        $user->attachRole($role);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with(['role:id,display_name','country:id,name','state:id,name'])
            ->where('slug',$id)->first();
        //dd($user->toArray());
        $role = DB::table('role_user')->where('role_user.user_id',$user->id)
            ->join('roles','roles.id','role_user.role_id')
            ->select('roles.id','roles.name','roles.display_name')
            ->get();
        $roles = Role::where('school_id',auth()->user()->school_id)
            ->where('name','<>','guardian')
            ->where('name','<>','student')
            ->get();
        return view('admin.employess.profile',[
            'user'=>$user,
            'roles'=>$roles,
            'assigned_roles'=>$role
        ]);
    }

    public function unassignRole($user_id,$role_id){
        DB::table('role_user')->where('user_id',$user_id)
            ->where('role_id',$role_id)
            ->delete();
        return response()->json([
            'status'=>'success',
            'data'=>DB::table('role_user')->where('role_user.user_id',$user_id)
                ->join('roles','roles.id','role_user.role_id')
                ->select('roles.id','roles.name','roles.display_name')
                ->get()

        ],200);
    }
    public function assignRole($user_id,$role_id){
        $user = User::where('id',$user_id)->first();
        $user->attachRole($role_id);
        return response()->json([
            'status'=>'success',
            'data'=>DB::table('role_user')->where('role_user.user_id',$user_id)
                ->join('roles','roles.id','role_user.role_id')
                ->select('roles.id','roles.name','roles.display_name')
                ->get()
        ],200);
    }
    public function changePassword(Request $request){
        $student = User::where('id',$request->id)->first();
        $rules = [
            'password'=>'min:6|confirmed',
            'current'=>[
                function($attribute, $value, $fail) use ($request,$student){
                    if(isset($value))
                        if(!Hash::check($value,$student->password)){
                            return $fail('The Current Password is Incorrect');
                        }

                }
            ],
        ];
        $this->validate($request,$rules);
        User::where('id',$request->id)->update([
            'password'=>bcrypt($request->password)
        ]);
        return response()->json([
            'message','success'
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('academicInfo')->where('slug',$id)->first();
        /*dd($user->toArray());*/
        $roles = Role::where('school_id',auth()->user()->school_id)
            ->where('name','<>','guardian')
            ->get();
        $countries = Country::all();
        return view('admin.employess.edit',[
            'countries'=>$countries,
            'roles'=>$roles,
            'user'=>$user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update user info
        User::where('id',$id)->update([
            'mname'=>$request->mname,
            'lname'=>$request->lname,
            'fname'=>$request->fname,
            'name'=>$request['fname'].' '.$request['lname'] . ' '.$request->mname,
            'dob'=>Carbon::parse($request->dob)->toDateString(),
            'state_id'=>$request->state_id,
            'country_id'=>$request->country_id,
            'alt_phone'=>$request->alt_phone,
            'contact'=>$request->contact,
            'religion'=>$request->religion,
            'gender'=>$request->gender,
        ]);
        //update academic info
        //first check for academic info
        $check = AcademicInfo::where('user_id',$id)->first();
        if(isset($check)){
            AcademicInfo::where('id',$request->academic_info['id'])
                ->update([
                    'degree'=>$request->academic_info['degree'],
                    'university'=>$request->academic_info['university'],
                    'year'=>$request->academic_info['year'],
                ]);
        }else{
            $academic = new AcademicInfo();
            $academic->degree =  $request->academic_info['degree'];
            $academic->user_id = $id;
            $academic->university = $request->academic_info['university'];
            $academic->year = $request->academic_info['year'];
            $academic->save();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
