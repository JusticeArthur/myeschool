<?php

namespace App\Http\Controllers\Admin;

use App\Models\Remark;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RemarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $remarks = Remark::where('school_id',auth()->user()->school_id)
            ->orderby('grade','asc')
            ->get();
        return view('admin.remarks.remark',[
            'remarks'=>$remarks
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'start'=>'required',
            'end'=>'required',
            'remark'=>'required',
            'grade'=>'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        $validator->after(function ($validator) use ($request){
            if(!$this->checkNegatives($request->start,$request->end)){
                $validator->errors()->add('negative', 'No Negative values allowed');
            }
        });
        $validator->after(function ($validator) use ($request){
            if(!$this->correctRange($request->start,$request->end)){
                $validator->errors()->add('incompatible', 'The end value should be greater than start value');
            }
        });
        $validator->after(function ($validator) use ($request){
            if(!$this->checkRange($request->start,$request->end)){
                $validator->errors()->add('range', 'The ranges should be distinct');
            }
        });
        if ($validator->fails()) {
            return response()->json([
                'errors'=>$validator->errors()
            ],422);
        }
        $remark = new Remark();
        $remark->start = $request->start;
        $remark->end = $request->end;
        $remark->grade = $request->grade;
        $remark->remark = $request->remark;
        $remark->school_id = auth()->user()->school_id;
        try{
            $remark->save();
        }catch (QueryException $e){
            return response()->json([
                'message'=>'Please the grades should be unique'
            ],501);
        }catch (\Exception $e){
            return response()->json([
                'message'=>'sorry an error occurred'
            ],501);
        }
        return $remark;
    }
    protected function correctRange($start,$end){
        return $end > $start;
    }
    protected function checkNegatives($start,$end){
        return $end > 0 && $start > 0;
    }
    protected function checkRange($start,$end,$id = null) :bool{
        $remarks = null;
        if($id){
            $remarks = Remark::where('id',$id)
                ->where('school_id',auth()->user()->school_id)
                ->get();
        }else{
            $remarks = Remark::where('school_id',auth()->user()->school_id)
                ->get();
        }
        if($remarks){
            foreach ($remarks as $remark){
                $remark_start = $remark->start;
                $remark_end = $remark->end;
                if($start >= $remark_start && $start <= $remark_end || $end >= $remark_start && $end <= $remark_end){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Remark::where('id',$id)->delete();
    }
}
