<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\FeeApproval;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeeSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $approvals = FeeApproval::where('school_id',auth()->user()->school_id)
            ->orderBy('created_at','desc')
            ->get();
        return view('admin.settings.fees.index',[
            'approvals'=>$approvals
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $approval = new FeeApproval();
            $approval->academic_year_id = $request->academic_year_id;
            $approval->term_id = $request->term_id;
            $approval->class_id = $request->class_id;
            $approval->school_id = auth()->user()->school_id;
            $approval->approved = true;
            $approval->user_id = auth()->user()->id;
            $approval->save();
        }catch (QueryException $q){
            return response()->json([
                'status'=>'error',
            ],501);
        }catch(\Exception $e){
            return response()->json([
                'status'=>'error',
            ],500);
        }
        return FeeApproval::find($approval->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = FeeApproval::where('id',$id)->update([
            'approved'=>$request->approved
        ]);
        return FeeApproval::where('id',$id)->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FeeApproval::find($id)->delete();
        return response()->json([
            'status'=>'success'
        ],200);
    }
}
