<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\ProfileField;
use App\Models\StudentSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ProfileField::all();
        $settings = StudentSetting::all();
        return view('admin.settings.student.index',[
            'data'=>$data->groupBy('type'),
            'settings'=>$settings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $fields = json_decode($request->fields);
        $previous = StudentSetting::where('school_id',auth()->user()->school_id);
        if(isset($previous)){
            $previous->delete();
        }
        foreach ($fields as $field){
            $student_field = new StudentSetting();
            $student_field->school_id = auth()->user()->school_id;
            $student_field->profile_fields_id = $field;
            $student_field->save();
        }
        $settings = StudentSetting::all();
        return response()->json(['message'=>'success','current_settings'=>$settings],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
