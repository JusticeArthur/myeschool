<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\GeneralSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class GeneralSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = GeneralSetting::where('school_id',auth()->user()->school_id)->first();
        return view('admin.settings.general',[
            'data'=>$data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = GeneralSetting::where('school_id',auth()->user()->school_id)->first();
        if($request->has('sidebar_enable')){
            if(!$settings){
                $new_settings = new GeneralSetting();
                $new_settings->school_id = auth()->user()->school_id;
                $new_settings->sidebar_opened = $request->sidebar_enable == 'on'? 1 :0;
                $new_settings->save();
            }else{
                GeneralSetting::where('school_id',auth()->user()->school_id)->update([
                    'sidebar_opened'=>$request->sidebar_enable == 'on'? 1 :0
                ]);
            }
        }else{
            if(!$settings){
                $new_settings = new GeneralSetting();
                $new_settings->school_id = auth()->user()->school_id;
                $new_settings->sidebar_opened = 0;
                $new_settings->save();
            }else{
                GeneralSetting::where('school_id',auth()->user()->school_id)->update([
                    'sidebar_opened'=>0
                ]);
            }
        }
        Session::flash('success','Info Updated');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
