<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\InstitutionSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstitutionSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        $currencies = file_get_contents(asset('files/currencies.json'));
        $countries = file_get_contents(asset('files/countries.json'));
        return view('admin.settings.institution',[
            'countries'=>$countries,
            'currencies'=>$currencies,
            'current_settings'=>$current_settings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'contact_email'=>'email|nullable'
            ];
        $this->validate($request,$rules);
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        $image_name = null;
        if($request->hasFile('logo')){
            $ima = $request->file('logo')
                ->storePubliclyAs('public/avatars',time() .'.'. $request->logo->getClientOriginalExtension() );
            $data = explode('/',$ima);
            $image_name = end($data);
        }
        if(!isset($image_name)){
            if($settings){
                $image_name = $settings->logo;
            }
        }
        if($settings){
            $settings->update([
                'name'=>$request->name,
                'country'=>$request->country,
                'currency'=>$request->currency,
                'contact_email'=>$request->contact_email,
                'contact_phone'=>$request->contact_phone,
                'address'=>$request->address,
                'logo'=>$image_name,
                'settlement_name'=>$request->settlement_name,
                'slogan'=>$request->slogan,
                'sms_display_name'=>$request->sms_display_name,
                'sms_password'=>$request->sms_password,
                'sms_username'=>$request->sms_username,
                'abbr'=>$request->abbr,
                'assessment'=>$request->assessment,
                'website'=>$request->website,
                'examination'=>$request->examination
            ]);
        }else{
            $data['school_id'] = auth()->user()->school_id;
            $data['country'] = $request->country;
            $data['website'] = $request->website;
            $data['assessment'] = $request->assessment;
            $data['examination'] = $request->examination;
            $data['currency'] = $request->currency;
            $data['sms_display_name']=$request->sms_display_name;
            $data['sms_password']=$request->sms_password;
            $data['sms_username']=$request->sms_username;
            $data['contact_email'] = $request->contact_email;
            $data['contact_phone'] = $request->contact_phone;
            $data['address'] = $request->address;
            $data['logo'] = $image_name;
            $data['settlement_name']=$request->settlement_name;
            $data['slogan']= $request->slogan;
            $data['abbr'] = $request->abbr;
            InstitutionSetting::create($data);
        }
        return response()->json(['message'=>'done'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
