<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\NewParent;
use App\Models\Student;
use App\Models\StudentBill;
use App\Models\StudentGuardian;
use App\Models\StudentWallet;
use App\utilities\Utility;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class MiscController extends Controller
{
    public function getStates($id){
        $country = Country::find($id);
        $states = $country->states;
        return $states;
    }
    public function addGuardian(Request $request){
        $rule = [
            'fname'=>'required',
            'lname'=>'required',
            'email'=>'email|nullable',
            'gender'=>'required'
        ];
        $this->validate($request,$rule);
        $data['title'] = $request->title;
        $data['lname'] = $request->lname;
        $data['fname'] = $request->fname;
        $data['mname'] = $request->mname;
        $data['occupation'] = $request->occupation;
        $data['pick'] = $request->pick ? 1 : 0;
        $data['email'] = $request->email;
        $data['gender'] = $request->gender;
        $data['temp_relation'] = $request->ward_rel;
        if(isset($request->country_id) && $request->country_id != 'undefined')
            $data['country_id'] = $request->country_id;
        if(isset($request->state_id) && $request->state_id != 'undefined')
            $data['state_id'] = $request->state_id;
        $data['contact'] = $request->contact;
        $data['city'] = $request->city;
        $data['address'] = $request->address;
        $data['school_id'] = auth()->user()->school_id;
        $guardian = NewParent::create($data);

        $guardian_ids = [];
        if(session()->has('guardians')){
            $guardian_ids = session('guardians');
        }
        array_push($guardian_ids,$guardian->id);
        session(['guardians'=>$guardian_ids]);
        $recent_guardians = [];
        foreach ($guardian_ids as $id){
            $temp = NewParent::find($id);
            if($temp)
                array_push($recent_guardians,$temp);
        }
        $collection = collect($recent_guardians);
        if($collection->count() > 0)
            return $collection->sortByDesc('created_at');

    }

    public function addStudent(Request $request){
        $rules = [
            'lname'=>'required',
            'fname'=>'required',
            'class_id'=>'required',
            'admission_date'=>'required',
            'ref_no'=>'required',
            'email'=>'email|unique:students|nullable'
        ];
        $this->validate($request,$rules);
        $data['lname'] = $request->lname;
        $data['mname'] = $request->mname;
        $data['fname'] = $request->fname;
        $data['admission_date'] = Carbon::parse($request->admission_date)->toDateString();
        $data['email'] = $request->email;
        $data['birth_date'] = Carbon::parse($request->birth_date)->toDateString();
        $data['gender'] = $request->gender;
        $data['address'] = $request->address;
        $data['phone'] = $request->phone;
        $data['class_id'] = $request->class_id;
        $data['ref_no'] = $request->ref_no;
        $data['religion'] = $request->religion;
        $data['student_category_id'] = $request->student_category_id;
        $data['blood_group'] = $request->blood_group;
        $data['genotype'] = $request->genotype;
        $data['health_info'] = 'not now';
        $data['nationality'] = $request->nationality;
        $data['country_id'] = $request->country_id;
        $data['state_id'] = $request->state_id;
        $data['school_id'] = auth()->user()->school_id;
        $data['city'] = $request->city;
        $data['username'] = str_slug($request->fname) .str_slug($request->lname) . str_random(8);
        $data['password'] = bcrypt('password');
        //check if request has photo
        if($request->hasFile('avatar')){
            $ima = $request->file('avatar')
                ->storePubliclyAs('public/students/avatars',time() .'.'. $request->avatar->getClientOriginalExtension() );
            $item = explode('/',$ima);
            $image_name = end($item);
            $data['avatar'] = $image_name;
        }
        try{
            $student = Student::create($data);
        }catch (QueryException $q) {
            $error_code = 'students_school_id_ref_no_unique';
            $error = $q->getMessage();
            $error_contains = str_contains($error,$error_code);
            return response()->json([
                'error'=>$error_contains
            ],425);
        }
        //check of balance is set an credit student account
        $academic_details = Utility::getActiveTermYear();
        $bill = StudentBill::where('school_id',auth()->user()->school_id)
            ->where('class_id',$student->class_id)
            ->where('term_id',$academic_details['active_term'])
            ->where('academic_year_id',$academic_details['active_year'])
            ->first();
        if(isset($bill)){
            //bill students
            $wallet = new StudentWallet();
            $wallet->student_id = $student->id;
            $wallet->class_id = $student->class_id;
            $wallet->school_id = auth()->user()->school_id;
            $wallet->balance = ((float) $bill->amount) * -1;
            $wallet->save();
        }
        //assign guardians
        $all_guardians = json_decode($request->guardians);
        foreach ($all_guardians as $guardian){
            if($guardian->assignable){
                $assign = new StudentGuardian();
                $assign->student_id = $student->id;
                $assign->parent_id = $guardian->id;
                $assign->relation = $guardian->temp_relation;
                $assign->save();
            }
        }
        $request->session()->flash('success', 'Saved Successfully!');
        $request->session()->forget('guardians');
        return response()->json(['message'=>'done'],200);
    }

    public function searchGuardian(Request $request){
        $data = NewParent::search($request->item)->where('school_id',auth()->user()->school_id)->get();
        return $data;
    }
    public function getGuardian(Request $request){
        $data = NewParent::find($request->id);
        $data->update([
            'temp_relation'=>$request->ward_rel
        ]);
        return $data;
    }

    public function updateStudentPassword(Request $request){
        $student = Student::where('id',$request->id)->first();
        $rules = [
            'password'=>'min:6|confirmed',
            'current'=>[
                function($attribute, $value, $fail) use ($request,$student){
                    if(isset($value))
                        if(!Hash::check($value,$student->password)){
                            return $fail('The Current Password is Incorrect');
                        }

                }
            ],
        ];
        $this->validate($request,$rules);
        Student::where('id',$request->id)->update([
            'password'=>bcrypt($request->password)
        ]);
        return response()->json([
            'message','success'
        ],200);
    }

    public function unassignGuardian($id){
        StudentGuardian::where('id',$id)->delete();
        return response()->json([
            'message'=>'success'
        ],200);
    }

}
