<?php

namespace App\Http\Controllers\Admin;

use App\Models\AcademicInfo;
use App\Models\AcademicYear;
use App\Models\Fee;
use App\Models\FeeApproval;
use App\Models\InstitutionSetting;
use App\Models\NewClass;
use App\Models\NewParent;
use App\Models\ReportTemplate;
use App\Models\Term;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DetailController extends Controller
{
    public function index(){
        return view('admin.details.index');
    }
    //items functions
    public function itemsIndex(){
        $items = Item::where('school_id',auth()->user()->school_id)
            ->orderBy('created_at','desc')
            ->get();
        return view('admin.details.items.add',[
            'items'=>$items
        ]);
    }
    public function saveItem(Request $request){
            try{
                $item = new Item();
                $item->name = $request->name;
                $item->amount = $request->amount;
                $item->school_id = auth()->user()->school_id;
                $item->academic_year_id = $request->year;
                $item->term_id = $request->term;
                if($item->save()){
                    return response()->json([
                        'status'=>'success',
                        'data'=>Item::find($item->id)
                    ]);
                }
                return response()->json([
                    'status'=>'error'
                ]);
            }catch (\Exception $e){
                return response()->json([
                    'status'=>'error'
                ]);
            }
    }
    public function deleteItem($id){
        //check if the item is linked with a fee
        $fee = Fee::where('item_id',$id)->first();
        if(!isset($fee)){
            Item::find($id)->delete();
            return response()->json([
                'status'=>'success'
            ],200);
        }else{
            return response()->json([
                'status'=>'error'
            ],501);
        }
    }
    public function  updateItem(Request $request,$id){
        try{
            Item::where('id',$id)->update([
                'name'=>$request->name,
                'amount'=>$request->amount
            ]);
        }catch (QueryException $e){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return Item::find($id);
    }
    //years
    public function academicIndex(){
        $years = AcademicYear::where('school_id',auth()->user()->school_id)
            ->orderBy('status','asc')
            ->get();
        return view('admin.details.academics.add',[
            'years'=>$years
        ]);
    }
    public function saveAcademicYear(Request $request){
        $item = new AcademicYear();
            try {
                    $item->name = $request->name;
                    $item->school_id = auth()->user()->school_id;
                    if ($request->status == 'active') {
                        AcademicYear::where('status', 'active')
                            ->where('school_id',auth()->user()->school_id)
                            ->update([
                            'status' => 'inactive'
                        ]);
                    }
                    $item->status = $request->status;
                    if ($item->save()) {
                        return response()->json([
                            'status' => 'success',
                            'item'=>AcademicYear::where('school_id',auth()->user()->school_id)
                                ->orderBy('created_at','desc')
                                ->get()
                        ]);
                    }
                    return response()->json([
                        'status' => 'error'
                    ]);
                }catch (\Exception $e){
                return response()->json([
                    'status'=>'error'
                ]);
            }


    }

    public function deleteAcademicYear($id){
        AcademicYear::find($id)->delete();
        return response()->json([
            'message'=>'success'
        ]);
    }
    public function updateEditAcademic(Request $request){
            if ($request->status == 'active') {
                AcademicYear::where('school_id',auth()->user()->school_id)
                    ->where('status', 'active')->update([
                        'status' => 'inactive'
                    ]);
            }
            try{
                AcademicYear::where('id',$request->id)
                    ->update([
                        'status' => $request->status,
                        'name'=>$request->name
                    ]);
            }catch (QueryException $q){
                return response()->json([
                    'status'=>'error'
                ],501);
            }
        return AcademicYear::where('school_id',auth()->user()->school_id)
            ->orderBy('updated_at','desc')
            ->get();
    }
    //terms
    public function showTerms(){
        return view('admin.details.term.add');
    }
    protected function validateDeadline($start_date,$end_date){
        $start = Carbon::parse($start_date);
        $end = Carbon::parse($end_date);
        if($start->lt($end))
            return false;
        return true;
    }
    //adding a term
    public function saveTerms(Request $request){
        $validator = Validator::make($request->all(), [
            'start_date'=>'required',
            'end_date'=>'required',
        ]);
        $validator->after(function($validator) use ($request){
            if($this->validateDeadline($request->start_date,$request->end_date)){
                $validator->errors()->add('date','The end date should be more current');
            }
        });
        if ($validator->fails()) {
            return response()->json([
                'errors'=>$validator->errors()
            ],422);
        }
        $item = new Term();
        try{
                $item->number = $request->number;
                $item->display_name = $request->name;
                $item->start_date = Carbon::parse($request->start_date)->toDateString();
                $item->promotable = $request->promotable;
                $item->end_date = Carbon::parse($request->end_date)->toDateString();
                if($request->status == 'active'){
                    $newStatus = Term::where('status','active')
                        ->where('school_id',auth()->user()->school_id)
                        ->update([
                        'status'=>'inactive'
                    ]);
                }
                $item->status = $request->status;
                $item->school_id = auth()->user()->school_id;
                $item->academic_year_id = $request->year;
                $item->save();
                return Term::where('school_id',auth()->user()->school_id)->get();

        }catch (QueryException $e){
            return response()->json([
                'message'=>'Duplicate Entry'
            ],500);
        }catch (\Exception $e){
            return response()->json([
                'message'=>'Sorry an error occurred'
            ],500);
        }

    }

    public function deleteTerm($id){
        $data = Term::find($id)->delete();
        return response()->json([
            'message'=>'Success',
        ]);
    }
    public function updateEditTerm(Request $request){
        $validator = Validator::make($request->all(), [
            'start_date'=>'date|required',
            'end_date'=>'date|required',
        ]);
        $validator->after(function($validator) use ($request){
            if($this->validateDeadline($request->start_date,$request->end_date)){
                $validator->errors()->add('date','The selected dates are invalid');
            }
        });
        if ($validator->fails()) {
            return response()->json([
                'status'=>'date'
            ]);
        }
        if ($request->status == 'active') {
            Term::where('status', 'active')
                ->where('school_id',auth()->user()->school_id)
                ->update([
                    'status' => 'inactive'
                ]);
        }
        DB::table('terms')->where('id',$request->id)
            ->update([
                'status' => $request->status,
                'display_name'=>$request->display_name,
                'promotable'=>$request->promotable,
                'start_date'=>Carbon::parse($request->start_date)->toDateString(),
                'end_date'=>Carbon::parse($request->end_date)->toDateString()
            ]);
        return Term::where('school_id',auth()->user()->school_id)
            ->orderby('updated_at','asc')
            ->get();
    }

    ///fee details start here
    public function showFees(){
        $approvals  = FeeApproval::where('school_id',auth()->user()->school_id)
            ->where('approved',true)
            ->get();
        $breakdowns = Fee::where('school_id',auth()->user()->school_id)
            ->orderBy('class_id')
            ->get();
        return view('admin.details.fees.add',[
            'breakdowns'=>$breakdowns,
            'approvals'=>$approvals
        ]);
    }

    public function saveFees(Request $request){
        $rules =
            [
                'classes'=>'required',
                'items'=>'required'
            ];

        $this->validate($request,$rules);
            try{
                for($i = 0; $i < count($request->classes) ; $i++){
                    for($j = 0; $j < count($request->items);$j++){
                        $fee = new Fee();
                        $tempSchool = $fee->school_id = auth()->user()->school_id;
                        $tempTerm = $fee->term_id = $request->term;
                        $tempYear = $fee->academic_year_id = $request->year;
                        $tempItem = $fee->item_id = $request->items[$j]['id'];
                        $tempClass = $fee->class_id = $request->classes[$i]['id'];
                        //test for duplicates
                        $data = Fee::where('school_id',$tempSchool)
                            ->where('academic_year_id',$tempYear)
                            ->where('term_id',$tempTerm)
                            ->where('item_id',$tempItem)
                            ->where('class_id',$tempClass)
                            ->first();
                        //check approvals
                        $approvals  = FeeApproval::where('school_id',$tempSchool)
                            ->where('approved',true)
                            ->where('term_id',$tempTerm)
                            ->where('class_id',$tempClass)
                            ->where('academic_year_id',$tempYear)
                            ->first();
                        if(isset($approvals))
                            continue;
                        if(isset($data))
                            continue;

                        $fee->save();
                    }
                }
                return response()->json([
                    'status'=>'success',
                    'data'=>Fee::where('school_id',auth()->user()->school_id)
                        ->orderBy('created_at','desc')
                        ->get()
                ]);
            }catch (\Exception $e){
                return response()->json([
                    'status'=>'error'
                ]);
            }
    }

    public function deleteBreakDownItem($id){
        Fee::find($id)->delete();
        return response()->json([
            'status'=>'success'
        ],200);
    }

    public function showEditAcademic($id){
        $data = AcademicYear::where('id',$id)->get();
        return $data;
    }
    public function filterFees(Request $request){
        $class = $request->class_id !== 'all'?true:false;
        $class_id = 0;
        if($class){
            $class_id = $request->class_id;
        }
        $term = $request->term_id !== 'all'?true:false;
        $term_id = 0;
        if($term){
            $term_id = $request->term_id;
        }
        $year = $request->academic_year_id !== 'all'?true:false;
        $year_id = 0;
        if($year){
            $year_id = $request->academic_year_id;
        }
        $filtered = Fee::where('school_id',auth()->user()->school_id)
            ->when($class, function ($query) use ($class_id) {
                return $query->where('class_id', $class_id);
            })
            ->when($year, function ($query) use ($year_id) {
                return $query->where('academic_year_id', $year_id);
            })
            ->when($term, function ($query) use ($term_id) {
                return $query->where('term_id', $term_id);
            })
            ->get();
        return $filtered;
    }


    public function showEditTerm($id){
        $data = Term::find($id);
        return $data;
    }


    public function getReportCardTemplate(){
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        $templates = ReportTemplate::all();
        return view('admin.details.templates.report',[
            'templates'=>$templates,
            'settings'=>$settings
        ]);
    }
    public function setSchoolTemplate($template_id){
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        if(!isset($settings)){
            return response()->json([
                'status'=>'error'
            ],422);
        }else{
            $settings->report_template_id = $template_id;
            $settings->save();
        }
    }
    public function getAllGuardians(){
        $guardians = NewParent::withCount('wards')->where('school_id',auth()->user()->school_id)->get();
       $occupations = file_get_contents(asset('files/occupations.json'));
        return view('admin.details.guardians.index',[
            'guardians'=>$guardians,
            'occupations'=>$occupations
        ]);
    }
    public function updateGuardian(Request $request,$id){
        $data['country_id'] = $request->country_id;
        $data['fname'] = $request->fname;
        $data['lname'] = $request->lname;
        $data['occupation'] = $request->occupation;
        $data['pick'] = $request->pick ? 1 :0;
        $data['mname'] = $request->mname;
        $data['title'] = $request->title;
        $data['contact'] = $request->contact;
        $data['alt_contact'] = $request->alt_contact;
        $data['address'] = $request->address;
        $data['city'] = $request->city;
        $data['state_id'] = $request->state_id;
        $data['gender'] = $request->gender;
        NewParent::where('id',$id)->update($data);
        return NewParent::find($id);
    }
    public function deleteGuardian(Request $request){
        if((int)$request->count > 0){
            NewParent::find($request->id)->delete();
        }else{
            return response()->json([
                'status'=>'error'
            ],422);
        }
    }
}
