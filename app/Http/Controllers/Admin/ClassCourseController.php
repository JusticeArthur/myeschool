<?php

namespace App\Http\Controllers\Admin;

use App\Models\AcademicYear;
use App\Models\Term;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ClassCourse;
use App\Models\NewClass;
use App\Models\Teacher;
use App\Models\Course;
use Illuminate\Support\Facades\DB;
class ClassCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.subjects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pairing = ClassCourse::where('school_id',auth()->user()->school_id)
            ->orderBy('created_at','desc')
            ->get();
        //dd($pairing->toArray());
        return view('admin.subjects.create',[
            'pairings'=>$pairing
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        if($request->ajax()){
            try{
                $class_course = new ClassCourse();
                //populate with data

                $class_course->class_id = $request->class_id;
                $class_course->teacher_id = $request->teacher_id;
                $class_course->school_id = auth()->user()->school_id;
                $class_course->course_id = $request->name;
                $class_course->academic_year_id = $active_year->id;
                $class_course->term_id = $active_term->id;
                $class_course->save();
                return response()->json([
                    'status'=>'success',
                    'data'=> ClassCourse::where('id',$class_course->id)
                        ->orderBy('created_at','desc')
                        ->first()
                ]);
            }catch (\Exception $e){
                return response()->json([
                    'status'=>'error'
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit_subject = DB::table('class_courses')
            ->where('class_courses.id', $id)
            ->leftjoin('courses', 'class_courses.course_id', 'courses.id')
            ->leftjoin('classes', 'class_courses.class_id', 'classes.id')
            ->leftjoin('teachers', 'class_courses.teacher_id', 'teachers.id')
            ->select('class_courses.id','courses.id as course_id', 'classes.id as class_id', 'teachers.id as teacher_id')
            ->first();
            return json_encode($edit_subject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try{
            DB::table('class_courses')
                ->where('id', $id)
                ->update([
                    'class_id' => $request->class['id'],
                    'teacher_id' => $request->teacher['id'],
                    'course_id'=>$request->course['id']
                ]);
        }catch (QueryException $e){
            return response()->json([
                'status'=>'error'
            ],501);
        }
        return ClassCourse::where('school_id',auth()->user()->school_id)
            ->orderBy('updated_at','desc')
            ->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ClassCourse::find($id)->delete();
        return response()->json([
            'status'=>'success'
        ],200);
    }
}
