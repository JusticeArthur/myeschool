<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slot;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slots = Slot::where('school_id',auth()->user()->school_id)
            ->orderby('created_at')
            ->get();
        return view('admin.slots.index',[
            'slots'=>$slots
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'start'=>'required',
            'end'=>'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        $validator->after(function ($validator)use ($request){
            if(!$this->checkDates($request->start,$request->end)){
                $validator->errors()->add('incompatible', 'The end time should be more current');
            }
        });
        $validator->after(function ($validator) use ($request){
           if(!$this->validateDates($request->start,$request->end)){
                $validator->errors()->add('dates', 'Make Sure All the time slots are unique');
            };
        });
        if ($validator->fails()) {
            return response()->json([
                'errors'=>$validator->errors()
            ],422);
        }
        $slot = new Slot();
        $slot->school_id = auth()->user()->school_id;
        $slot->name = $request->name;
        $slot->start = $request->start;
        $slot->end = $request->end;
        $slot->save();
        return response()->json([
            'slot'=>$slot
        ]);
    }
    protected function checkDates($start,$end,$id=null) :bool{
        $new_start = Carbon::parse($start);
        $new_end = Carbon::parse($end);
        return $new_end->gt($new_start);
    }
    protected function validateDates($start,$end,$id=null) : bool {
        $new_start = Carbon::parse($start);
        $new_end = Carbon::parse($end);
        //get previous slots
        $slots = null;
        if($id){
            $slots = Slot::where('school_id',auth()->user()->school_id)
                    ->where('id','<>',$id)
                    ->get();
        }else{
            $slots = Slot::where('school_id',auth()->user()->school_id)
                    ->get();
        }

        if(isset($slots)){
            foreach ($slots as $slot){
                $previous_start = Carbon::parse($slot->start);
                $previous_end = Carbon::parse($slot->end);
                if($new_start->between($previous_start,$previous_end,false) || $new_end->between($previous_start,$previous_end,false)){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'=>'required',
            'start'=>'required',
            'end'=>'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        $validator->after(function ($validator)use ($request){
            if(!$this->checkDates($request->start,$request->end)){
                $validator->errors()->add('incompatible', 'The end time should be more current');
            }
        });
        $validator->after(function ($validator) use ($request,$id){
            if(!$this->validateDates($request->start,$request->end,$id)){
                $validator->errors()->add('dates', 'Make Sure All the time slots are unique');
            };
        });
        if ($validator->fails()) {
            return response()->json([
                'errors'=>$validator->errors()
            ],422);
        }
        Slot::where('id',$id)->update([
           'name'=>$request->name,
           'start'=>$request->start,
           'end'=>$request->end
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slot::where('id',$id)->delete();
        return response()->json([
            'message'=>'success'
        ]);
    }
}
