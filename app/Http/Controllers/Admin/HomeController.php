<?php

namespace App\Http\Controllers\Admin;

use App\Models\SmsBill;
use App\Models\Transaction;
use App\utilities\Utility;
use Illuminate\Http\Request;
use App\Models\Teacher;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Term;
use App\Models\AcademicYear;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function home(){
        $student_count = Student::where('school_id',auth()->user()->school_id)->count();
        $teachers_count = Teacher::where('school_id',auth()->user()->school_id)->count();
        $user = auth()->user();
        //get amounts
        $now = Carbon::now();
        $today = $now->toDateString();
        $today_amount = Transaction::where('school_id',$user->school_id)
            ->whereDate('created_at',$today)
            ->sum('amount');
        $activeTerm = Utility::getActiveTermYear();
        $term_amount = Transaction::where('school_id',$user->school_id)
            ->where('term_id',$activeTerm['active_term'])
            ->sum('amount');
        $sms_cost = SmsBill::where('school_id',auth()->user()->school_id)->first();
        return view('admin.main',[
            'student_count'=>$student_count,
            'teacher_count'=>$teachers_count,
            'today_amount'=>$today_amount,
            'term_amount'=> isset($term_amount)? $term_amount:0,
            'sms_cost'=>$sms_cost
        ]);
    }
}
