<?php

namespace App\Http\Controllers\Admin;
use App\Models\NewParent;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NewClass;
use Illuminate\Support\Facades\Auth;
use Webpatser\Uuid\Uuid;
use Yajra\DataTables\Facades\DataTables;
class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = NewClass::where('school_id',\auth()->user()->school_id)->get();
        return view('admin.class.create',[
            'classes'=>$classes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $class = new NewClass();
            $class->school_id = Auth::user()->school_id;
            $class->name = $request->name;
            $class->code = $request->code;
            $class->teacher_id = $request->teacher;
            $class->description = $request->description;
           if($class->save()){
               return response()->json([
                   'message'=>'Class added successfully',
                   'status'=>'success',
                   'data'=>NewClass::where('id',$class->id)->first()
               ]);
           }else{
               return response()->json([
                   'message'=>'Duplicate Data',
                   'status'=>'error'
               ]);
           }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = NewClass::where('id',$id)->get();
        return response()->json([
            'data'=>$data,
            'id'=>$id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        try{
            NewClass::where('id',$id)->update([
                'name'=>$request->name,
                'code'=>$request->code,
                'description'=>$request->description,
                'teacher_id'=>$request->teacher['id']
            ]);
        }catch (QueryException $q){
            return response()->json([
                'message'=>'error'
            ],501);
        }
        return NewClass::where('school_id',\auth()->user()->school_id)->orderBy('updated_at','desc')->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NewClass::findorfail($id)->delete();
        return response()->json([
            'success'=>'Deleted Succssfully',
        ]);
    }
}
