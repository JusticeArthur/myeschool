<?php

namespace App\Http\Controllers\Admin;

use App\Models\AcademicInfo;
use App\Models\Country;
use App\Models\State;
use App\Models\Student;
use App\Models\NewClass;
use App\Models\NewParent;
use App\Models\StudentAcademicInfo;
use App\Models\StudentGuardian;
use App\Models\StudentSetting;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Student::where('school_id',\auth()->user()->school_id)
            ->where('status','active')->get();
        $classes = NewClass::where('school_id',\auth()->user()->school_id)->get();
        return view('admin.student.index',[
            'data'=>$data,
            'classes'=>$classes
        ]);
    }

    public function searchStudent(Request $request){
        $search = null;
        $class_id = isset($request['class_id']) && $request['class_id'] != 'all' ? true:false;
        if(isset($request->data) || $class_id){
            $search = Student::where('school_id',\auth()->user()->school_id)
                ->where('status',$request->status)->search($request->data)
                ->when($class_id, function ($query, $class_id) use ($request) {
                    return $query->where('class_id', $request->class_id);
                })
                ->get();
        }else{
            $search = Student::where('school_id',\auth()->user()->school_id)
                ->where('status',$request->status)->get();
        }
        return $search;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $recent_guardians = [];
        if(session()->has('guardians')){
            $guardian_ids = session('guardians');
            foreach ($guardian_ids as $id){
                $temp = NewParent::find($id);
                if($temp)
                    array_push($recent_guardians,$temp);
            }
        }
        $collection = collect($recent_guardians);
        //get settings
        $settings = DB::table('student_settings')->where('school_id',\auth()->user()->school_id)
            ->leftJoin('profile_fields','profile_fields.id','student_settings.profile_fields_id')
            ->select('student_settings.id','student_settings.school_id','student_settings.profile_fields_id',
                'profile_fields.name','profile_fields.type')
            ->get();
        $nationalities = file_get_contents(public_path('files/nationality.json'));
        $classes = NewClass::where('school_id',auth()->user()->school_id)->get();
        $countries = Country::all();
        $occupations = file_get_contents(asset('files/occupations.json'));

        return view('admin.student.create',[
            'countries'=>$countries,
            'classes'=>$classes,
            'nationalities'=>$nationalities,
            'all_guardians'=>$collection->count() > 0 ?$collection->sortByDesc('id'):'',
            'settings'=>$settings,
            'occupations'=>$occupations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'email'=>'email|required|unique:parents|unique:users,email',
        ]);

        try{
            //parent data
            $admin = Role::where('school_id',auth()->user()->school_id)
                ->where('name','guardian')->first();
            $admin_stud = Role::where('school_id',auth()->user()->school_id)
                ->where('name','student')->first();
            $parent = new NewParent();
            $parent->school_id = auth()->user()->school_id;
            $parent->fname =$request->father_name;
            $parent->mname = $request->mother_name;
            $parent->occupation = $request->occupation;
            $parent->contact = $request->contact;
            $parent->alt_contact = $request->alt_contact;
            $parent->religion = $request->p_religion;
            $parent->email = $request->email;
            $parent->nationality = $request->p_nationality;
            $parent->address  = $request->address;
            $parent->country = $request->country;
            $parent->save();


            //insert parent to users table
            $user = new User();
            //insert users
            $user->name = $request->father_name;
            $user->email = $request['email'];
            $user->password = bcrypt('password');
            $user->school_id = Auth::user()->school_id ? Auth::user()->school_id: 0;
            $user->role_id = $admin->id;
            $user->save();
            $user->attachRole($admin);

            //
            $currentParent = NewParent::orderby('created_at','DESC')->pluck('id')->first();

        //academic info data
        $info = new StudentAcademicInfo();
        $info->school_id = auth()->user()->school_id;
        $info->class_id = $request->s_class;
        $info->last_school = $request->last_sch;
        $info->roll = $request->roll_no;
        $info->reg_number = $request->reg_number;
        $info->sports = $request->sports;
            //current info
        $currentInfo = AcademicInfo::orderby('created_at','DESC')->pluck('id')->first();
            //student data
            $student = new Student();
            $tempFirstName = $student->fname = $request->fname;
            $student->mname = $request->mname;
            $tempLastName = $student->lname = $request->lname;
            $student->school_id = auth()->user()->school_id;
            $student->full_name = $request->lname .' '.$request->mname. ' '.$request->fname;
            $student->email = $request->student_email;
            $student->username = $tempFirstName.'.'.$tempLastName.str_random(5);
            $student->ref_no = uniqid('MYE');
            $student->role_id = $admin_stud->id;
            $student->birth_date = $request->dob;
            $student->gender = $request->gender;
            $student->class_id = $request->s_class;
            $student->religion = $request->s_religion;
            $student->student_academic_info_id =$currentInfo;
            $student->password =bcrypt('password');
            $student->name =$tempFirstName.' '.$request->lname;
            $student->parent_id = $currentParent;
            $student->save();


            //how do u login a student??

            if($info->save()){
                return redirect()
                    ->route('admin.student.receipt.print')
                    ->with([
                    'success'=>'Student Added Successfully',
                ]);
            }
            return redirect()->back()->with([
                'error' => 'An Error Occured'
            ]);

        }catch (QueryException $q){
            $error_code = 'students_school_id_ref_no_unique';
            $error = $q->getMessage();
            $error_contains = str_contains($error_code,$error);
            return response()->json([
                'error'=>$error_contains
            ],425);
        }
        catch (\Exception $e){
            return redirect()->back()->with([
                'error' => 'An Error Occured'
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $studentData = Student::with(['guardians','transactions','wallet'])->where('school_id',\auth()->user()->school_id)
            ->where('username',$id)
            ->first();
        /*echo $studentData->toJson();
        dd();*/
        //dd($studentData->toArray());

        return view('admin.student.showprofile',[
            'students'=>$studentData
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = DB::table('student_settings')->where('school_id',\auth()->user()->school_id)
            ->leftJoin('profile_fields','profile_fields.id','student_settings.profile_fields_id')
            ->select('student_settings.id','student_settings.school_id','student_settings.profile_fields_id',
                'profile_fields.name','profile_fields.type')
            ->get();
        $nationalities = file_get_contents(public_path('files/nationality.json'));
        $classes = NewClass::where('school_id',auth()->user()->school_id)->get();
       $countries = Country::all();
        $persondata = Student::where('username',$id)->where('school_id',\auth()->user()->school_id)->first();
        $states = State::where('country_id',$persondata->country_id)->get();
        return view('admin.student.edit',[
            'countries'=>$countries,
            'classes'=>$classes,
            'personal_data'=>$persondata,
            'nationalities'=>$nationalities,
            'settings'=>$settings,
            'states'=>$states
        ]);
        
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'lname'=>'required',
            'fname'=>'required',
            'class_id'=>'required',
            'admission_date'=>'required',
            'ref_no'=>'required',
            //'email'=>'email|unique:students|nullable'
        ];
        $this->validate($request,$rules);
        $data['lname'] = $request->lname;
        $data['mname'] = $request->mname;
        $data['fname'] = $request->fname;
        $data['full_name'] = $request->lname .' '.$request->mname. ' '.$request->fname;
        $data['address'] = $request->address;
        $data['admission_date'] = Carbon::parse($request->admission_date)->toDateString();
        $data['email'] = $request->email;
        $data['birth_date'] = Carbon::parse($request->birth_date)->toDateString();
        $data['gender'] = $request->gender;
        $data['phone'] = $request->phone;
        $data['class_id'] = $request->class_id;
        $data['ref_no'] = $request->ref_no;
        $data['religion'] = $request->religion;
        $data['student_category_id'] = $request->student_category_id;
        $data['blood_group'] = $request->blood_group;
        $data['genotype'] = $request->genotype;
        $data['health_info'] = 'not now';
        $data['nationality'] = $request->nationality;
        $data['country_id'] = $request->country_id;
        $data['state_id'] = $request->state_id;
        $data['city'] = $request->city;

        if($request->hasFile('avatar')){
            $ima = $request->file('avatar')
                ->storePubliclyAs('public/students/avatars',time() .'.'. $request->avatar->getClientOriginalExtension() );
            $item = explode('/',$ima);
            $image_name = end($item);
            $data['avatar'] = $image_name;
        }
        try{
            $student = Student::where('id',$id)->update($data);
        }catch (QueryException $q) {
            $error_code = 'students_school_id_ref_no_unique';
            $error = $q->getMessage();
            $error_contains = str_contains($error,$error_code);
            return response()->json([
                'error'=>$error_contains
            ],425);
        }

        $all_guardians = json_decode($request->guardians);
        $all_guardians = collect($all_guardians);
        foreach ($all_guardians as $guardian){
            if($guardian->assignable){
                //check to see if the guardian has already been assigned
                //if yes please continue without any error
                try{
                    $check = StudentGuardian::where('parent_id',$guardian->id)
                        ->where('student_id',$id)->first();
                    if(isset($check))
                        continue;
                    $assign = new StudentGuardian();
                    $assign->student_id = $id;
                    $assign->parent_id = $guardian->id;
                    $assign->relation = $guardian->temp_relation;
                    $assign->save();
                }catch (QueryException $q){
                    return response()->json([
                        'message'=>'This Parent is already assigned to this student'
                    ],501);
                }catch (\Exception $e){
                    return response()->json([
                        'message'=>'Sorry an error occurred'
                    ],501);
                }
            }
        }
        return response()->json([
            'message'=>'success'
        ],200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info = DB::table('students')->where('id',$id)->pluck('student_academic_info_id')->first();
        DB::table('students')->where('id',$id)->delete();
        DB::table('student_academic_infos')->where('id',$info)->delete();
        return redirect()->back()->with([
            'success'=>'Deleted Successfully'
        ]);
    }
}
