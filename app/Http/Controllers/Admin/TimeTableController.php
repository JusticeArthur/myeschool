<?php


namespace App\Http\Controllers\Admin;
use App\Models\ClassCourse;
use App\utilities\Utility;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
//use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Models\NewClass;
use App\Models\Course;
use App\Models\Teacher;
use App\Models\Timetable;
use App\Models\Slot;
use Yajra\DataTables\Facades\DataTables;
use DB;
class TimeTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        $slots = Slot::where('school_id',auth()->user()->school_id)->get();
        $classes = NewClass::where('school_id',auth()->user()->school_id)->get();
        return view('admin.timetable.index',
            [
                'classes'=>$classes,
                'slots'=>$slots
            ]
        );

    }

    public function getTeachers($id){
        $class_courses = ClassCourse::where('school_id',auth()->user()->school_id)
        ->where('class_id',$id)->get();
        $active = Utility::getActiveTermYear();
        //get class time table
        $class_slots = Timetable::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$active['active_year'])
            ->where('term_id',$active['active_term'])
            ->where('class_id',$id)
            ->select('slot_id')
            ->groupBy('slot_id')
            ->get();
        $class_days = Timetable::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$active['active_year'])
            ->where('term_id',$active['active_term'])
            ->where('class_id',$id)
            ->select('day')
            ->groupBy('day')
            ->get();
        $all_class_days_slots = [];
        foreach ($class_days as $day){
            $temp = Timetable::where('school_id',auth()->user()->school_id)
                ->where('academic_year_id',$active['active_year'])
                ->where('term_id',$active['active_term'])
                ->where('class_id',$id)
                ->where('day',$day->day)
                ->get();
            array_push($all_class_days_slots,$temp);
        }

        return response()->json([
            'courses'=>$class_courses,
            'class_slots'=>$class_slots,
            'full_timetable'=>$all_class_days_slots,
            'class_name'=>NewClass::find($id)->name
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Utility::getActiveTermYear();
        $timetable = new Timetable();
        $timetable->academic_year_id = $data['active_year'];
        $timetable->term_id = $data['active_term'];
        $timetable->course_id = $request->course_id;
        $timetable->day = $request->day;
        $timetable->school_id = auth()->user()->school_id;
        $timetable->slot_id = $request->slot_id;
        $timetable->class_id = $request->class_id;
        $timetable->teacher_id = $request->teacher_id;
        try{
            $timetable->save();
        }catch (QueryException $e){
            if (strpos($e->errorInfo[2], Utility::CLASS_SLOT_ASSIGNED) !== false) {
                return response()->json([
                    'status'=>0,
                    'message'=>'Please this class slot has already been assigned'
                ],500);
            }else if(strpos($e->errorInfo[2], Utility::TEACHER_SLOT_ASSIGNED) !== false){
                return response()->json([
                    'status'=>0,
                    'message'=>'Please this teacher has already been assigned for this slot'
                ],500);
            }
            else{
                dd(Utility::CLASS_SLOT_ASSIGNED);
            }
        }catch (\Exception $e){
            return response()->json([
                'status'=>0,
                'message'=>'sorry an error occurred'
            ],500);
        }
        //////////////returning class data ///////
        //get class time table
        $class_slots = Timetable::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$data['active_year'])
            ->where('term_id',$data['active_term'])
            ->where('class_id',$timetable->class_id)
            ->select('slot_id')
            ->groupBy('slot_id')
            ->get();
        $class_days = Timetable::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$data['active_year'])
            ->where('term_id',$data['active_term'])
            ->where('class_id',$timetable->class_id)
            ->select('day')
            ->groupBy('day')
            ->get();
        $all_class_days_slots = [];
        foreach ($class_days as $day){
            $temp = Timetable::where('school_id',auth()->user()->school_id)
                ->where('academic_year_id',$data['active_year'])
                ->where('term_id',$data['active_term'])
                ->where('class_id',$timetable->class_id)
                ->where('day',$day->day)
                ->get();
            array_push($all_class_days_slots,$temp);
        }
        return response()->json([
            'class_slots'=>$class_slots,
            'full_timetable'=>$all_class_days_slots,
            'class_name'=>NewClass::find($timetable->class_id)->name
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            Timetable::where('id',$id)->update([
                'slot_id'=>$request->slot_id,
                'course_id'=>$request->course_id,
                'class_id'=>$request->class_id,
                'day'=>$request->day,
                'teacher_id'=>$request->teacher_id
            ]);
        }catch (QueryException $e){
            if (strpos($e->errorInfo[2], Utility::CLASS_SLOT_ASSIGNED) !== false) {
                return response()->json([
                    'status'=>0,
                    'message'=>'Please this class slot has already been assigned'
                ],500);
            }else if(strpos($e->errorInfo[2], Utility::TEACHER_SLOT_ASSIGNED) !== false){
                return response()->json([
                    'status'=>0,
                    'message'=>'Please this teacher has already been assigned for this slot'
                ],500);
            }
            else{
                dd(Utility::CLASS_SLOT_ASSIGNED);
            }
        }catch (\Exception $e){
            return response()->json([
                'status'=>0,
                'message'=>'sorry an error occurred'
            ],500);
        }
        //////////////returning class data ///////
        //get class time table
        $data = Utility::getActiveTermYear();
        $class_slots = Timetable::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$data['active_year'])
            ->where('term_id',$data['active_term'])
            ->where('class_id',$request->class_id)
            ->select('slot_id')
            ->groupBy('slot_id')
            ->get();
        $class_days = Timetable::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$data['active_year'])
            ->where('term_id',$data['active_term'])
            ->where('class_id',$request->class_id)
            ->select('day')
            ->groupBy('day')
            ->get();
        $all_class_days_slots = [];
        foreach ($class_days as $day){
            $temp = Timetable::where('school_id',auth()->user()->school_id)
                ->where('academic_year_id',$data['active_year'])
                ->where('term_id',$data['active_term'])
                ->where('class_id',$request->class_id)
                ->where('day',$day->day)
                ->get();
            array_push($all_class_days_slots,$temp);
        }
        return response()->json([
            'class_slots'=>$class_slots,
            'full_timetable'=>$all_class_days_slots,
            'class_name'=>NewClass::find($request->class_id)->name
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Timetable::where('id',$id)->delete();
    }
}
