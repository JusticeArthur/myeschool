<?php

namespace App\Http\Controllers\Admin;

use App\Models\ClassCourse;
use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::where('school_id',auth()->user()->school_id)
            ->orderBy('created_at','desc')
            ->get();
        return view('admin.general_course.create',[
            'courses'=>$courses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.general_course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try{
                $now = Carbon::now();
                $course = new Course();
                //populate with data
                $course->name = $request->name;
                $course->code = $request->code;
                $course->year = $now->year;
                $course->school_id = auth()->user()->school_id;
                $course->save();
                return response()->json([
                    'status'=>'success',
                    'data'=>$course
                ]);
            }catch (\Exception $e){
                return response()->json([
                    'status'=>'error'
                ]);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $edit_course = Course::find($id);
       return $edit_course;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try{
            DB::table('courses')
                ->where('id', $id)
                ->update([
                    'name' => $request->name,
                    'code' => $request->code
                ]);
        }catch (QueryException $e){
            return response()->json([
                'status'=>'error'
            ],501);
        }

       return Course::where('school_id',auth()->user()->school_id)
            ->orderBy('updated_at','desc')
            ->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::find($id)->delete();
        return response()->json([
            'status'=>'success'
        ],200);
    }
}
