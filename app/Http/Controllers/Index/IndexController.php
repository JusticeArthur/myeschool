<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function getScheduleIndex(){
        return view('index.schedule');
    }
    public function getManageCourseIndex(){
        return view('index.manage-courses');
    }
    public function getManageStudentIndex(){
        return view('index.manage-students');
    }
    public function getManageClassesIndex(){
        return view('index.manage-classes');
    }
    public function getManageFees(){
        return view('index.manage-fees');
    }
    public function getManageFinanceBills(){
        return view('index.finance.manage-bills');
    }
    public function getManageExams(){
        return view('index.manage-exams');
    }
    public function adminSettings(){
        return view('index.admin-settings');
    }
}
