<?php

namespace App\Http\Controllers\Misc;

use App\Models\InstitutionSetting;
use App\Models\VerifyUser;
use App\Models\VerifyViaSms;
use App\Notifications\VerifyEmailNoty;
use App\Permission;
use App\Role;
use App\utilities\Utility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\School;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Webpatser\Uuid\Uuid;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
use Illuminate\Support\Facades\Auth;
use App\User;

class MiscController extends Controller
{
    public function regSchool(Request $request){
        if($request->ajax()){

              $school = new School();
              $school->email = $request->email;
              $school->contact = $request->contact;
              $school->alt_contact = $request->alt_contact;
              $school->name = $request->name;
              $school->location = $request->location;
              $temp = $school->id = (string) Uuid::generate();
                $school->save();
                //insert into institution settings
            $settings = new InstitutionSetting();
            $settings->name = $school->name;
            $settings->school_id = $temp;
            $settings->contact_phone = $school->contact;
            $settings->contact_email = $school->email;
            $settings->save();
              //create roles for the schools and attach roles

              $role_names = ['admin','accountant','guardian','teacher','student'];
              foreach ($role_names as $name){
                  DB::table('roles')->insert([
                      'name'=>$name,
                      'school_id'=> $temp,
                      'display_name'=>title_case($name)
                  ]);
              }
              $permissions = Permission::all();

              $role = Role::where('school_id',$temp)
                  ->where('name', 'admin')->firstOrFail();
              $role->perms()->sync(
                  $permissions->pluck('id')->all()
              );
              //
              $admin = Role::where('school_id',$temp)
                  ->where('name','admin')->first();
              $user = new User();
              try{
                  $user->name =  $request['fname'].' '.$request['lname'];
                  $user->password = bcrypt($request->password);
                  $user->email = $request->user_mail;
                  $user->school_id = $temp;
                  $user->lname = $request->lname;
                  $user->fname = $request->fname;
                  $user->role_id = $admin->id;
                  $user->slug = str_replace('-','',(string) Uuid::generate());
                  $user->contact = $request->user_contact;
                  $user_save = $user->save();
              }catch (\Exception $e){
                  return response()->json([
                      'status'=>'error',
                      'message'=>'A user with the same email already exists'
                  ]);
              }
              $user->attachRole($admin);
              $token = str_random(40);
              VerifyUser::create([
                  'user_id' => $user->id,
                  'token' => $token
              ]);
              //SMS::send()
            try{
                Notification::send($user ,new VerifyEmailNoty($user));
            }catch (\Exception $e){
                return response()->json([
                    'message'=>'error',
                    'response'=>'email could not be sent'
                ],501);
            }
              if($school->save() && $user_save )
                  return $user->id;
        }

    }

    public function login(Request $request){
        if($request->ajax()){
            Auth::logout();
            $checks = ['admin','accountant','teacher'];
           try{
               $url = null;
               if($request['format'] == 'admin') {
                   if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']]
                       , $request['remember'])) {
                       if(\auth()->user()->verified == 0){
                           \auth()->logout();
                           return response()->json([
                               'status'=>'unverified',
                               'message'=>'Email Not Verified'
                           ]);
                       }else{
                            $user = User::where('email',$request->email)->first();
                            if($user->hasRole('super')){
                                $url = '/super';
                            }else if($user->hasRole('admin')){
                                $url = '/admin';
                            }else if($user->hasRole('teacher')){
                                $url = '/teacher';
                            }
                            else{
                                $url = '/dashboard';
                            }
                           /*$role = User::where('email', $request['email'])->pluck('role_id')->first();
                           $url = '/' . Role::where('id', $role)->pluck('name')->first();*/
                       }
                   }
               }else{
                   if(Auth::guard('students')
                       ->attempt(['username'=>$request['username'],'password'=>$request['password']],
                           $request['remember'])){
                       auth()->shouldUse('students');
                      $url = '/student';
                   }
               }
                   if($url != null){
                       return response()->json([
                           'status'=>'success',
                           'url'=>$url
                       ]);
                   }
                   return response()->json([
                       'status'=>'error',
                   ]);

           }catch(\Exception $e){
               return response()->json([
                   'status'=>'error',
               ]);
           }
        }
    }

    public function logout(){
        $data = array_keys(config('auth.guards'));
        foreach($data as $item){
            if(auth()->guard($item)->check())
                Auth::guard($item)->logout();
        }
        return redirect('/');
    }

    /**
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can login.";
            }
        }else{
            return redirect('/login')->with('error', "Sorry your email cannot be identified.");
        }

        return redirect('/login')->with('success', $status);
    }



    //sms verification
    protected function sendSmsVerification($to,$code){
        $configuration = new BasicAuthConfiguration('myeschoolgh', 'Ghana2@sch', 'http://api.infobip.com/');
        $client = new SendMultipleTextualSmsAdvanced($configuration);
        $destination = new Destination();
        $destination->setTo($to);
        // $destination->setMessageId('Hi Justice');
        $message = new Message();
        $message->setDestinations([$destination]);
        $message->setFrom('Myeschool');
        $message->setTo($to);
        $message->setText($code);
        $message->setNotifyContentType('application/json');
        //$message->setCallbackData('');
        $requestBody = new SMSAdvancedTextualRequest();
        $requestBody->setMessages([$message]);
        $response = $client->execute($requestBody);
        return $response;
    }
    public function verifySMS(Request $request){
        $verifi_sms = VerifyViaSms::where('user_id',$request->id)->where('code',$request->code)->first();
        if(isset($verifi_sms)){
            User::where('id',$request->id)->update([
                'verified'=>1
            ]);
            $request->session()->flash('success', 'Verified Successfully');
        }else{
            return response()->json([
                'message'=>'incorrect data'
            ],501);
        }
        return response()->json([
            'message'=>'success'
        ],200);
    }
}
