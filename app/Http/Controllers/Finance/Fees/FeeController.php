<?php

namespace App\Http\Controllers\Finance\Fees;

use App\Models\AcademicYear;
use App\Models\InstitutionSetting;
use App\Models\NewClass;
use App\Models\PaymentMethod;
use App\Models\Student;
use App\Models\StudentReceipt;
use App\Models\StudentTransaction;
use App\Models\StudentWallet;
use App\Models\Term;
use App\Models\Transaction;
use App\utilities\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Webpatser\Uuid\Uuid;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_methods = PaymentMethod::where('school_id',auth()->user()->school_id)->get();
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        return view('finance.fees.index',[
            'all_methods'=>$all_methods,
            'settings'=>$settings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getStudentForClass($id){
        $class = NewClass::with(['students'])->find($id);
        return $class->students;
    }
    public function getStudentWallet($student_id){
        $wallet = StudentWallet::where('school_id',auth()->user()->school_id)
            ->where('student_id',$student_id)
            ->first();
        return $wallet;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'payment_method'=>'required',
            'date'=>'required',
            'amount'=>'required'
        ];
        //debit student account;
        $checkAvailability = StudentWallet::where('student_id',$request->student_id)->first();
        $previous_balance = 0;
        if(isset($checkAvailability)){
            $previous_balance = Utility::getStudentWallet($request->student_id);
            StudentWallet::where('student_id',$request->student_id)
                ->update([
                    'balance'=>(float) $previous_balance + (float) $request->amount
                ]);
        }else{
            $studentWallet = new StudentWallet();
            $studentWallet->balance = $request->amount;
            $studentWallet->school_id = auth()->user()->school_id;
            $studentWallet->class_id = $request->class_id;
            $studentWallet->student_id = $request->student_id;
            $studentWallet->save();
        }
        //
        $activePeriod = Utility::getActiveTermYear();
        //generate a receipt for the transaction
        $receipt = new StudentReceipt();
        $receipt->school_id = auth()->user()->school_id;
        $receipt->user_id = auth()->user()->id;
        $receipt->receipt_no = str_random(8);
        $receipt->term_id = $activePeriod['active_term'];
        $receipt->academic_year_id = $activePeriod['active_year'];
        $receipt->amount = (float) $request->amount;
        $receipt->owing =(float) $previous_balance +(float) $request->amount;
        $receipt->save();
        //save transaction
        $this->validate($request,$rules);
        $transaction = new Transaction();
        $transaction->slug = str_replace('-','',(string) Uuid::generate());
        $transaction->user_id = auth()->user()->id;
        $transaction->student_id = $request->student_id;
        $transaction->class_id = $request->class_id;
        $transaction->receipt_id = $receipt->id;
        $transaction->school_id = auth()->user()->school_id;
        $transaction->date = Carbon::parse($request->date)->toDateString();
        $transaction->transaction_no = $request->transaction_no;
        $transaction->payment_method_id = $request->payment_method;
        $transaction->amount = $request->amount;
        $transaction->category = 'income';
        $transaction->term_id = $activePeriod['active_term'];
        $transaction->academic_year_id = $activePeriod['active_year'];
        $transaction->save();

        //receipt =
        $finalReceipt = [
            'user'=>auth()->user()->name,
            'receipt_no'=>$receipt->receipt_no,
            'term'=>Term::find($activePeriod['active_term'])->display_name,
            'year'=>AcademicYear::find($activePeriod['active_year'])->name,
            'amount'=>number_format($request->amount,2),
            'owing'=>number_format(((float)$previous_balance + (float) $request->amount) * -1,2),
            'class'=>NewClass::find($request->class_id)->name,
            'student'=>Student::find($request->student_id)->name,
            'date'=>Carbon::now()->toDayDateTimeString(),
        ];
        if($previous_balance < 0){
            $finalReceipt['is_owing'] = true;
        }else{
            $finalReceipt['is_owing'] = false;
        }
        return response()->json([
            'status'=>'success',
            'receipt'=>$finalReceipt
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
