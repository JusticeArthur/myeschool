<?php

namespace App\Http\Controllers\Finance\Income;

use App\Models\Income;
use App\Models\IncomeCategory;
use App\utilities\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = IncomeCategory::where('school_id',auth()->user()->school_id)->get();
        $incomes = Income::where('school_id',auth()->user()->school_id)->get();
        return view('finance.incomes.income',[
            'incomes'=>$incomes,
            'categories'=>$categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current = Utility::getActiveTermYear();
        $data['income_category_id'] = $request['income_category_id'];
        $data['amount'] = $request['amount'];
        $data['school_id'] = auth()->user()->school_id;
        $data['user_id'] = auth()->user()->id;
        $data['date'] = Carbon::parse($request['date'])->toDateString();
        $data['term_id'] = $current['active_term'];
        $data['academic_year_id'] = $current['active_year'];
        $data['description'] = $request['description'];
        $data['quantity'] = $request['quantity'] <= 0 || !isset($request['quantity']) ? 1 : $request['quantity'];
        $data['total'] = $data['amount'] * $data['quantity'];
        $expense = Income::create($data);
        return Income::where('id',$expense->id)->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $current = Utility::getActiveTermYear();
        $data['income_category_id'] = $request['income_category_id'];
        $data['amount'] = $request['amount'];
        $data['date'] = Carbon::parse($request['date'])->toDateString();
        $data['description'] = $request['description'];
        $data['quantity'] = $request['quantity'] <= 0 || !isset($request['quantity']) ? 1 : $request['quantity'];
        $data['total'] = $data['amount'] * $data['quantity'];
        Income::where('id',$id)->update($data);
        return Income::where('id',$id)->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Income::find($id)->delete();
    }
}
