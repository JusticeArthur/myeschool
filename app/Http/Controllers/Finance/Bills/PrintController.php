<?php

namespace App\Http\Controllers\Finance\Bills;

use App\Models\AcademicYear;
use App\Models\Fee;
use App\Models\NewClass;
use App\Models\Student;
use App\Models\StudentWallet;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrintController extends Controller
{
    public function index(){
        $classes = NewClass::where('school_id',auth()->user()->school_id)->get();
        $years = AcademicYear::where('school_id',auth()->user()->school_id)->get();
        return view('finance.bills.print',[
            'classes'=>$classes,
            'years'=>$years
        ]);
    }
    public function fetchBills(Request $request){
        $bills = Fee::where('school_id',auth()->user()->school_id)
            ->where('class_id',$request->class_id)
            ->where('term_id',$request->term_id)
            ->where('academic_year_id',$request->academic_year_id)
            ->get();
        $sum = $bills->sum('item.value');
        //get student wallet
        $wallet = StudentWallet::where('school_id',auth()->user()->school_id)
            ->where('student_id',$request->student_id)
            ->first();
        return [
            'bills'=>$bills,
            'sum'=>number_format($sum,2),
            'wallet'=>$wallet,
            'student'=>Student::find($request->student_id),
            'latestTransaction'=>Transaction::where('student_id',$request->student_id)->orderBy('created_at','desc')->first()
        ];
    }
}
