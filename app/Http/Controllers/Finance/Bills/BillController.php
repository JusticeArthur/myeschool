<?php

namespace App\Http\Controllers\Finance\Bills;

use App\Models\Fee;
use App\Models\FeeApproval;
use App\Models\NewClass;
use App\Models\StudentBill;
use App\Models\StudentWallet;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = StudentBill::where('school_id',auth()->user()->school_id)
            ->get();
        //echo $bills->toJSON();
        //dd();
        return view('finance.bills.index',[
            'bills'=>$bills
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return StudentBill
     */
    public function store(Request $request)
    {
        //first check for approvals before billing students
        $approval = FeeApproval::where('school_id',auth()->user()->school_id)
            ->where('approved',true)
            ->where('class_id',$request->class_id)
            ->where('academic_year_id',$request->academic_year_id)
            ->where('term_id',$request->term_id)
            ->first();
        if(isset($approval)){
            //get amount for fee records
            $fees = Fee::where('school_id',auth()->user()->school_id)
                ->where('class_id',$request->class_id)
                ->where('academic_year_id',$request->academic_year_id)
                ->where('term_id',$request->term_id)
                ->get();
            //dd($fees->toArray());
            $sum = $fees->sum(function ($item){
                return ($item['item']['value']);
            });
            //get class bills
            $update = StudentBill::where('school_id',auth()->user()->school_id)
                ->where('class_id',$request->class_id)
                ->where('academic_year_id',$request->academic_year_id)
                ->where('term_id',$request->term_id)
                ->first();
            $classes = NewClass::with(['students'])->find($request->class_id);
            try{
                $bill = new StudentBill();
                $bill->class_id = $request->class_id;
                $bill->school_id = auth()->user()->school_id;
                $bill->academic_year_id = $request->academic_year_id;
                $bill->term_id = $request->term_id;
                $bill->user_id = auth()->user()->id;
                $bill->amount = $sum;
                $bill->save();
            }catch(QueryException $q){
                return response()->json([
                    'status'=>'error'
                ],501);
            }catch(\Exception $e){
                return response()->json([
                    'status'=>'error'
                ],500);
            }
            //credit student upon new amount
            foreach ($classes->students as $student){
                //get student previous balance
                $balance = 0;
                $previousBalance = StudentWallet::where('school_id',auth()->user()->school_id)
                    ->where('student_id',$student->id)
                    ->first();
                if(isset($previousBalance)){
                    //update balance
                    $balance = $previousBalance->balance;
                }
                if(isset($previousBalance)){
                    StudentWallet::where('school_id',auth()->user()->school_id)
                        ->where('student_id',$student->id)->update([
                            'balance'=>$balance-$sum
                        ]);
                }else{
                    $student_wallet  = new StudentWallet();
                    $student_wallet->school_id = auth()->user()->school_id;
                    $student_wallet->student_id = $student->id;
                    $student_wallet->balance = $balance - $sum;
                    $student_wallet->class_id = $request->class_id;
                    $student_wallet->save();
                }

            }
            return StudentBill::where('id',$bill->id)->first();
        }else{
            return response()->json([
                'status'=>'error'
            ],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bill = StudentBill::find($id);
        $amount = $bill->amount;
        $class_id = $bill->class_id;
        $student_wallets = StudentWallet::where('school_id',auth()->user()->school_id)
            ->where('class_id',$class_id)
            ->get();
        foreach ($student_wallets as $wallet){
            $balance = 0;
            $previousBalance = StudentWallet::where('school_id',auth()->user()->school_id)
                ->where('class_id',$class_id)
                ->where('student_id',$wallet->student_id)
                ->first();
            if(isset($previousBalance)){
                //update balance
                $balance = $previousBalance->balance;
            }
            //update student balance
            StudentWallet::where('school_id',auth()->user()->school_id)
                //->where('class_id',$class_id)
                ->where('student_id',$wallet->student_id)
                ->update([
                    'balance'=>$balance + $amount
                ]);
        }
        $bill->delete();
        return response()->json(['status'=>'success'],200);
    }
}
