<?php

namespace App\Http\Controllers\Finance\Expense;

use App\Models\ExpenseCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpenseCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories  = ExpenseCategory::where('school_id',auth()->user()->school_id)->get();
        return view('finance.expenses.categories',[
            'categories'=>$categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['name'] = $request->name;
        $data['category'] = $request->category;
        $data['school_id'] = auth()->user()->school_id;
        $data['user_id'] = auth()->user()->id;
        $category = null;
        try{
            $category = ExpenseCategory::create($data);
        }catch (QueryException $q){
            return response()->json([
                'message'=>"Duplicate Entry Detected for {$request->name}"
            ],501);
        }catch (\Exception $e){
            return $request->json([
               'message'=>'Sorry an error occurred'
            ],501);
        }
        return ExpenseCategory::where('id',$category->id)->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            ExpenseCategory::where('id',$id)->update([
                'name'=>$request->name,
                'category'=>$request->category
            ]);
        }catch (QueryException $q){
            return response()->json([
                'message'=>"Duplicate Entry Detected for {$request->name}"
            ],501);
        }catch (\Exception $e){
            return $request->json([
                'message'=>'Sorry an error occurred'
            ],501);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ExpenseCategory::where('id',$id)->delete();
    }

    public function bulkDelete(Request $request){
        foreach ($request->data as $item){
            ExpenseCategory::where('id',$item['id'])->delete();
        }
        return ExpenseCategory::where('school_id',auth()->user()->school_id)->get();
    }
}
