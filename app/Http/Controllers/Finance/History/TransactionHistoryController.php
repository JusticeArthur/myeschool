<?php

namespace App\Http\Controllers\Finance\History;

use App\Models\Student;
use App\Models\StudentTransaction;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionHistoryController extends Controller
{
    public function index(){
        $transactions = Transaction::where('school_id',auth()->user()->school_id)
            ->orderby('created_at','desc')
            ->get();
        /*echo $transactions->toJSON() ;
        dd();*/
        return view('finance.history.index',[
            'transactions'=>$transactions
        ]);
    }
    public function filterResults(Request $request){
        $category = $request->category == 'expense'? false:true;
        if($request->type === 'students'){
            $year = $request->academic_year_id == 'all'? false:true;
            $term = $request->term_id == 'all'? false:true;
            $class = $request->class_id == 'all'? false:true;
            $student_name = isset($request->name)?true:false;
            $transactions = Transaction::where('school_id',auth()->user()->school_id)
                ->when($year,function ($query) use ($request){
                    return $query->where('academic_year_id',$request->academic_year_id);
                })
                ->when($term,function ($query) use ($request){
                    return $query->where('term_id',$request->term_id);
                })
                ->when($class,function ($query) use ($request){
                    return $query->where('class_id',$request->class_id);
                })
                ->when($category,function ($query) use ($request){
                    return $query->where('category','income');
                },function ($query){
                    return $query->where('category','expense');
                })
                ->when($student_name,function ($query) use ($request){
                    return $query->whereHas('student',function ($query) use ($request){
                        $query->Where('full_name','like',"%{$request->name}%")
                            ->orwhere('lname','like',"%{$request->name}%")
                            ->orWhere('fname','like',"%{$request->name}%")
                            ->orWhere('mname','like',"%{$request->name}%");

                    });
                })
                ->get();

            return $transactions;
        }else{
            dd($request);
        }
    }
}
