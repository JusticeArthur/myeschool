<?php

namespace App\Http\Controllers\Finance;

use App\Models\NewClass;
use App\Models\Student;
use App\Models\StudentWallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function index(){
        $students = StudentWallet::where('school_id',auth()->user()->school_id)
            ->with(['student','student.guardians'])
            ->where('balance','<',0)
            ->orderBy('balance')
            ->get();
        $classes = NewClass::where('school_id',auth()->user()->school_id)->get();
        /*echo $students->toJson();
        dd();*/
        return view('finance.notifications.index',[
            'students'=>$students,
            'classes'=>$classes
        ]);
    }
    public function searchStudent(Request $request){
        $search = null;
        $class_id = isset($request['class_id']) && $request['class_id'] != 'all' ? true:false;
        if(isset($request->data) || $class_id){
            $all_students = StudentWallet::search($request->data)
                ->where('school_id',auth()->user()->school_id)
                ->with(['student','student.guardians'])
                ->where('balance','<',0)
                ->orderBy('balance')
                ->get();
            if($class_id){
                $filtered = $all_students->filter(function ($value, $key) use($request) {
                    return $value->student->class_id == $request['class_id'];
                });
                $search = $filtered->values()->all();
            }else{
                $search = $all_students;
            }
        }else{
            $search = StudentWallet::where('school_id',auth()->user()->school_id)
                ->with(['student','student.guardians'])
                ->where('balance','<',0)
                ->orderBy('balance')
                ->get();
        }
        return $search;
    }
}
