<?php

namespace App\Http\Controllers\Finance\Payments;

use App\Models\PaymentMethod;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $methods = PaymentMethod::where('school_id',auth()->user()->school_id)->get();
        //echo $methods->toJSON();
        //,dd();
        return view('finance.payments.categories',[
            'methods'=>$methods
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $method = new PaymentMethod();
            $method->name = $request->name;
            $method->school_id = auth()->user()->school_id;
            $method->user_id = auth()->user()->id;
            $method->save();
        }catch(QueryException $q){
            return response()->json([
                'status'=>'error'
            ],501);
        }catch(\Exception $e){
            return response()->json([
                'status'=>'error'
            ],500);
        }
        return PaymentMethod::find($method->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            PaymentMethod::where('id',$id)->update([
                'name'=>$request->name
            ]);
        }catch(QueryException $q){
            return response()->json([
                'status'=>'error'
            ],501);
        }catch(\Exception $e){
            return response()->json([
                'status'=>'error'
            ],500);
        }
        return PaymentMethod::find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PaymentMethod::find($id)->delete();
        return response()->json([
            'status'=>'success'
        ],200);
    }
}
