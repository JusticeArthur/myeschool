<?php

namespace App\Http\Controllers\Home;

use App\Models\SiteMenu;
use App\Models\SiteSubMenu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function search(Request $request){
        $main =  SiteMenu::search($request->search)->distinct()->take(3)->get()->toArray();
        $sub = SiteSubMenu::search($request->search)->distinct()->take(3)->get()->toArray();
        $results = array_merge($main,$sub);
        return $results;
    }
}
