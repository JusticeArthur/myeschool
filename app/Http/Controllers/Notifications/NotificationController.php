<?php

namespace App\Http\Controllers\Notifications;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function index(){
        $data = [
            'count'=>count(auth()->user()->unreadNotifications),
            'noty'=>auth()->user()->unreadNotifications
        ];
        return view('notifications.index',[
            'notifications'=>json_encode($data)
        ]);
    }
}
