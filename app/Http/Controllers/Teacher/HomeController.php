<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Expense;
use App\Models\InstitutionSetting;
use App\utilities\Utility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function home(){
        $day = Carbon::now()->day;
        $todayExpenses = DB::table('expenses')->where('school_id',auth()->user()->school_id)
            ->whereDay('created_at',$day)
            ->sum('total');
        $activeTermYear = Utility::getActiveTermYear();
        $termExpenses = DB::table('expenses')->where('school_id',auth()->user()->school_id)
            ->where('term_id',$activeTermYear['active_term'])
            ->sum('total');
        $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
        return view('teacher.main',
            [
                'settings'=>$settings,
                'todayExpenses'=>number_format($todayExpenses,2),
                'termExpenses'=>number_format($termExpenses,2)
            ]);
    }
}
