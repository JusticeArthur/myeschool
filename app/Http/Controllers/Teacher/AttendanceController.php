<?php

namespace App\Http\Controllers\Teacher;

use App\Models\AcademicYear;
use App\Models\Attendance;
use App\Models\ExamApproval;
use App\Models\NewClass;
use App\Models\Student;
use App\Models\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    public function attendanceInfo($id){
        $data = DB::table('attendances')->where('attendances.school_id',auth()->user()->school_id)
            ->where('attendances.date',$id)
            ->leftJoin('students','attendances.student_id','students.id')
            ->Join('classes','attendances.class_id','classes.id')
            ->select('students.fname','students.lname',
                'attendances.id','attendances.status',
                'attendances.remarks')
            ->get();
        return view('teacher.attendance.details',[
            'details'=>$data
        ]);
    }

    public function showMark(){
        if(auth()->user()->hasRole(['admin'])){
            $classes = $classes = NewClass::where('school_id',auth()->user()->school_id)
                ->get();
            return view('teacher.attendance.mark',[
                'classes'=>$classes
            ]);
        }else{
            $teacher_id = auth()->user()->teacher->id;
            $classes = NewClass::where('school_id',auth()->user()->school_id)
                ->where('teacher_id',$teacher_id)
                ->get();
            return view('teacher.attendance.mark',[
                'classes'=>$classes
            ]);
        }

    }
    public function getStudentsToMarkAttendance($id){
        $students = Student::where('school_id',auth()->user()->school_id)
            ->where('class_id',$id)
            ->get();
        return $students;
    }
    public function markAttendance(Request $request){
        $user = auth()->user();
        $active_year = AcademicYear::where('school_id',$user->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $check = ExamApproval::where('school_id',$user->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->first();
        if(isset($check) && $check->approved){
            return response()->json([
                'status'=>'error'
            ],401);
        }else{
            $now = Carbon::now()->toDateString();
            try{
                foreach($request->select as $key=>$value){
                    $attend = new Attendance();
                    $attend->school_id = auth()->user()->school_id;
                    $attend->date = $now;
                    $attend->status = $value['value'];
                    $attend->remarks = $request->remarks[$key]['value'];
                    $attend->student_id = $value['name'];
                    $attend->teacher_id = auth()->user()->hasRole(['admin'])?auth()->user()->id : auth()->user()->teacher->id;
                    if(auth()->user()->hasRole(['admin'])){
                        $attend->marked_by = 'admin';
                    }
                    $attend->class_id = $request->class_id;
                    $attend->academic_year_id = $active_year->id;
                    $attend->term_id = $active_term->id;

                    $data = DB::table('attendances')->where('school_id',auth()->user()->school_id)
                        ->where('class_id',$request->class_id)
                        ->where('student_id',$value['name'])
                        ->where('date',$now)
                        ->first();
                    if(isset($data)){
                        DB::table('attendances')->where('school_id',auth()->user()->school_id)
                            ->where('class_id',$request->class_id)
                            ->where('student_id',$value['name'])
                            ->where('student_id',$value)
                            ->where('date',$now)
                            ->update([
                                'status'=>$value['value'],
                                'remarks'=> $request->remarks[$key]['value'],
                            ]);
                        continue;
                    }
                    $attend->save();
                }
                return response()->json([
                    'message'=>'Success'
                ]);
            }catch (\Exception $e){
                return response()->json([
                    'message'=>'error'
                ]);
            }
        }

    }
    public function showView(){
        if(auth()->user()->hasRole(['admin'])){
            $classes = NewClass::where('school_id',auth()->user()->school_id)
                ->get();
            return view('teacher.attendance.view',[
                'classes'=>$classes
            ]);
        }else{
            $teacher_id = auth()->user()->teacher->id;
            $classes = NewClass::where('school_id',auth()->user()->school_id)
                ->where('teacher_id',$teacher_id)
                ->get();
            return view('teacher.attendance.view',[
                'classes'=>$classes
            ]);
        }

    }
    public function getAttendanceDates($id){
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $dates = Attendance::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->where('class_id',$id)
            ->select('date')
            ->groupBy('date')
            ->get();
        return $dates;
    }
    public function getAttendanceDateStudent(Request $request){
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $students = DB::table('attendances')
            ->where('attendances.school_id',auth()->user()->school_id)
            ->where('attendances.academic_year_id',$active_year->id)
            ->where('attendances.term_id',$active_term->id)
            ->where('attendances.class_id',$request->class_id)
            ->where('attendances.date',$request->date)
            ->leftJoin('students','students.id','attendances.student_id')
            ->select('students.fname','students.lname','students.mname','students.username','attendances.*')
            ->get();
        return $students;
    }
/////////////////////////////////////////
    public function showAttendanceInfo($id){
        $data = DB::table('attendances')->where('attendances.id',$id)
            ->leftJoin('students','attendances.student_id','students.id')
            ->select('students.fname','students.lname',
                'attendances.id','attendances.status',
                'attendances.remarks')
            ->first();
        return response()->json($data);
    }

    public function updateAttendanceInfo(Request $request){
            DB::table('attendances')
                ->where('id',$request->id)
                ->update([
                    'status'=>$request->status,
                    'remarks'=>$request->remarks
                ]);
            return response()->json([
                'message'=>'success'
            ],200);
    }


}
