<?php

namespace App\Http\Controllers\Teacher;

use App\Models\AcademicYear;
use App\Models\Assessment;
use App\Models\ClassCourse;
use App\Models\ExamApproval;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $classes = DB::table('assessments')
            ->where('assessments.school_id',auth()->user()->school_id)
            ->where('assessments.academic_year_id',$active_year->id)
            ->where('assessments.term_id',$active_term->id)
            ->where('assessments.teacher_id',auth()->user()->teacher->id)
            ->leftJoin('classes','assessments.class_id','classes.id')
            ->select('classes.id','classes.name','classes.code')
            ->distinct()
            ->get();
        return view('teacher.marks.view',[
            'classes'=>$classes
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //check if the results has been approved
        $user = auth()->user();
        $active_year = AcademicYear::where('school_id',$user->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('school_id',$user->school_id)
            ->where('status','active')->first();

        $check = ExamApproval::where('school_id',$user->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->first();
        $teacher_cred = $user->teacher;
       //$unique = $teacher_cred->classes->unique();
       //dd($unique->values()->all());

        $classes = DB::table('class_courses')->where('class_courses.academic_year_id',$active_year->id)
            ->where('class_courses.term_id',$active_term->id)
            ->where('class_courses.teacher_id',$teacher_cred->id)
            ->leftJoin('classes','class_courses.class_id','classes.id')
            ->select('classes.id','classes.name','classes.code')
            ->distinct()
            ->get();

        return view('teacher.marks.add',[
            'classes'=>$classes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $active_year = AcademicYear::where('school_id',$user->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('school_id',$user->school_id)
            ->where('status','active')->first();

        $check = ExamApproval::where('school_id',$user->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->first();
        if(isset($check) && $check->approved){
            return response()->json([
                'message'=>'warning'
            ],401);
        }else{
            foreach($request->data as $score){
                $assessment = new Assessment();
                $assessment->category = $request->category;
                $assessment->school_id = auth()->user()->school_id;
                $assessment->score = $score['value'] == null ? 0:$score['value'];
                $assessment->max = $request->max_score;
                if($request->category == 'class'){
                    $assessment->name = title_case($request->name);
                }else{
                    $assessment->name = title_case('examination');
                }
                $assessment->student_id = $score['name'];
                $assessment->course_id = $request->course_id;
                $assessment->class_id = $request->class_id;
                $assessment->teacher_id = auth()->user()->teacher->id;
                $assessment->academic_year_id = $active_year->id;
                $assessment->term_id = $active_term->id;
                $assessment->save();
            }
        }
        return response()->json([
            'message'=>'success'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
