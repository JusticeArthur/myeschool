<?php

namespace App\Http\Controllers\Teacher;

use App\Models\AcademicYear;
use App\Models\Assessment;
use App\Models\ClassCourse;
use App\Models\ExamApproval;
use App\Models\ExamComments;
use App\Models\InstitutionSetting;
use App\Models\NewClass;
use App\Models\Remark;
use App\Models\Report;
use App\Models\Student;
use App\Models\Term;
use App\utilities\Utility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MiscController extends Controller
{
   /* public $active_year;
    public $active_term;
    function __construct()
    {
        $this->active_year = Utility::getActiveTermYear()['active_year'];
        $this->active_term = Utility::getActiveTermYear()['active_term'];
    }*/

    public function getClassTeacherCourses($id){

        $teacher_cred = auth()->user()->teacher;
        $data = DB::table('class_courses')
            ->where('class_courses.teacher_id',$teacher_cred->id)
            ->where('class_courses.class_id',$id)
            ->leftJoin('courses','class_courses.course_id','courses.id')
            ->select('courses.id','courses.name','courses.code')
            ->distinct()
            ->get();
        $students = Student::where('school_id',auth()->user()->school_id)
            ->where('class_id',$id)
            ->get();
        return response()->json([
            'data'=>$data,
            'students'=>$students
        ],200);

    }

    public function getClassCourse(Request $request){
        $category = $request->category;
        $courses = DB::table('assessments')
            ->where('assessments.school_id',auth()->user()->school_id)
            ->where('assessments.teacher_id',auth()->user()->teacher->id)
            ->when($category, function ($query, $category) {
                return $query->where('assessments.category', $category);
            })
            ->where('assessments.class_id',$request->class_id)
            ->leftJoin('courses','assessments.course_id','courses.id')
            ->select('courses.id','courses.name','courses.code')
            ->distinct()
            ->get();
        return $courses;
    }

    public function getClassCourseNames(Request $request){
        $category = $request->category;
        $flag = $request->flag;
        $names = DB::table('assessments')
            ->where('assessments.school_id',auth()->user()->school_id)

            ->where('assessments.teacher_id',auth()->user()->teacher->id)
            ->when($category, function ($query, $category) {
                return $query->where('assessments.category', $category);
            })
            ->when($flag, function ($query, $flag) {
                return $query;
            },function($query){
                return $query->where('assessments.submitted',false);
            })
            ->where('assessments.class_id',$request->class_id)
            ->where('assessments.course_id',$request->course_id)
            ->select('assessments.name','assessments.category')
            ->distinct()
            ->get();
        return $names;
    }
    public function getClassCourseNameStudents(Request $request){
        $students = DB::table('assessments')
            ->where('assessments.school_id',auth()->user()->school_id)
            ->where('assessments.teacher_id',auth()->user()->teacher->id)
            ->where('assessments.category',$request->category)
            ->where('assessments.class_id',$request->class_id)
            ->where('assessments.course_id',$request->course_id)
            ->where('assessments.name',$request->name)
            ->leftJoin('students','assessments.student_id','students.id')
            ->distinct()
            ->get();
        return $students;
    }

    public function massUpdate(Request $request){
        foreach($request->data as $item){
            Assessment::where('school_id',auth()->user()->school_id)
                ->where('teacher_id',auth()->user()->teacher->id)
                ->where('category',$request->status['category'])
                ->where('class_id',$request->status['class_id'])
                ->where('course_id',$request->status['course_id'])
                ->where('name',$request->status['name'])
                ->where('student_id',$item['name'])
                ->update([
                    'score'=>$item['value']
                ]);
        }
        return response()->json([
            'message'=>'Updated Successfully'
        ],200);
    }

    public function massDelete(Request $request){
        Assessment::where('school_id',auth()->user()->school_id)
            ->where('teacher_id',auth()->user()->teacher->id)
            ->where('category',$request->status['category'])
            ->where('class_id',$request->status['class_id'])
            ->where('course_id',$request->status['course_id'])
            ->where('name',$request->status['name'])
            ->delete();
        Session::flash('success', $request->status['name'].' Deleted Successfully');
        return response()->json([
            'message'=>'Deleted Successfully'
        ]);
    }

    //submitting the results per assessments
    public function showSubmission(){
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $classes = DB::table('assessments')
            ->where('assessments.school_id',auth()->user()->school_id)
            ->where('assessments.academic_year_id',$active_year->id)
            ->where('assessments.term_id',$active_term->id)
            ->where('assessments.teacher_id',auth()->user()->teacher->id)
            ->leftJoin('classes','assessments.class_id','classes.id')
            ->select('classes.id','classes.name','classes.code')
            ->distinct()
            ->get();
        return view('teacher.marks.submission',[
            'classes'=>$classes
        ]);
    }
    //submitting the assessments for each test
    public function submitAssessment(Request $request){
        $user = auth()->user();
        $active_year = AcademicYear::where('school_id',$user->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('school_id',$user->school_id)
            ->where('status','active')->first();

        $check = ExamApproval::where('school_id',$user->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->first();
        if(isset($check) && $check->approved){
            return response()->json([
                'message'=>'warning'
            ],401);
        }else{
            $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
            foreach($request->data as $item){
                Assessment::where('school_id',auth()->user()->school_id)
                    ->where('teacher_id',auth()->user()->teacher->id)
                    ->where('category',$item['name'])
                    ->where('class_id',$request->status['class_id'])
                    ->where('course_id',$request->status['course_id'])
                    ->where('name',$item['value'])
                    ->update([
                        'submitted'=>1
                    ]);
            }
            //populating reports table
            $submittedData = Assessment::where('school_id',auth()->user()->school_id)
                ->where('teacher_id',auth()->user()->teacher->id)
                ->where('class_id',$request->status['class_id'])
                ->where('course_id',$request->status['course_id'])
                ->where('submitted',true)
                ->select('student_id', DB::raw('SUM(score) as total_student_score'),
                    DB::raw('SUM(max) as total_scoreable'),
                    'category','academic_year_id','term_id','course_id','teacher_id','class_id')
                ->groupBy('category','student_id','academic_year_id','term_id','course_id','teacher_id','class_id')
                ->get();

            for ($i = 0; $i < 2;$i++){
                foreach ($submittedData as $report){
                    $reportData = Report::where('school_id',auth()->user()->school_id)
                        ->where('teacher_id',auth()->user()->teacher->id)
                        ->where('class_id',$report['class_id'])
                        ->where('subject_id',$report['course_id'])
                        ->where('student_id',$report['student_id'])
                        ->where('term_id',$report['term_id'])
                        ->where('academic_year_id',$report['academic_year_id'])
                        ->first();
                    if(isset($reportData)){
                        if($report['category'] == 'exam'){
                            Report::where('school_id',auth()->user()->school_id)
                                ->where('teacher_id',auth()->user()->teacher->id)
                                ->where('class_id',$report['class_id'])
                                ->where('subject_id',$report['course_id'])
                                ->where('student_id',$report['student_id'])
                                ->where('term_id',$report['term_id'])
                                ->where('academic_year_id',$report['academic_year_id'])
                                ->update([
                                    'exam_score'=>round($report['total_student_score']/$report['total_scoreable']*$settings['examination'],2),
                                ]);
                        }else{
                            Report::where('school_id',auth()->user()->school_id)
                                ->where('teacher_id',auth()->user()->teacher->id)
                                ->where('class_id',$report['class_id'])
                                ->where('subject_id',$report['course_id'])
                                ->where('student_id',$report['student_id'])
                                ->where('term_id',$report['term_id'])
                                ->where('academic_year_id',$report['academic_year_id'])
                                ->update([
                                    'class_score'=>round($report['total_student_score']/$report['total_scoreable']*$settings['assessment'],2)
                                ]);
                        }
                    }else{
                        if($report['category'] == 'class'){
                            $new_report = new Report();
                            $new_report->school_id = auth()->user()->school_id;
                            $new_report->class_score = round($report['total_student_score']/$report['total_scoreable']*$settings['assessment'],2);
                            $new_report->teacher_id = $report['teacher_id'];
                            $new_report->class_id = $report['class_id'];
                            $new_report->subject_id = $report['course_id'];
                            $new_report->academic_year_id = $report['academic_year_id'];
                            $new_report->student_id = $report['student_id'];
                            $new_report->term_id = $report['term_id'];
                            $new_report->save();
                        }else{
                            $new_report = new Report();
                            $new_report->school_id = auth()->user()->school_id;
                            $new_report->exam_score = round($report['total_student_score']/$report['total_scoreable']*$settings['examination'],2);
                            $new_report->teacher_id = $report['teacher_id'];
                            $new_report->class_id = $report['class_id'];
                            $new_report->subject_id = $report['course_id'];
                            $new_report->student_id = $report['student_id'];
                            $new_report->academic_year_id = $report['academic_year_id'];
                            $new_report->term_id = $report['term_id'];
                            $new_report->save();
                        }
                    }
                }
            }
            return response()->json([
                'message'=>'Submitted Successfully'
            ],200);
        }

    }
    public function generateReportsForClass(Request $request){
        $classes = null;
        if(auth()->user()->hasRole('admin')){
            $classes = NewClass::where('school_id',auth()->user()->school_id)->get();
        }else{
            $classes = NewClass::where('school_id',auth()->user()->school_id)->where('teacher_id',auth()->user()->teacher->id)->get();
        }
        $url = $request->segment(4);
        if(isset($url)){
            return view('teacher.report.student',[
                'classes'=>$classes
            ]);
        }else{
            return view('teacher.report.index',[
                'classes'=>$classes
            ]);
        }

    }
    public function getReportsForClass($id){
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $total_class_report = [];
        $reports = Report::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->where('class_id',$id)
            ->select('subject_id')
            ->groupBy('subject_id')
            ->get();
        foreach ($reports as $report){
            $temp = Report::where('school_id',auth()->user()->school_id)
                ->where('academic_year_id',$active_year->id)
                ->where('term_id',$active_term->id)
                ->where('class_id',$id)
                ->where('subject_id',$report->subject_id)
                ->get();
            array_push($total_class_report,['data'=>$temp->toArray(),'subject_id'=>$report->subject_id]);
        }
        $total_reports = collect($total_class_report);
        $final_report =[];
        foreach ($total_reports as $report){
            $all_reports_for_subject = collect($report['data'])->sortByDesc('score');
            array_push($final_report,['data'=>$all_reports_for_subject->values()->all(),'subject_id'=>$report['subject_id']]);
        }
        //calculate position
        foreach($final_report as $key=>$final){
            $rank = 1;
            $tie_rank = 0;
            $prev_score = -1;
            $count = 0;
            foreach ($final['data'] as $index=>$report){
               if($report['score'] != $prev_score){ //this score is not tie
                   $count = 0;
                   $prev_score = $report['score'];
                   $final_report[$key]['data'][$index]['position'] = $rank;
               }else{
                   $prev_score = $report['score'];
                   if($count++ == 0){
                       $tie_rank = $rank -1;
                   }
                   $final_report[$key]['data'][$index]['position'] = $tie_rank;
               }
                $rank ++;
            }
        }
        $students = DB::table('students')
            ->where('school_id',auth()->user()->school_id)
            ->where('class_id',$id)
            ->select('fname','lname','mname','id','username')
            ->get();
        $courses = DB::table('reports')
            ->where('reports.school_id',auth()->user()->school_id)
            ->where('reports.academic_year_id',$active_year->id)
            ->where('reports.term_id',$active_term->id)
            ->where('reports.class_id',$id)
            ->distinct()
            ->leftJoin('courses','reports.subject_id','courses.id')
            ->select('courses.*')
            ->get();
        return response()->json([
            'reports'=>$final_report,
            'students'=>$students,
            'courses'=>$courses
        ]);
    }
    public function cacheStudentId($id){
        $user = auth()->user();
        $active_year = AcademicYear::where('school_id',$user->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $student = Student::where('school_id',$user->school_id)
            ->where('username',$id)
            ->first();
        $reports = Report::where('school_id',$user->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->where('class_id',$student->class_id)
            ->where('student_id',$student->id)
            ->select('subject_id')
            ->groupBy('subject_id')
            ->get();
        $total_class_report = [];
        foreach ($reports as $report){
            $temp = Report::where('school_id',$user->school_id)
                ->where('academic_year_id',$active_year->id)
                ->where('term_id',$active_term->id)
                ->where('class_id',$student->class_id)
                ->where('subject_id',$report->subject_id)
                ->get();
            array_push($total_class_report,['data'=>$temp->toArray(),'subject_id'=>$report->subject_id]);
        }
        $total_reports = collect($total_class_report);
        $final_report =[];
        foreach ($total_reports as $report){
            $all_reports_for_subject = collect($report['data'])->sortByDesc('score');
            array_push($final_report,['data'=>$all_reports_for_subject->values()->all(),'subject_id'=>$report['subject_id']]);
        }
        //calculate position
        foreach($final_report as $key=>$final){
            $rank = 1;
            $tie_rank = 0;
            $prev_score = -1;
            $count = 0;
            foreach ($final['data'] as $index=>$report){
                if($report['score'] != $prev_score){ //this score is not tie
                    $count = 0;
                    $prev_score = $report['score'];
                    $final_report[$key]['data'][$index]['position'] = $rank;
                }else{
                    $prev_score = $report['score'];
                    if($count++ == 0){
                        $tie_rank = $rank -1;
                    }
                    $final_report[$key]['data'][$index]['position'] = $tie_rank;
                }
                $rank ++;
            }
        }
        $final_student_report = [];
        //get final student report
        foreach ($final_report as $key=>$final){
            foreach ($final['data'] as $index=>$report){
                if($report['student_id'] == $student->id){
                    array_push($final_student_report,[$key=>$report,'subject_id'=>$final['subject_id']]);
                }
            }
        }

        //dd($final_student_report);
        //getting the overall student data
        $all_reports = Report::where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->where('class_id',$student->class_id)
            ->select('student_id')
            ->groupBy('student_id')
            ->get();
        $all_reports_students = [];
        foreach ($all_reports as $report){
            $temp = Report::where('school_id',auth()->user()->school_id)
                ->where('academic_year_id',$active_year->id)
                ->where('term_id',$active_term->id)
                ->where('class_id',$student->class_id)
                ->where('student_id',$report->student_id)
                ->get();
          //  dd($temp);
            array_push($all_reports_students,['student_id'=>$report->student_id,
                'total'=>$temp->sum('exam_score')+$temp->sum('class_score'),'rank'=>0]);
        }
        $all_reports_students = collect($all_reports_students)->sortByDesc('total')->values()->all();
        //rank final positions for all subjects
        $rank = 1;
        $tie_rank = 0;
        $prev_score = -1;
        $count = 0;
        foreach($all_reports_students as $key=>$value){
                if($value['total'] != $prev_score){ //this score is not tie
                    $count = 0;
                    $prev_score = $value['total'];
                   // dd( $all_reports_students[$key]);
                    $all_reports_students[$key]['rank'] = $rank;
                }else{
                    $prev_score = $value['total'];
                    if($count++ == 0){
                        $tie_rank = $rank -1;
                    }
                    $all_reports_students[$key]['rank'] = $tie_rank;
                }
                $rank ++;
        }
        $courses = DB::table('reports')
            ->where('reports.school_id',auth()->user()->school_id)
            ->where('reports.academic_year_id',$active_year->id)
            ->where('reports.term_id',$active_term->id)
            ->where('reports.class_id',$student->class_id)
            ->distinct()
            ->leftJoin('courses','reports.subject_id','courses.id')
            ->select('courses.*')
            ->get();
        $settings = InstitutionSetting::where('school_id',$user->school_id)->first();
        $all_students = count(Student::where('class_id',$student->class_id)->get());
        $remarks = Remark::where('school_id',$user->school_id)->get();

        //check if comments already exists
        $check = ExamComments::where('student_id',$student->id)
            ->where('class_id',$student->studentClass->id)
            ->where('school_id',$user->school_id)
            ->where('term_id',$active_term->id)
            ->where('academic_year_id',$active_year->id)
            ->first();
        //check results approval
        $test1 = ExamApproval::where('school_id',$user->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->first();
        $results_approved = isset($test1) && $test1->approved ? true:false;
        return view('teacher.report.student.index',[
            'student'=>$student,
            'student_courses'=>json_encode($final_student_report),
            'overall_rank'=>json_encode($all_reports_students),
            'courses'=>$courses,
            'settings'=>$settings,
            'all_students'=>$all_students,
            'year'=>strtoupper($active_year->name),
            'term'=>strtoupper($active_term->display_name),
            'remarks'=>$remarks,
            'comments'=>$check,
            'admin'=>auth()->user()->hasRole('admin')?1:0,
            'approved'=>$results_approved? 1: 0
        ]);
    }

    public function saveStudentComment(Request $request){
        //first check if the results has been approved
        $activeYearTerm = Utility::getActiveTermYear();
        $checks = ExamApproval::where('school_id',auth()->user()->school_id)
            ->where('term_id',$activeYearTerm['active_term'])
            ->where('academic_year_id',$activeYearTerm['active_year'])
            ->first();
        if(isset($checks) && $checks->approved){
            return response()->json([
                'message'=>'results approved'
            ],405);
        }else{
            if(auth()->user()->hasRole(['admin','teacher'])){
                $comments = $request->all();
                $comments['academic_year_id'] = Utility::getActiveTermYear()['active_year'];
                $comments['term_id'] = Utility::getActiveTermYear()['active_term'];
                $comments['school_id'] = auth()->user()->school_id;


                //check if comments already exists
                $check = ExamComments::where('student_id',$request->student_id)
                    ->where('school_id',auth()->user()->school_id)
                    ->where('class_id',$request->class_id)
                    ->where('term_id',$comments['term_id'])
                    ->where('academic_year_id',$comments['academic_year_id'])
                    ->first();
                //check if the user is a teacher
                if(auth()->user()->hasRole('teacher')){
                    if(auth()->user()->teacher->id == $request->teacher_id){
                        $comments['head_teacher_id'] = auth()->user()->id;
                        if(isset($check)){
                            $check->teacher_comment = $request->teacher_comment;
                            $check->save();
                        }else{
                            ExamComments::create($comments);
                        }
                        return $comments;
                    }else{
                        return response()->json([
                            'message'=>'unauthorized'
                        ],401);
                    }
                }else{
                    $comments['head_teacher_id'] = auth()->user()->id;
                    if(isset($check)){
                        //$check->teacher_comment = $request->teacher_comment;
                        $check->head_teacher_comment = $request->head_teacher_comment;
                        $check->save();
                    }else{
                        ExamComments::create($comments);
                    }
                    return $comments;
                }

            }else{
                return response()->json([
                    'message'=>'unauthorized'
                ],401);
            }
        }

    }
}
