<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function welcome(){
        return view('welcome.index');
    }
    public function register(){
        return view('registration.index');
    }
    public function dashboard(){
        return view('dashboard.main');
    }
    public function login(){
        return view('login.index');
    }
    public function about(){
        return view('welcome.about');
    }
    public function contact(){
        return view('welcome.contact');
    }
    public function salesReps(){
        return view('welcome.sales');
    }
}
