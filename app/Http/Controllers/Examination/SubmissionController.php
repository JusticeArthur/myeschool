<?php

namespace App\Http\Controllers\Examination;

use App\Models\AcademicYear;
use App\Models\NewClass;
use App\Models\Report;
use App\Models\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubmissionController extends Controller
{
    public function index(){
        $user = auth()->user();
        $classes = NewClass::where('school_id',$user->school_id)->get();
        return view('examination.submissions.index',[
            'classes'=>$classes
        ]);
    }
    public function getClassCourses($id){
        $courses = [];
        $collection =  NewClass::find($id)->courses;
        foreach ($collection as $item){
            array_push($courses,$item->course->only(['id','name']));
        }
        return $courses;
    }
    public function getSubmissions(Request $request){
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = Term::where('academic_year_id',$active_year->id)
            ->where('status','active')->first();
        $reports = Report::with(['student'])->where('school_id',auth()->user()->school_id)
            ->where('academic_year_id',$active_year->id)
            ->where('term_id',$active_term->id)
            ->where('class_id',$request->class_id)
            ->where('subject_id',$request->subject_id)
            ->get();

            $reports = $reports->sortByDesc('score')->values()->toArray();
        //calculate position
        $rank = 1;
        $tie_rank = 0;
        $prev_score = -1;
        $count = 0;
        foreach($reports as $index=>$report){
                if($report['score'] != $prev_score){ //this score is not tie
                    $count = 0;
                    $prev_score = $report['score'];
                    $reports[$index]['position'] = $rank;
                }else{
                    $prev_score = $report['score'];
                    if($count++ == 0){
                        $tie_rank = $rank -1;
                    }
                    $reports[$index]['position'] = $tie_rank;
                }
                $rank ++;
        }
        return $reports;

    }
}
