<?php

namespace App\Http\Controllers\Examination;

use App\Models\ExamApproval;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $approvals = ExamApproval::with(['term','academicYear'])->where('school_id',auth()->user()->school_id)->get();
        return view('examination.approval.index',[
            'approvals'=>$approvals
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //save a new approval
        $user = auth()->user();
        $approval = new ExamApproval();
        $approval->school_id = $user->school_id;
        $approval->approved = true;
        $approval->user_id = $user->id;
        $approval->academic_year_id = $request->academic_year_id;
        $approval->term_id = $request->term_id;
        try{
            $approval->save();
        }catch (QueryException $q){
            return response()->json([
                'status'=>'approved'
            ],401);
        }
        return ExamApproval::with(['term','academicYear'])
            ->where('id',$approval->id)
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ExamApproval::where('id',$id)->update([
            'approved'=>$request->approved
        ]);
        return ExamApproval::with(['term','academicYear'])
            ->where('id',$id)
            ->first();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ExamApproval::find($id)->delete();
    }
}
