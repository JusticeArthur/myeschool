<?php

namespace App\Http\Controllers\SMS;

use App\Jobs\SendSMS;
use App\Models\InstitutionSetting;
use App\Models\NewClass;
use App\Models\NewParent;
use App\Models\Sms;
use App\Models\SmsBill;
use App\Models\Student;
use App\Models\StudentSetting;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\Entrust;

class SMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes  = NewClass::where('school_id',auth()->user()->school_id)
            ->get();
        return view('sms.send',[
            'classes'=>$classes,
        ]);
    }

    public function smsForStaff(){
        return User::where('school_id',auth()->user()->school_id)->where('id','<>',auth()->user()->id)
            ->where('contact','<>',null)
            ->get();
    }
    public function smsForParent(){
        return NewParent::where('school_id',auth()->user()->school_id)
            ->where('contact','<>',null)
            ->get();
    }
    public function smsForStudent(){
        return  Student::where('school_id',auth()->user()->school_id)
            ->where('phone','<>',null)
            ->get();
    }
    public function getStudentForParentClass($class_id){
        $students = DB::table('students')
            ->where('students.school_id',auth()->user()->school_id)
            ->where('students.class_id',$class_id)
            ->Join('student_guardians','students.id','student_guardians.student_id')
            ->Join('parents','student_guardians.parent_id','parents.id')
            ->where('parents.contact','<>',null)
            ->select('parents.*',DB::raw('CONCAT(parents.lname," ", parents.fname) AS name'))
            ->distinct()
            ->get();
        return $students;
    }

    public function getStudentForClass($class_id){
        $students =  NewClass::with(['students'=>function($query){
            $query->where('phone','<>',null);
        }])->where('id',$class_id)->get();
        return $students->toArray()[0]['students'];
    }
    protected function FormatContact($contact){
        //replace 0 with +233 that is the country code
        $formatted = str_replace(' ','',$contact);
        if($contact[0] == '0'){
            $pos = strpos($formatted, '0');
            if ($pos !== false) {
                $formatted = substr_replace($formatted, '+233', $pos, 1);
            }
        }

        return $formatted;
    }
    //send sms from here
    public function sendSMS(Request $request,$id = null){
        if(auth()->user()->can('add_sms')){

            $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
            if($id == null){
                foreach($request->data as $item){
                    if(isset($item['contact']))
                        //update sms bill of school
                        $bill = SmsBill::where('school_id',auth()->user()->school_id)->first();
                        $units = $request->units;

                    if(isset($bill)){
                            $bill->amount += ((int)$units * 0.04) ;
                            $bill->save();
                    }else{
                       $newBill = new SmsBill();
                       $newBill->school_id = auth()->user()->school_id;
                       $newBill->amount = ((int)$units * 0.04);
                       $newBill->save();
                    }
                        SendSMS::dispatch($this->FormatContact($item['contact']),$request->text,auth()->user(),$settings);
                }
            }else{
                //return $request->data['student'];
                $data = json_decode($request->data,true);
                foreach ($data as $datum){
                    $replacements = array();
                    $replacements[1] = $datum['amount'];
                    $replacements[0] = $datum['student']['name'];
                    $patterns = array();
                    $patterns[1] = '/[@]amount/i';
                    $patterns[0] = '/[@]name/i';
                    $string = preg_replace($patterns, $replacements, $request->text);
                    $guardians = data_get($datum,'student.guardians');
                    if(count($guardians) > 0){
                        foreach ($guardians as $guardian){
                            if(isset($guardian['contact'])){
                                $bill = SmsBill::where('school_id',auth()->user()->school_id)->first();
                                $units = $request->units;

                                if(isset($bill)){
                                    $bill->amount += ((int)$units * 0.04) ;
                                    $bill->save();
                                }else{
                                    $newBill = new SmsBill();
                                    $newBill->school_id = auth()->user()->school_id;
                                    $newBill->amount = ((int)$units * 0.04);
                                    $newBill->save();
                                }
                                SendSMS::dispatch($this->FormatContact($guardian['contact']),$string,auth()->user(),$settings);
                            }
                        }
                    }else{
                        continue;
                    }
                }

                //return $request->data['student']['guardians'];
            }
        }else{
            return response()->json([
                'message'=>'unauthorised'
            ],501);
        }
    }
    public function sendSingleSMS(Request $request,$id = null){
        if(auth()->user()->can('add_sms')){
            $settings = InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
            if($id == null){
                $bill = SmsBill::where('school_id',auth()->user()->school_id)->first();
                $units = $request->units;

                if(isset($bill)){
                    $bill->amount += ((int)$units * 0.04) ;
                    $bill->save();
                }else{
                    $newBill = new SmsBill();
                    $newBill->school_id = auth()->user()->school_id;
                    $newBill->amount = ((int)$units * 0.04);
                    $newBill->save();
                }
                SendSMS::dispatch($this->FormatContact($request->contact),$request->text,auth()->user(),$settings);
            }else{
                if(count($request->guardians) > 0){

                    foreach ($request->guardians as $guardian){
                        if(isset($guardian['contact'])){
                            $bill = SmsBill::where('school_id',auth()->user()->school_id)->first();
                            $units = $request->units;

                            if(isset($bill)){
                                $bill->amount += ((int)$units * 0.04) ;
                                $bill->save();
                            }else{
                                $newBill = new SmsBill();
                                $newBill->school_id = auth()->user()->school_id;
                                $newBill->amount = ((int)$units * 0.04);
                                $newBill->save();
                            }
                            SendSMS::dispatch($this->FormatContact($guardian['contact']),$request->text,auth()->user(),$settings);
                        }
                    }
                }else{
                    return response()->json([
                        'status'=>'error'
                    ],422);
                }
            }
        }else{
            return response()->json([
                'message'=>'unauthorised'
            ],501);
        }

    }

    //////show SMS Details /////
    public function getSMSDetailsIndex(){
        $sms = Sms::where('school_id',auth()->user()->school_id)
            ->orderBy('created_at','desc')
            ->paginate(10);
        return view('sms.details.selection',[
            'sms'=>$sms
        ]);
    }
    public function getSMSDetailParticular($id){
        $sms = Sms::where('message_id',$id)->first();
        $response = \App\utilities\SMS::response($id,$sms->to);
        $data = json_encode($response);
        $data = json_decode($data,true);
        $status = $data['results'][0]['status']['groupName'];
        $time = Carbon::parse($data['results'][0]['sentAt'])->toDayDateTimeString();
        return view('sms.details.detail',[
            'sms'=>$sms,
            'status'=>$status,
            'time'=>$time
        ]);
    }
}
