<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'=>'required',
            'state_id'=>'required',
            'role_id'=>'required',
            'dob'=>'required',
            'phone'=>'required'
        ];
    }
    public function messages()
    {
       return [
            'country_id.required' => 'The country field is required',
            'state_id.required'  => 'The state field is required',
           'role_id.required'  => 'The role field is required',
           'dob.required'  => 'The date of birth is required',
           'phone'  => 'The phone field is required',
        ];
    }
}
