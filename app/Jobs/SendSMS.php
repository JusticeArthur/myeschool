<?php

namespace App\Jobs;

use App\utilities\SMS;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($to,$text,$user,$settings)
    {
        $this->to = $to;
        $this->settings = $settings;
        $this->text = $text;
        $this->user = $user;
    }
    protected $to;
    protected $settings;
    protected $text;
    protected $user;
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        SMS::send($this->to,$this->text,$this->user,$this->settings);
    }
}
