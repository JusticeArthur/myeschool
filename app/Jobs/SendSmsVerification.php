<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use infobip\api\client\GetSentSmsLogs;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\logs\GetSentSmsLogsExecuteContext;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;

class SendSmsVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($to,$code)
    {
        $this->to = $to;
        $this->code = $code;
    }
    protected $to;
    protected $code;
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $configuration = new BasicAuthConfiguration('myeschoolgh', 'Ghana2@sch', 'http://api.infobip.com/');
        $client = new SendMultipleTextualSmsAdvanced($configuration);
        $destination = new Destination();
        $destination->setTo($this->to);
        // $destination->setMessageId('Hi Justice');
        $message = new Message();
        $message->setDestinations([$destination]);
        $message->setFrom('Myeschool');
        $message->setTo($this->to);
        $message->setText($this->code);
        $message->setNotifyContentType('application/json');
        //$message->setCallbackData('');
        $requestBody = new SMSAdvancedTextualRequest();
        $requestBody->setMessages([$message]);
        $client->execute($requestBody);
    }
}
