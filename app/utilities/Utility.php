<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 24/01/2019
 * Time: 21:55
 */

namespace App\utilities;


use App\Models\AcademicYear;
use App\Models\StudentWallet;
use App\Models\Term;

class Utility
{
    public static function getActiveTermYear(){
        $active_year = AcademicYear::where('school_id',auth()->user()->school_id)
            ->where('status','active')->first();
        $active_term = null;
        if(isset($active_year)){
            $active_term = Term::where('academic_year_id',$active_year->id)
                ->where('school_id',auth()->user()->school_id)
                ->where('status','active')->first();
        }
        return [
          'active_term'=> isset($active_term) ? $active_term->id:0,
          'active_year'=> isset($active_year)?$active_year->id:0
        ];
    }

    public static function generateCode() {
        return random_int(1000,9999);
    }
    public  static  function  FormatContact($contact){
        $formatted = str_replace(' ','',$contact);
        if($contact[0] == '0'){
            $pos = strpos($formatted, '0');
            if ($pos !== false) {
                $formatted = substr_replace($formatted, '+233', $pos, 1);
            }
        }
        return $formatted;
    }
    public static function getStudentWallet($id){
        $student = StudentWallet::where('student_id',$id)->first()->balance;
        return $student;
    }
}