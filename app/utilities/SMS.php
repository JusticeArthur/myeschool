<?php
/**
 * Created by PhpStorm.
 * User: Arthur
 * Date: 30/01/2019
 * Time: 12:27
 */

namespace App\utilities;
use infobip\api\client\GetSentSmsLogs;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\Destination;
use infobip\api\model\sms\mt\logs\GetSentSmsLogsExecuteContext;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
use Webpatser\Uuid\Uuid;


class SMS
{
    public static function send($to,$text,$user,$settings){
        $configuration = new BasicAuthConfiguration($settings->sms_username, $settings->sms_password, 'http://api.infobip.com/');
        $client = new SendMultipleTextualSmsAdvanced($configuration);
        $destination = new Destination();
        $destination->setTo($to);
        // $destination->setMessageId('Hi Justice');

        $message = new Message();
        $message->setDestinations([$destination]);
        $message->setFrom($settings->sms_display_name);
        $message->setTo($to);
        $message->setText($text);
        $message->setNotifyUrl(route('sms.callback'));
        $message->setNotifyContentType('application/json');
        //$message->setCallbackData('');
        $requestBody = new SMSAdvancedTextualRequest();
        $requestBody->setMessages([$message]);
        $response = $client->execute($requestBody);
        $responses = $response->getMessages();
        //create new sms
        $sms = new \App\Models\Sms();
        $sms->text = $text;
        $sms->to = $responses[0]->getTo();
        $sms->school_id = $user->school_id;
        $sms->group_id = $responses[0]->getStatus()->getGroupId();
        $sms->group_name = $responses[0]->getStatus()->getGroupName();
        $sms->description = $responses[0]->getStatus()->getDescription();
        $sms->message_id = $responses[0]->getMessageId();
        $sms->count = $responses[0]->getSmsCount();
        $sms->user_id = $user->id;
        $sms->id = (string) Uuid::generate();
        $sms->save();
      //  dd($responses);
    }
    public static function response($messageId,$to){
        $configuration = new BasicAuthConfiguration('myeschoolgh', 'Ghana2@sch', 'http://api.infobip.com/');
        $client = new GetSentSmsLogs($configuration);
        $context = new GetSentSmsLogsExecuteContext();
        $context->setMessageId($messageId);
        $context->setTo($to);
        $context->setLimit(5);
        $apiResponse = $client->execute($context);
        return $apiResponse;
    }
}