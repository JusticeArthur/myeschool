@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-info"></i>MANAGE FINANCES</h5>
                    <div class="section-divider"></div>
                    <div class="dash-item first-dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('expenses-categories.index')}}" class="btn btn-success">
                                                    GO</a></h1>
                                            <p>EXPENSES CATEGORIES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-pencil-square-o"></i>What We Spend On</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('income-categories.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>INCOME CATEGORIES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-exclamation-triangle"></i>The Income We Expect</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('expenses.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>EXPENSES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-money "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-exclamation-triangle">

                                            </i>Expenses</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('incomes.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>INCOMES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-money "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-exclamation-triangle">

                                            </i>Incomes</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {{--second row--}}
                    <div class="dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('payments.index')}}" class="btn btn-success">
                                                    GO</a></h1>
                                            <p>PAYMENT METHODS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-cc-mastercard "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-pencil-square-o"></i>Payments Categories</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('finance.manage.bills.home')}}" class="btn btn-success">GO</a></h1>
                                            <p>MANAGE BILLS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-credit-card "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-credit-card-alt">

                                            </i>Billings</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('student-fees.index')}}" class="btn btn-success">
                                                    GO</a></h1>
                                            <p>SCHOOL FEES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-credit-card-alt "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-pencil-square-o"></i>Receive School Fees</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('transaction_history.index')}}" class="btn btn-success">
                                                    GO</a></h1>
                                            <p>TRANSACTION HISTORY</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-history "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-pencil-square-o"></i>All Transactions</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {{--third row--}}
                    <div class="dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('finance.notifications.index')}}" class="btn btn-success">
                                                    GO</a></h1>
                                            <p>OWING STUDENTS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-bullhorn "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-bell"></i>Send Notifications</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush