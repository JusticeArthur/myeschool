@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row hide-for-printing">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-clock-o"></i>FEE PAYMENTS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <fee-collection-component all_methods="{{$all_methods}}" settings="{{$settings}}"></fee-collection-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        <div class="dash-footer col-lg-12">
            <p>Copyright Pathshala</p>
        </div>
    </div>
@endsection

@section('js')
@endsection

@push('scripts')

@endpush
