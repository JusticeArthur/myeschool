@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-info"></i>MANAGE DETAILS</h5>
                    <div class="section-divider"></div>
                    <div class="dash-item first-dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('admin.details.fees')}}" class="btn btn-success">ADD</a></h1>
                                            <p>SET FEES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-ex"><i class="fa fa-pencil-square-o"></i>Set School Fees here</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('admin.details.items')}}" class="btn btn-success">ADD</a></h1>
                                            <p>FEES BREAKDOWN</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-braille"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-danger"><i class="fa fa-exclamation-triangle"></i>See all fees breakdown</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('fee-approvals.index')}}" class="btn btn-success">ADD</a></h1>
                                            <p>APPROVE FEES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-diamond"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-clock-o"></i>Manage Fee Approval</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush