@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-clock-o"></i>MANAGE COURSES</h5>
                    <div class="section-divider"></div>
                    <div class="dash-item first-dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('courses.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>COURSES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-book"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-ex"><i class="fa fa-pencil-square-o"></i>Manage Courses</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('class_courses.create')}}" class="btn btn-success">GO</a></h1>
                                            <p>TEACHER</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-user-plus"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-ex"><i class="fa fa-pencil-square"></i>Manage Course Teachers</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush