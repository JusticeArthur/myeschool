@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-info"></i>MANAGE BILLS</h5>
                    <div class="section-divider"></div>
                    <div class="dash-item first-dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('student-bills.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>BILL STUDENTS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-credit-card "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-credit-card-alt">

                                            </i>Billings</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('finance.print.bills.index')}}" class="btn btn-success">
                                                    GO</a></h1>
                                            <p>PRINT BILLS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-print "></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-pencil-square-o"></i>Print Bills</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush