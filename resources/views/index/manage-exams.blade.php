@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-info"></i>EXAMINATION</h5>
                    <div class="section-divider"></div>
                    <div class="dash-item first-dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('examinations.submissions.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>SUBMISSIONS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-info"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-ex"><i class="fa fa-pencil-square-o"></i>Results Submissions</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('teacher.reports.generate.students')}}" class="btn btn-success">GO</a></h1>
                                            <p>STUDENT REPORTS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-resistance"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-ex"><i class="fa fa-pencil-square-o"></i>Report Cards</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('remarks.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>GRADES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            Report Cards Remarks
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('get.report.template')}}" class="btn btn-success">GO</a></h1>
                                            <p>REPORT TEMPLATE</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-folder-open-o"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status"><p class="text-success"><i class="fa fa-folder-open-o"></i>Report Card Template</p></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('exam-approvals.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>APPROVE RESULTS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-ex"><i class="fa fa-pencil-square-o"></i>Exams Results</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush