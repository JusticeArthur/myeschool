@if(Session::has('success'))
    <script type="text/javascript">
        var message ="{!! Session::get('success') !!}";
        new Noty({
            theme:'mint',
            text: message,
            layout:'topRight',
            progressBar:true,
            timeout:3000,
            type:'success'

        }).show();
    </script>
@elseif(Session::has('error'))
    <script type="text/javascript">
        var message = "{!! Session::get('error') !!}";
        new Noty({
            theme:'mint',
            text: message,
            layout:'topRight',
            progressBar:true,
            timeout:3000,
            type:'error'

        }).show();
    </script>
@elseif(Session::has('message'))
    <script type="text/javascript">
        var message = "{!! Session::get('message') !!}";
        Snackbar.show(
            {
                text: message,
                pos:'top-center',
                showAction:true,
                backgroundColor:'#e5ac39',
                actionTextColor:'#ffffff',
                actionText:'Dismiss',
                duration:4000
            }
        );
    </script>
@endif




