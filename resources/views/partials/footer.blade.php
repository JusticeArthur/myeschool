<div class="row footer-row">
    <div class="container">
        <div class="school-logo">
            <img height="100" src="{{asset('image_home/svg/logo-1.svg')}}" alt="">
        </div>
        <p class="text-center">Over 1500+ Schools | Free Demo Available | Easy to use everytime</p>
        <p class="text-center">Updates and 24/7 Support | Flexible Payment Methods</p>
        <br><br>
        <div class="col-md-4 col-sm-6 footer-item">
            <h5>CONTACT US</h5>
            <p><i class="fa fa-map-marker"></i>Opp. Joyce Ababio School, Cantoment, Accra</p>
            <p><i class="fa fa-mobile"></i> 030 70 70 199 | 050 123 9930</p>
            <p><i class="fa fa-envelope"></i>support@tecunitgh.com | info@tecunitgh.com </p>
        </div>
        <div class="col-md-2 col-sm-6 footer-item">
            <h5>QUICK LINKS</h5>
            <div class="quick-link-box">
                <a href="#"><i class="fa fa-home"></i>HOME</a>
                <a href="{{route('home.about')}}"><i class="fa fa-info-circle"></i>ABOUT</a>
                <a href="{{route('home.contact')}}"><i class="fa fa-phone"></i>CONTACT</a>
                <a href="{{route('home.sales_reps')}}"><i class="fa fa-briefcase"></i>SALES REPS</a>
            </div>
        </div>
        <div class="clearfix visible-sm"></div>
        <div class="col-md-3 col-sm-6 footer-item">
            <h5>WORK HOURS</h5>
            <p><i class="fa fa-clock-o"></i> MON - FRI 9:00 AM - 3:00 PM</p>
            <p><i class="fa fa-clock-o"></i> SAT 9:00 AM - 1:00 PM</p>
        </div>
        <div class="col-md-3 col-sm-6 footer-item">
            <h5>SUBSCRIBE</h5>
            <div class="footer-subscribe">
                <i class="fa fa-envelope"></i></a><input type="text" placeholder="example@gmail.com" />
            </div>
            <a href="#" class="subscribe-link"><i class="fa fa-paper-plane"></i>SUBMIT</a>
        </div>
    </div>
    <div class="footer-social-row">
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
    </div>
</div>
