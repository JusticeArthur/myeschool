@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-info"></i>MANAGE EMPLOYEES</h5>
                    <div class="section-divider"></div>
                    <div class="clearfix"></div>
                    <div class="dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('school-roles.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>ROLES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-lock success-icon"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            Manage all Roles
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('roles.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>PERMISSIONS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-road success-icon"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            Manage who sees what
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('employees.create')}}" class="btn btn-success">GO</a></h1>
                                            <p>ADD EMPLOYEE</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-user-plus success-icon"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            Add employee
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('employees.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>ALL EMPLOYEES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-user success-icon"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            All Employees
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush