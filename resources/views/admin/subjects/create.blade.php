@extends('admin.index')


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-cogs"></i>ALL SUBJECTS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <course-component pairings="{{$pairings}}"></course-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
    @include('admin.partials.footer')

        <!--Edit details modal-->
        <div id="editDetailModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i>EDIT SUBJECT DETAILS</h4>
                    </div>
                    <div class="modal-body dash-form">
                        <div class="col-sm-4">
                            <label class="clear-top-margin"><i class="fa fa-book"></i>SUBJECT</label>
                            <select id="edit_course" class="edit_course">
                            <option</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label class="clear-top-margin"><i class="fa fa-book"></i>CLASS</label>
                            <select id="edit_class" >
                            <option</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label class="clear-top-margin"><i class="fa fa-user-secret"></i>TEACHER</label>
                            <select id="edit_teacher">
                            <option></option>
                            </select>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="table-action-box">
                            <a href="#" class="save editupdate" id=""><i class="fa fa-check"></i>SAVE</a>
                            <a href="#" class="cancel" data-dismiss="modal"><i class="fa fa-ban"></i>CLOSE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

