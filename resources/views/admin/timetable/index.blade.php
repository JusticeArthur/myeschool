@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row hide-for-printing">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-clock-o"></i>TIME SLOTS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <timetable-component classes="{{$classes}}" slots="{{$slots}}"></timetable-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')




    </div>
@endsection

@section('js')
@endsection

@push('scripts')

@endpush
