@extends('admin.index')

@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-clock-o"></i>FEE APPROVALS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <fee-settings-component approvals="{{$approvals}}"></fee-settings-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        <div class="dash-footer col-lg-12">
            <p>Copyright Pathshala</p>
        </div>



    </div>
@endsection


