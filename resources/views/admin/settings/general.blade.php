@extends('admin.index')

@section('css')
    <style>
        .view{
            text-align: center;
        }
        .fa-eye{
            color: white;
        }

    </style>
@endsection


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-user-secret"></i>GENERAL SETTINGS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <div class="col-lg-12">
                        <div class="dash-item first-dash-item">
                            <div class="inner-item">
                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Web Page Settings</a></li>
                                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Site Skin</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <br>
                                            <form method="post" action="{{route('general_settings.store')}}">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="sidebar_enable" type="checkbox" @if($data) {{$data->sidebar_opened?'checked':''}} @endif> Sidebar Opened
                                                    </label>
                                                </div>
                                                <button type="submit" class="btn btn-default">Submit</button>
                                                {{csrf_field()}}
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="settings">
                                            <br>
                                            <form method="post" action="{{route('general_settings.store')}}">
                                                    <div class="form-group col-sm-3">
                                                        <input name="skin" class="form-control" value="#67b14e" type="color" required/>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-default">Submit</button>
                                                    </div>
                                                {{csrf_field()}}
                                            </form>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>




        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>



@endsection

@section('js')
    <script>
        $(function () {
            $('#weekTable').dataTable();
            $('#termTable').dataTable();
            $('#yearTable').dataTable();
        })
    </script>
@endsection

