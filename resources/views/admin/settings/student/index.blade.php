@extends('admin.index')

@section('css')
    <style>
        @media screen and (max-width: 700px){
            td:nth-child(4),td:nth-child(2),td:nth-child(5){
                display: none;
            }

        }

    </style>
@endsection


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-cogs"></i>CONFIGURATION | STUDENT PROFILE FIELDS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <student-profile-component data="{{$data}}" settings="{{$settings}}"></student-profile-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')

    </div>
@endsection


@section('js')
@endsection

@section('css')
    <style>
        .fa{
            color: white;
        }
        td{
            text-align: center;
        }
        th{
            text-align: center;
        }
    </style>
@endsection