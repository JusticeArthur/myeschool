@extends('admin.index')

@section('css')
@endsection


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title">REPORT TEMPLATES</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                        <div class="my-msg dash-item first-dash-item">
                            <h6 class="item-title">SELECT REPORT TEMPLATE</h6>
                            <report-template-component setting="{{$settings}}" template="{{$templates}}"></report-template-component>
                            <div class="clearfix"></div>
                        </div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
    @include('admin.partials.footer')
    </div>
@endsection


@section('css')
    <style>
        .fa{
            color: white;
        }
    </style>
@endsection