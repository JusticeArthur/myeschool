@extends('admin.index')


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-sliders"></i>ALL FEES</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <fees-component breakdowns="{{$breakdowns}}" approvals="{{$approvals}}"></fees-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
    @include('admin.partials.footer')


        <!--Edit details modal-->
        <div id="editDetailModal" class="modal fade" role="dialog">
            <form action="{{route('classes.update', ":id")}}" method="post">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-edit"></i>EDIT CLASS DETAILS</h4>
                        </div>
                        <div class="modal-body dash-form">
                            <div class="col-sm-4">
                                <label class="clear-top-margin"><i class="fa fa-book"></i>CLASS</label>
                                <input type="text" name="name" id="class-name" placeholder="CLASS"  />
                            </div>
                            <div class="col-sm-4">
                                <label class="clear-top-margin"><i class="fa fa-code"></i>CLASS CODE</label>
                                <input type="text" name="code" id="class-code" placeholder="CLASS CODE"  />
                            </div>
                            <input type="hidden" name="hidden" id="hidden-input">
                            <div class="col-sm-4">
                                <label class="clear-top-margin"><i class="fa fa-user-secret"></i>CLASS TEACHER</label>
                                <select name="teacher" id="teachers">

                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-12">
                                <label><i class="fa fa-info-circle"></i>DESCRIPTION</label>
                                <textarea name="description" placeholder="Enter Description Here" id="class-description"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                            <div class="table-action-box">
                                <a href="#" id="modal-submit-button" class="save"><i class="fa fa-check"></i>SAVE</a>
                                <a href="#" class="cancel" data-dismiss="modal"><i class="fa fa-ban"></i>CLOSE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
