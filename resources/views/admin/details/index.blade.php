@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-info"></i>MANAGE DETAILS</h5>
                    <div class="section-divider"></div>
                    <div class="dash-item first-dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('manage.fees.home')}}" class="btn btn-success">
                                                    GO</a></h1>
                                            <p>MANAGE FEES</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-pencil"></i>All Fee Actions</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('admin.details.academic')}}" class="btn btn-success">ADD</a></h1>
                                            <p>ACADEMIC YEAR</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-success"><i class="fa fa-folder-open-o"></i>Manage academic years here</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('admin.details.terms')}}" class="btn btn-success">ADD</a></h1>
                                            <p>SET SCHOOL TERMS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-diamond"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-clock-o"></i>Manage all terms here</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('admin.get.all.guardians')}}" class="btn btn-success">GO</a></h1>
                                            <p>MANAGE GUARDIANS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-user-secret"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status"><p class="text-success"><i class="fa fa-folder-open-o"></i>All Guardians</p></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="dash-item">
                        <div class="dashboard-stats">
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('general_settings.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>WEB SETTINGS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-cog"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            website settings
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('institution_settings.index')}}" class="btn btn-success">GO</a></h1>
                                            <p>INSTITUTION SETTINGS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-code"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            all institution details
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm"></div>
                            <div class="col-sm-6 col-md-3">
                                <div class="stat-item">
                                    <div class="stats">
                                        <div class="col-xs-8 count">
                                            <h1><a href="{{route('configuraton.student.profile.field')}}" class="btn btn-success">GO</a></h1>
                                            <p>STUDENT SETTINGS</p>
                                        </div>
                                        <div class="col-xs-4 icon">
                                            <i class="fa fa-stumbleupon"></i>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="status">
                                        <p class="text-look"><i class="fa fa-pencil-square-o"></i>
                                            students admission fields
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush