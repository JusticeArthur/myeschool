@extends('admin.index')

@section('css')

@endsection


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-sliders"></i>ACADEMIC YEARS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            @include('partials.alert')
                    <academic-component years="{{$years}}"></academic-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
    @include('admin.partials.footer')



    </div>
@endsection


@section('js')

@endsection