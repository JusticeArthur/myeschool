@extends('layouts.app')
@push('css')
    <style>
        .current{
            font-weight: bolder;
            border-bottom: 5px solid #14909e;
            background-color: #2e4670 !important;
            color: white !important;
        }
        .current a{
            color: white !important;
        }
    </style>
@yield('css')
@endpush
@section('content')
@include('admin.partials.header')
@php $school = \App\Models\School::find(auth()->user()->school_id); @endphp
<div class="parent-wrapper  @if($school->generalSettings) {{$school->generalSettings->sidebar_opened ? 'toggled':''}} @endif" id="outer-wrapper">
@include('admin.partials.sidebar')

@yield('main')

</div>

@endsection
@push('js')
@yield('js')
@endpush
