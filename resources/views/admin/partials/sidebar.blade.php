<div class="sidebar-nav-wrapper" id="sidebar-wrapper">

    <ul class="sidebar-nav">
        @if(!\Illuminate\Support\Facades\Auth::user()->hasRole(['admin','teacher','super']))
            <li>
                <a href="{{route('myeschool.local.dashboard')}}"><i class="fa fa-dashboard menu-icon"></i> DASHBOARD</a>
            </li>
        @endif
        @role('super')
            <li>
                <a href="{{route('super.home')}}"><i class="fa fa-home menu-icon"></i> HOME</a>
            </li>
            <li>
                <a href="{{route('schools.index')}}"><i class="fa fa-graduation-cap menu-icon"></i> MANAGE SCHOOLS</a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user menu-icon"></i> PROFILE <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{route('super.profile.show')}}"><i class="fa fa-caret-right"></i>SHOW PROFILE</a>
                    </li>
                    <li style="padding-bottom: 40px;">
                        <a href="{{route('super.passwords.show')}}"><i class="fa fa-caret-right" ></i>CHANGE PASSWORD</a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </li>
        @endrole

        @role('admin')
        <li>
            <a href="{{route('admin.home')}}"><i class="fa fa-home menu-icon"></i> HOME</a>
        </li>
            <li>
                <a href="{{route('admin.details.index')}}"><i class="fa fa-info menu-icon"></i> MANAGE DETAILS</a>
            </li>
            <li>
                <a href="{{route('manage.schedule.home')}}"><i class="fa fa-clock-o menu-icon"></i> MANAGE SCHEDULE</a>
            </li>
        <li>
            <a href="{{route('manage.employees.index')}}"><i class="fa fa-user menu-icon"></i> MANAGE EMPLOYEES</a>
        </li>
            <li>
                <a href="{{route('manage.courses.home')}}"><i class="fa fa-subway menu-icon"></i> MANAGE COURSES</a>
            </li>
            <li>
                <a href="{{route('manage.students.home')}}"><i class="fa fa-user-plus menu-icon"></i> MANAGE STUDENTS</a>
            </li>

        @if(Auth::user()->can('browse_classes'))
                <li>
                    <a href="{{route('manage.classes.home')}}"><i class="fa fa-group menu-icon"></i> MANAGE CLASSES</a>
                </li>
        @endif
            <li>
                <a href="{{route('admin.configuration.index')}}"><i class="fa fa-cog menu-icon"></i> SETTINGS</a>
            </li>
        @endrole
        @role('student')
        <li>
            <a href="{{route('student.home')}}"><i class="fa fa-home menu-icon"></i> HOME</a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-file-code-o menu-icon"></i> ASSIGNMENTS <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('students.ass.download')}}"><i class="fa fa-caret-right"></i>DOWNLOAD</a>
                </li>
                <li>
                    <a href="{{route('student.ass.upload')}}"><i class="fa fa-caret-right"></i>UPLOAD</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bar-chart menu-icon"></i> ATTENDENCE <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('student.att.summary')}}"><i class="fa fa-caret-right"></i>SUMMARY</a>
                </li>
                <li>
                    <a href="{{route('student.att.detail')}}"><i class="fa fa-caret-right"></i>DETAILED</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li>
            <a href="{{route('student.messages.show')}}"><i class="fa fa-envelope menu-icon"></i> MY MESSAGES</a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-address-card menu-icon"></i> VIEW MARKS <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('student.assessment.show')}}"><i class="fa fa-caret-right"></i>CLASS ASSESSMENT</a>
                </li>
                <li>
                    <a href="{{route('student.assessment.mid')}}"><i class="fa fa-caret-right"></i>MID TERM</a>
                </li>
                <li>
                    <a href="{{route('student.assessment.end')}}"><i class="fa fa-caret-right"></i>END TERM</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li>
            <a href="{{route('student.timetable')}}"><i class="fa fa-calendar menu-icon"></i> TIME TABLE</a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-book menu-icon"></i> EXAMINATION <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('student.exam.plan')}}"><i class="fa fa-caret-right"></i>SEATING PLAN</a>
                </li>
                <li>
                    <a href="{{route('student.exam.schedule')}}"><i class="fa fa-caret-right"></i>EXAM SCHEDULE</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li>
            <a href="{{route('student.fee')}}"><i class="fa fa-money menu-icon"></i> FEE DETAILS</a>
        </li>
        @endrole
        @role(['teacher'])
        <li>
            <a href="{{route('teacher.home')}}"><i class="fa fa-home menu-icon"></i> HOME</a>
        </li>
        @if(Auth::user()->can('browse_students'))
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-users menu-icon"></i> STUDENTS <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    @if(Auth::user()->can('add_students'))
                        <li>
                            <a href="{{route('students.create')}}"><i class="fa fa-caret-right"></i>ADD</a>
                        </li>
                        @endif
                    <li>
                        <a href="{{route('students.index')}}"><i class="fa fa-caret-right"></i>ALL STUDENT  </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </li>
            @endif
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-file-code-o menu-icon"></i> ASSIGNMENTS <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('teacher.assignment.create')}}"><i class="fa fa-caret-right"></i>CREATE</a>
                </li>
                <li>
                    <a href="{{route('teacher.assignment.download')}}"><i class="fa fa-caret-right"></i>DOWNLOAD</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bar-chart menu-icon"></i> ATTENDENCE <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('teacher.attendance.mark')}}"><i class="fa fa-caret-right"></i>MARK</a>
                </li>
                <li>
                    <a href="{{route('teacher.attendance.view')}}"><i class="fa fa-caret-right"></i>VIEW</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        {{--<li>
            <a href="{{route('teacher.messages')}}"><i class="fa fa-envelope menu-icon"></i> MY MESSAGES</a>
        </li>--}}
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-address-card menu-icon"></i> ASSESSMENT <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('assessments.create')}}"><i class="fa fa-caret-right"></i>CREATE</a>
                </li>
                <li>
                    <a href="{{route('assessments.index')}}"><i class="fa fa-caret-right"></i>VIEW</a>
                </li>
                <li>
                    <a href="{{route('teacher.reports.submissions')}}"><i class="fa fa-caret-right"></i>SUBMISSION</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        {{--<li>
            <a href="{{route('teacher.timetable')}}"><i class="fa fa-calendar menu-icon"></i> TIME TABLE</a>
        </li>--}}
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-address-card menu-icon"></i> REPORTS <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('teacher.reports.generate')}}"><i class="fa fa-caret-right"></i>CLASS</a>
                </li>
                <li>
                    <a href="{{route('teacher.reports.generate.students')}}"><i class="fa fa-caret-right"></i>STUDENTS</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        @endrole
        @role('guardian')
        <li>
            <a href="student-dashboard.html"><i class="fa fa-home menu-icon"></i> HOME</a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-file-code-o menu-icon"></i> ASSIGNMENTS <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="student-assignment-download.html"><i class="fa fa-caret-right"></i>DOWNLOAD</a>
                </li>
                <li>
                    <a href="student-assignment-upload.html"><i class="fa fa-caret-right"></i>UPLOAD</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bar-chart menu-icon"></i> ATTENDENCE <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="student-attendence.html"><i class="fa fa-caret-right"></i>SUMMARY</a>
                </li>
                <li>
                    <a href="student-attendence-detailed.html"><i class="fa fa-caret-right"></i>DETAILED</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li>
            <a href="message.html"><i class="fa fa-envelope menu-icon"></i> MY MESSAGES</a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-address-card menu-icon"></i> VIEW MARKS <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="student-marks.html"><i class="fa fa-caret-right"></i>CLASS ASSESSMENT</a>
                </li>
                <li>
                    <a href="student-marks.html"><i class="fa fa-caret-right"></i>MID TERM</a>
                </li>
                <li>
                    <a href="student-marks.html"><i class="fa fa-caret-right"></i>END TERM</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li>
            <a href="student-timetable.html"><i class="fa fa-calendar menu-icon"></i> TIME TABLE</a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-book menu-icon"></i> EXAMINATION <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="student-exam-plan.html"><i class="fa fa-caret-right"></i>SEATING PLAN</a>
                </li>
                <li>
                    <a href="student-exam-schedule.html"><i class="fa fa-caret-right"></i>EXAM SCHEDULE</a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </li>
        <li>
            <a href="student-fees.html"><i class="fa fa-money menu-icon"></i> FEE DETAILS</a>
        </li>
        @endrole
            @permission('browse_student_academic_infos')
            <li>
                <a href="{{route('manage.exams.index')}}"><i class="fa fa-apple menu-icon"></i> EXAMS & REPORTS</a>
            </li>
            @endpermission
            @permission('browse_finances')
            <li>
                <a href="{{route('manage_all.finances')}}"><i class="fa fa-money menu-icon"></i>MANAGE FINANCES</a>
            </li>
            @endpermission
        {{--<li>
            <a href="{{route('notifications.index')}}"><i class="fa fa-bell menu-icon"></i>NOTIFICATIONS</a>
        </li>--}}
       {{-- @permission('browse_timetables')

        @endpermission--}}
        @permission('browse_sms')
        <li>
            <a href="{{route('manage_all_sms.sms')}}"><i class="fa fa-bullhorn menu-icon"></i> MANAGE SMS</a>
        </li>
        @endpermission


        {{--@permission('browse_payroll')
        <li>
            <a href="{{route('payroll.home')}}"><i class="fa fa-usd menu-icon"></i> PAYROLL</a>
        </li>
        @endpermission--}}
    </ul>
</div>