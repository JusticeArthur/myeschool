<div class="row dashboard-top-nav">
    <div class="col-sm-4 logo" style="user-select: none">
        @if(Auth::check())
            <?php
            $settings = \App\Models\InstitutionSetting::where('school_id',auth()->user()->school_id)->first();
            $school_name = null;
            if(!isset($settings)){
                $id = Auth::user()->school_id;
                if($id != null){
                    $school_name = \App\Models\School::where('id',$id)->pluck('name')->first();
                }
            }
            ?>
                @if(isset($settings))
                    <h5>{{strtoupper($settings->name)}}</h5>
                    @else
                    @if($school_name != null)
                        <h5>{{strtoupper($school_name)}}</h5>
                    @endif
                    @endif
        @else
            <h5><i class="fa fa-book"></i>MYESCHOOL</h5>

        @endif
    </div>
    <app-header-search></app-header-search>

    <div class="col-sm-5 notification-area pull-right">
        <ul class="top-nav-list">
            <li class="shortcut">
                <a href="#" class="show-shortcut">
                        <span>
                            Sidebar Menus
                            <span class="caret"></span>
                        </span>
                </a>
                <ul class="custom-shortcut-folder">
                        <li class="multilevel-dropdown">
                            <a href="#"> MANAGE DETAILS <span class="fa fa-caret-right"></span></a>
                            <ul class="nested-custom-shortcut-folder ">
                                <li><a href="{{route('manage.fees.home')}}">MANAGE FEES</a></li>
                                <li><a href="{{route('admin.details.academic')}}">ACADEMIC YEARS</a></li>
                                <li><a href="{{route('admin.details.terms')}}">SET TERMS</a></li>
                                <li><a href="{{route('admin.get.all.guardians')}}">MANAGE GUARDIANS</a></li>
                                <li><a href="{{route('general_settings.index')}}">WEB SETTINGS</a></li>
                                <li><a href="{{route('institution_settings.index')}}">INSTITUTION SETTINGS</a></li>
                                <li><a href="{{route('configuraton.student.profile.field')}}">STUDENT SETTINGS</a></li>
                            </ul>
                        </li>
                        <li class="multilevel-dropdown">
                            <a href="#"> MANAGE SCHEDULE <span class="fa fa-caret-right"></span></a>
                            <ul class="nested-custom-shortcut-folder ">
                                <li><a href="{{route('slots.index')}}">SLOTS</a></li>
                                <li><a href="{{route('timetables.index')}}">TIMETABLE</a></li>
                            </ul>
                        </li>
                        <li class="multilevel-dropdown">
                            <a href="#"> MANAGE EMPLOYEES <span class="fa fa-caret-right"></span></a>
                            <ul class="nested-custom-shortcut-folder ">
                                <li><a href="{{route('school-roles.index')}}">ROLES</a></li>
                                <li><a href="{{route('roles.index')}}">PERMISSIONS</a></li>
                                <li><a href="{{route('employees.create')}}">ADD EMPLOYEE</a></li>
                                <li><a href="{{route('employees.index')}}">ALL EMPLOYEES</a></li>
                            </ul>
                        </li>
                        <li class="multilevel-dropdown">
                            <a href="#"> MANAGE COURSES <span class="fa fa-caret-right"></span></a>
                            <ul class="nested-custom-shortcut-folder ">
                                <li><a href="{{route('courses.index')}}">COURSES</a></li>
                                <li><a href="{{route('class_courses.create')}}">TEACHER</a></li>
                            </ul>
                        </li>
                    <li class="multilevel-dropdown">
                        <a href="#"> MANAGE STUDENTS <span class="fa fa-caret-right"></span></a>
                        <ul class="nested-custom-shortcut-folder ">
                            <li><a href="{{route('students.create')}}">STUDENTS</a></li>
                            <li><a href="{{route('students.index')}}">ALL STUDENTS</a></li>
                        </ul>
                    </li>
                    <li class="multilevel-dropdown">
                        <a href="#"> MANAGE CLASSES <span class="fa fa-caret-right"></span></a>
                        <ul class="nested-custom-shortcut-folder ">
                            <li><a href="{{route('classes.create')}}">ADD CLASS</a></li>
                            <li><a href="{{route('teacher.attendance.mark')}}">MARK ATTENDANCE</a></li>
                            <li><a href="{{route('teacher.attendance.view')}}">ATTENDANCE HISTORY</a></li>
                        </ul>
                    </li>
                    <li class="multilevel-dropdown">
                        <a href="#"> EXAMS & REPORTS <span class="fa fa-caret-right"></span></a>
                        <ul class="nested-custom-shortcut-folder ">
                            <li><a href="{{route('examinations.submissions.index')}}">SUBMISSIONS</a></li>
                            <li><a href="{{route('teacher.reports.generate.students')}}">STUDENT REPORTS</a></li>
                            <li><a href="{{route('remarks.index')}}">GRADES</a></li>
                            <li><a href="{{route('get.report.template')}}">REPORT TEMPLATES</a></li>
                            <li><a href="{{route('exam-approvals.index')}}">APPROVE RESULTS</a></li>
                        </ul>
                    </li>
                    <li class="multilevel-dropdown">
                        <a href="#"> SETTINGS <span class="fa fa-caret-right"></span></a>
                        <ul class="nested-custom-shortcut-folder ">
                            <li><a href="{{route('fee-approvals.index')}}">FEES APPROVALS</a></li>
                            <li><a href="{{route('admin.profile.show')}}">PROFILE VIEW</a></li>
                            <li><a href="{{route('admin.passwords.show')}}">PROFILE PASSWORD</a></li>
                            <li><a href="{{route('general_settings.index')}}">WEB SETTINGS</a></li>
                            <li><a href="{{route('institution_settings.index')}}">INSTITUTION SETTINGS</a></li>
                            <li><a href="{{route('configuraton.student.profile.field')}}">STUDENT SETTINGS</a></li>
                        </ul>
                    </li>
                    <li class="multilevel-dropdown">
                        <a href="#"> MANAGE FINANCES <span class="fa fa-caret-right"></span></a>
                        <ul class="nested-custom-shortcut-folder ">
                            <li><a href="{{route('incomes.index')}}">INCOMES</a></li>
                            <li><a href="{{route('expenses.index')}}">EXPENSES</a></li>
                            <li><a href="{{route('student-fees.index')}}">SCHOOL FEES</a></li>
                            <li><a href="{{route('finance.manage.bills.home')}}">MANAGE BILLS</a></li>
                            <li><a href="{{route('transaction_history.index')}}">TRANSACTION HISTORY</a></li>
                        </ul>
                    </li>
                    <li class="multilevel-dropdown">
                        <a href="#"> MANAGE SMS <span class="fa fa-caret-right"></span></a>
                        <ul class="nested-custom-shortcut-folder ">
                            <li><a href="{{route('manage_all_sms.sms.send')}}">SEND SMS</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="notification dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge nav-badge">{{count(auth()->user()->unreadNotifications)}}</span>
                </a>
                <ul class="dropdown-menu notification-list">
                    @if(count(auth()->user()->unreadNotifications) > 0)
                        <li>
                            <div class="list-msg">
                                <div class="col-xs-2 icon clear-padding">
                                    <i class="fa fa-trophy"></i>
                                </div>
                                <div class="col-sm-10 desc">
                                    <h5><a href="#">Upcoming Sports Meet</a></h5>
                                    <h6><i class="fa fa-clock-o"></i> 10 min ago</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </li>
                        <li>
                            <div class="all-notifications">
                                <a href="#">VIEW ALL NOTIFICATIONS</a>
                            </div>
                        </li>
                        @else
                        <li>
                            <div class="all-notifications">
                                <a>No New Notifications</a>
                            </div>
                        </li>
                        @endif
                </ul>
            </li>
            <li class="user dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span>
                        @if(Auth::user()->avatar != null)
                            <img src="{{asset('img/profile/'.Auth::user()->avatar)}}" alt="user" />
                        @else
                            <img src="{{asset('img/placeholder.png')}}" alt="user" />
                        @endif
                        @if(Auth::check())
                            {{Auth::user()->name}}
                            @endif
                        <span class="caret"></span></span>
                </a>
                <ul class="dropdown-menu notification-list">
                    @if(Auth::user()->hasRole('admin'))
                        <li>
                            <a href="{{route('admin.home')}}"><i class="fa fa-home"></i> HOME</a>
                        </li>
                        <li>
                            <a href="{{route('admin.profile.show')}}"><i class="fa fa-users"></i> USER PROFILE</a>
                        </li>
                        <li>
                            <a href="{{route('admin.passwords.show')}}"><i class="fa fa-key"></i> CHANGE PASSWORD</a>
                        </li>
                        {{--<li>
                            <a href="#"><i class="fa fa-cogs"></i> SETTINGS</a>
                        </li>--}}
                        @elseif(Auth::user()->hasRole('super'))
                        <li>
                            <a href="{{route('super.home')}}"><i class="fa fa-home"></i> HOME</a>
                        </li>
                        <li>
                            <a href="{{route('super.profile.show')}}"><i class="fa fa-users"></i> USER PROFILE</a>
                        </li>
                        <li>
                            <a href="{{route('super.passwords.show')}}"><i class="fa fa-key"></i> CHANGE PASSWORD</a>
                        </li>
                        {{--<li>
                            <a href="#"><i class="fa fa-cogs"></i> SETTINGS</a>
                        </li>--}}
                        @elseif(Auth::user()->hasRole('teacher'))
                        <li>
                            <a href=""><i class="fa fa-home"></i> HOME</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-users"></i> USER PROFILE</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-key"></i> CHANGE PASSWORD</a>
                        </li>
                        {{--<li>
                            <a href="#"><i class="fa fa-cogs"></i> SETTINGS</a>
                        </li>--}}
                        @elseif(Auth::user()->hasRole('student'))
                        <li>
                            <a href=""><i class="fa fa-home"></i> HOME</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-users"></i> USER PROFILE</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-key"></i> CHANGE PASSWORD</a>
                        </li>
                        {{--<li>
                            <a href="#"><i class="fa fa-cogs"></i> SETTINGS</a>
                        </li>--}}
                        @else
                        <li>
                            <a href=""><i class="fa fa-home"></i> HOME</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-users"></i> USER PROFILE</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-key"></i> CHANGE PASSWORD</a>
                        </li>
                        {{--<li>
                            <a href="#"><i class="fa fa-cogs"></i> SETTINGS</a>
                        </li>--}}
                        @endif

                    <li>
                        <div class="all-notifications">
                            <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">LOGOUT</a>
                        </div>
                        <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                            {{csrf_field()}}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

