@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                <h5 class="page-title"><i class="fa fa-user"></i>EDIT STUDENT</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            @include('partials.alert')

        <edit-student-component settings="{{$settings}}" states="{{$states}}" personal_data="{{$personal_data}}" classes="{{$classes}}" countries="{{$countries}}" nationalities="{{$nationalities}}"></edit-student-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@section('js')
    <script type="text/javascript">
        $(function () {
            $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
            $('#Student-form-submit').click(function (e) {
                $('#student-form').submit();
           })
        })
    </script>
@endsection

@section('css')
    <style>
        .style-button:hover{
            background-color: white;
            color: black;

        }
        .style-button{
            padding: 9px 12px 9px 12px;
            border-radius: 0;
        }
        .custom-clear-margin-botton{
            margin-bottom: -9px !important;
        }
        .error{
            border-color: #761c19 !important;
        }
    </style>
    @endsection