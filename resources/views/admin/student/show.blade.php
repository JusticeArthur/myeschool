@extends('admin.index')
@section('title','Admin')

@section('css')
    <style>
        .errors input{
            border-color:red;

        }
        .errors select option{
            border-color:red;

        }
        .errors span{
            color:red;
        }
        .all-select{
            margin-top: -20px !important;
            width: 100% !important;
        }
    </style>
@endsection

@section('main')
    <div class="main-content" id="content-wrapper" style="margin-top:5%">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-user-secret"></i>PROFILE</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
        </div>
        @include('partials.alert')
        <div class="row">

                <div class="col-lg-12 clear-padding-xs">
                    <div class="col-md-12">
                        <div class="dash-item first-dash-item">
                            <h6 class="item-title"><i class="fa fa-user"></i>PERSONAL INFO</h6>
                            <div class="inner-item">
                                <div class="dash-form">
                                    <div class="col-sm-3 ">
                                        
                                    </div>
                                    <div class="col-sm-3 ">
                                       
                                    </div>
                                    <div class="col-sm-3">
                                       
                                    </div>
                                    <div class="col-sm-3">
                                       
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-3">
                                       
                                    </div>
                                    <div class="col-sm-3">
                                      
                                    </div>
                                    <div class="col-sm-3 }">
                                       
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="dash-item">
                            <h6 class="item-title"><i class="fa fa-home"></i>Academic Info</h6>
                            <div class="inner-item">
                                <div class="dash-form">
                                    <div class="col-sm-3">
                                        
                                    </div>
                                    <div class="col-sm-3">
                                       
                                    </div>
                                    <div class="col-sm-3">
                                      
                                    </div>
                                    <div class="col-sm-3">
                                       
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="dash-item">
                                <h6 class="item-title"><i class="fa fa-home"></i>Guardian</h6>
                                <div class="inner-item">
                                    <div class="dash-form">
                                        <div class="col-sm-3">
                                            
                                        </div>
                                        <div class="col-sm-3">
                                           
                                        </div>
                                        <div class="col-sm-3">
                                          
                                        </div>
                                        <div class="col-sm-3">
                                           
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        <div class="dash-item">
                            <h6 class="item-title"><i class="fa fa-book"></i></h6>
                            <div class="inner-item">
                                <div class="dash-form">
                                    
                                    </div>
                                    <div class="col-sm-3">
                                   
                                    </div>
                                    <div class="col-sm-3">
                                       
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <br>
                                   
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
               
           
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
        })
    </script>
    @endsection