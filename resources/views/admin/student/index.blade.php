@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-clock-o"></i>ALL STUDENTS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <students-all-component classes="{{$classes}}" students="{{$data}}"></students-all-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@section('js')
    <script>

    function edit() {
        var id = $(".editstudent").attr("id");
        $.ajax({
            method: "GET",
            url: "/admin/students/"+id+"/edit",
            success:function () {
            }
        });
    }
    </script>
@endsection
