@extends('layouts.app')

@push('css')
    <style>
        body{
            background-image: linear-gradient(
                    to right bottom,
                    rgba(126,213,111,0.5),rgba(40,180,111,0.8)),url({{asset('img/register.jpeg')}});
            background-size: cover;
        }
    </style>
@endpush


@section('title','Login')


@section('content')
    @include('partials.alert')
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <login-component></login-component>
        </div>
    </div>
@endsection


@push('scripts')

@endpush