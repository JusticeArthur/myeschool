@extends('layouts.welcome')

@push('css')

@endpush


@section('title','Welcome')


@section('content')
    <!--Slider Area Start-->
    <div class="slider-area">
        <div class="preview-2">
            <div id="nivoslider" class="slides">
                <img src="{{asset('img/slider/1.jpg')}}" alt="" title="#slider-1-caption1"/>
                <img src="{{asset('img/slider/2.jpg')}}" alt="" title="#slider-1-caption2"/>
            </div>
            <div id="slider-1-caption1" class="nivo-html-caption nivo-caption">
                <div class="banner-content slider-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-content-wrapper">
                                    <div class="text-content">
                                        <h1 class="title1 wow fadeInUp" data-wow-duration="2000ms" data-wow-delay="0s">We Provide
                                            <br>Trusted Services
                                            <br> with Options <br> and Flexibility</h1>
                                        <p class="sub-title wow fadeInUp hidden-sm hidden-xs" data-wow-duration="2900ms" data-wow-delay=".5s">
                                            Register Now!
                                        </p>
                                        <div class="banner-readmore wow fadeInUp" data-wow-duration="3600ms" data-wow-delay=".6s">
                                            <a class="button-default" href="{{route('school.register')}}">GET STARTED</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="slider-1-caption2" class="nivo-html-caption nivo-caption">
                <div class="banner-content slider-2">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-content-wrapper">
                                    <div class="text-content slide-2">
                                        <h1 class="title1 wow rotateInDownLeft" data-wow-duration="1000ms" data-wow-delay="0s">All-In-One
                                            <br> ERP Solution
                                            <br> for your <br> institution
                                            </h1>
                                        <p class="sub-title wow rotateInDownLeft hidden-sm hidden-xs" data-wow-duration="1800ms" data-wow-delay="0s">
                                            Register Now!
                                        </p>
                                        <div class="banner-readmore wow rotateInDownLeft" data-wow-duration="1800ms" data-wow-delay="0s">
                                            <a class="button-default" href="{{route('school.register')}}">GET STARTED</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Slider Area-->
    <!--About Area Start-->
    <div class="about-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="about-container">
                        <h3>WHY MYESCHOOL ?</h3>
                        <p>Our mission is to develop myeschool as a school management system which could offer schools, colleges and training institutions a new way of managing their schools which puts student,
                            teachers and users at the centre of a flexible, easy to use and collaborative system.</p>
                        <a class="button-default" href="{{route('school.register')}}">GET STARTED</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br><br><br><br><br>
    <!--End of Course Area-->
    <!--Fun Factor Area Start-->
    <div class="fun-factor-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper white">
                        <div class="section-title">
                            <h3>IMPORTANT FACTS</h3>
                            <p>This is a summary of our members</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="single-fun-factor">
                        <h4>Schools</h4>
                        <h2><span class="counter">18</span>+</h2>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="single-fun-factor">
                        <h4>Users</h4>
                        <h2><span class="counter">400</span>+</h2>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="single-fun-factor">
                        <h4>Countries</h4>
                        <h2><span class="counter">3</span>+</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br><br><br><br>
    <br><!--Newsletter Area Start-->
    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="newsletter-content">
                        <h3>SUBSCRIBE</h3>
                        <h2>TO OUR NEWSLETTER</h2>
                    </div>
                </div>
                <div class="col-md-7 col-sm-7">
                    <div class="newsletter-form angle">
                        <form action="#" class="footer-newsletter fix">
                            <div class="subscribe-form">
                                <input type="email" name="email" placeholder="Enter your email address...">
                                <button type="submit">SUBSCRIBE</button>
                            </div>
                        </form>
                        <!-- mailchimp-alerts Start -->
                        <div class="mailchimp-alerts text-centre fix pull-right">
                            <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                            <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                            <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                        </div>
                        <!-- mailchimp-alerts end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Newsletter Area-->
@endsection


@push('scripts')

@endpush