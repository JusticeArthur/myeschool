@extends('layouts.welcome')

@push('css')

@endpush


@section('title','Contact Us')


@section('content')
    <div class="breadcrumb-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-text">
                        <h1 class="text-center">CONTACT US</h1>
                        <div class="breadcrumb-bar">
                            <ul class="breadcrumb text-center">
                                <li><a href="{{route('myeschool.index')}}">Home</a></li>
                                <li>CONTACT</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-form-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4 class="contact-title">contact info</h4>
                    <div class="contact-text">
                        <p><span class="c-icon"><i class="zmdi zmdi-phone"></i></span><span class="c-text">+233 30 70 70 199 | +233 50 123 99 30</span></p>
                        <p><span class="c-icon"><i class="zmdi zmdi-email"></i></span><span class="c-text">
                                <a href="mailto: info@tecunitgh.com">info@tecunitgh.com</a></span></p>
                        <p><span class="c-icon"><i class="zmdi zmdi-pin"></i></span><span class="c-text">Cantonment Opp. Joyce Ababio School
                                    </span></p>
                    </div>
                    <h4 class="contact-title">social media</h4>
                    <div class="link-social">
                        <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                        <a href="#"><i class="zmdi zmdi-rss"></i></a>
                        <a href="#"><i class="zmdi zmdi-google-plus"></i></a>
                        <a href="#"><i class="zmdi zmdi-pinterest"></i></a>
                        <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-7">
                    <h4 class="contact-title">send your massage</h4>
                    <form id="contact-form">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="name" placeholder="name" required>
                            </div>
                            <div class="col-md-6">
                                <input type="email" name="email" placeholder="Email" required>
                            </div>
                            <div class="col-md-12">
                                <textarea name="message" cols="30" rows="10" placeholder="Message" required></textarea>
                                <button type="submit" class="button-default">SUBMIT</button>
                            </div>
                        </div>
                    </form>
                    <p class="form-messege"></p>
                </div>
            </div>
        </div>
    </div>
    <!--End of Contact Form-->
    <!--Newsletter Area Start-->
    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="newsletter-content">
                        <h3>SUBSCRIBE</h3>
                        <h2>TO OUR NEWSLETTER</h2>
                    </div>
                </div>
                <div class="col-md-7 col-sm-7">
                    <div class="newsletter-form angle">
                        <form action="#" class="footer-newsletter fix">
                            <div class="subscribe-form">
                                <input type="email" name="email" placeholder="Enter your email address...">
                                <button type="submit">SUBSCRIBE</button>
                            </div>
                        </form>
                        <!-- mailchimp-alerts Start -->
                        <div class="mailchimp-alerts text-centre fix pull-right">
                            <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                            <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                            <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                        </div>
                        <!-- mailchimp-alerts end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

@endpush