@extends('layouts.welcome')

@push('css')

@endpush


@section('title','About US')


@section('content')
    <div class="breadcrumb-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-text">
                        <h1 class="text-center">ABOUT US</h1>
                        <div class="breadcrumb-bar">
                            <ul class="breadcrumb text-center">
                                <li><a href="{{route('myeschool.index')}}">Home</a></li>
                                <li>ABOUT US</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Breadcrumb Banner Area-->
    <!--About Page Area Start-->
    <div class="about-page-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper">
                        <div class="section-title">
                            <h3>Who we are</h3>
                            <p>There are many variations of passages</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="about-text-container">
                        <p><span>Well come to Myeschool</span>
                            My-Eschool has a complete list of powerful modules to make management of school easy, quick and efficient. Some of its messaging,
                            sharing and collaboration features makes it a really exciting and engaging experience for teachers, students, parents and sms integration.
                            My-Eschool is the language superior school management system for education institutions.
                            <br>
                            The following is a brief summary of some of the core features:
                        </p>
                        <div class="about-us">
                            <span>REPORTS GENERATION</span>
                            <span>AUTOMATIC ATTENDANCE</span>
                            <span>CLASS WISE REPORT</span>
                            <span>STUDENT REPORT</span>
                            <span>SMS</span>
                            <span>FEES MANAGEMENT</span>
                            <span>EXPENSES MANAGEMENT</span>
                        </div>
                        <p>
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
                        </p>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="skill-image">
                        <img src="img/banner/6.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of About Page Area-->
    <!--Skill And Experience Area Start-->
    <div class="skill-and-experience-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper">
                        <div class="section-title">
                            <h3>Skills and Experience</h3>
                            <p>There are many variations of passages</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="experience-skill-wrapper">
                        <div class="skill-bar-item">
                            <span>Programming</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="80%" style="width: 80%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">80%</span>
                                </div>
                            </div>
                        </div>
                        <div class="skill-bar-item">
                            <span>Designing</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="75%" style="width: 75%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">75%</span>
                                </div>
                            </div>
                        </div>
                        <div class="skill-bar-item">
                            <span>Creative Writing</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="90%" style="width: 90%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">90%</span>
                                </div>
                            </div>
                        </div>
                        <div class="skill-bar-item">
                            <span>English Lessons</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="70%" style="width: 70%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">70%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="experience-skill-wrapper">
                        <div class="skill-bar-item">
                            <span>Programming</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="75%" style="width: 75%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">75%</span>
                                </div>
                            </div>
                        </div>
                        <div class="skill-bar-item">
                            <span>Creative Writing</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="90%" style="width: 90%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">90%</span>
                                </div>
                            </div>
                        </div>
                        <div class="skill-bar-item">
                            <span>Designing</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="80%" style="width: 80%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">80%</span>
                                </div>
                            </div>
                        </div>
                        <div class="skill-bar-item">
                            <span>English Lessons</span>
                            <div class="progress">
                                <div class="progress-bar wow fadeInLeft" data-progress="100%" style="width: 100%;" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                    <span class="text-top">100%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Skill And Experience Area-->
    <!--Teachers Area Start-->
    <div class="teachers-area padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper">
                        <div class="section-title">
                            <h3>OUR CLIENTS</h3>
                            <p>There are many variations of passages of Lorem Ipsum</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="single-teacher-item">
                        <div class="single-teacher-image">
                            <a href="#"><img src="img/teacher/l-1.jpg" alt=""></a>
                        </div>
                        <div class="single-teacher-text">
                            <h3><a href="#">Louis Smith</a></h3>
                            <h4>Teacher</h4>
                            <p>There are many variaons of passa of Lorem Ipsuable, amrn in sofby injected humour, amr</p>
                            <div class="social-links">
                                <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                                <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="single-teacher-item">
                        <div class="single-teacher-image">
                            <a href="#"><img src="img/teacher/l-2.jpg" alt=""></a>
                        </div>
                        <div class="single-teacher-text">
                            <h3><a href="#">Louis Smith</a></h3>
                            <h4>Teacher</h4>
                            <p>There are many variaons of passa of Lorem Ipsuable, amrn in sofby injected humour, amr</p>
                            <div class="social-links">
                                <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                                <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm">
                    <div class="single-teacher-item">
                        <div class="single-teacher-image">
                            <a href="#"><img src="img/teacher/l-3.jpg" alt=""></a>
                        </div>
                        <div class="single-teacher-text">
                            <h3><a href="#">Louis Smith</a></h3>
                            <h4>Teacher</h4>
                            <p>There are many variaons of passa of Lorem Ipsuable, amrn in sofby injected humour, amr</p>
                            <div class="social-links">
                                <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                                <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 hidden-md hidden-sm">
                    <div class="single-teacher-item">
                        <div class="single-teacher-image">
                            <a href="#"><img src="img/teacher/l-4.jpg" alt=""></a>
                        </div>
                        <div class="single-teacher-text">
                            <h3><a href="#">Louis Smith</a></h3>
                            <h4>Teacher</h4>
                            <p>There are many variaons of passa of Lorem Ipsuable, amrn in sofby injected humour, amr</p>
                            <div class="social-links">
                                <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                <a href="#"><i class="zmdi zmdi-google-old"></i></a>
                                <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Teachers Area-->
    <!--Newsletter Area Start-->
    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="newsletter-content">
                        <h3>SUBSCRIBE</h3>
                        <h2>TO OUR NEWSLETTER</h2>
                    </div>
                </div>
                <div class="col-md-7 col-sm-7">
                    <div class="newsletter-form angle">
                        <form action="#" class="footer-newsletter fix">
                            <div class="subscribe-form">
                                <input type="email" name="email" placeholder="Enter your email address...">
                                <button type="submit">SUBSCRIBE</button>
                            </div>
                        </form>
                        <!-- mailchimp-alerts Start -->
                        <div class="mailchimp-alerts text-centre fix pull-right">
                            <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                            <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                            <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                        </div>
                        <!-- mailchimp-alerts end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

@endpush