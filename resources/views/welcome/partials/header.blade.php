<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-5 hidden-xs">
                    <span>Have any question? +233 55 702 800</span>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
                    <div class="header-top-right">
                        <div class="content"><a href="#"><i class="zmdi zmdi-account"></i> My Account</a>
                            <ul class="account-dropdown">
                                <li><a href="{{route('login.validate')}}">Log In</a></li>
                                <li><a href="{{route('school.register')}}">Register</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-logo-menu sticker">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="logo">
                        <a href="{{route('myeschool.index')}}"><img  width="100" style="border-radius:50%;padding-bottom: 3px" src="{{asset('image_home/logo/myeschoolgh.png')}}" alt="MYESCHOOL"></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="mainmenu-area pull-right">
                        <div class="mainmenu hidden-sm hidden-xs">
                            @php
                            $segment = Request::segment(1);
                                    @endphp
                            <nav>
                                <ul id="nav">
                                    <li class="{{$segment == ''?'current':''}}"><a href="{{route('myeschool.index')}}">Home</a></li>
                                    <li class="{{$segment == 'sales-reps'?'current':''}}"><a href="{{route('home.sales_reps')}}">SALES REPS</a></li>
                                    <li class="{{$segment == 'contact'?'current':''}}"><a href="{{route('home.contact')}}">Contact</a></li>
                                    <li class="{{$segment == 'about'?'current':''}}">
                                        <a href="{{route('home.about')}}">About</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu Area start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a href="{{route('myeschool.index')}}">HOME</a>
                                </li>
                                <li><a href="{{route('home.contact')}}">Contact us</a></li>
                                <li><a href="{{route('home.sales_reps')}}">Sales Reps</a></li>
                                <li><a href="{{route('home.about')}}">About Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu Area end -->
</header>
