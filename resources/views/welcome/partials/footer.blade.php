<!--Footer Widget Area Start-->
<div class="footer-widget-area">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <div class="single-footer-widget">
                    <div class="text-center footer-logo">
                        <a href="{{route('myeschool.index')}}"><img style="border-radius: 50%" src="{{asset('image_home/logo-original.jpg')}}" alt=""></a>
                    </div>
                    <p> My-Eschool has a complete list of powerful modules to make management of school easy,
                        quick and efficient. Some of its messaging,  </p>
                    <div class="social-icons">
                        <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                        <a href="#"><i class="zmdi zmdi-rss"></i></a>
                        <a href="#"><i class="zmdi zmdi-google-plus"></i></a>
                        <a href="#"><i class="zmdi zmdi-pinterest"></i></a>
                        <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="single-footer-widget">
                    <h3>GET IN TOUCH</h3>
                    <a href="tel:555-555-1212"><i class="fa fa-phone"></i>+233 55 702 800</a>
                    <span><i class="fa fa-envelope"></i>info@tecunitgh.com</span>
                    <span><i class="fa fa-globe"></i>www.educat.com</span>
                    <span><i class="fa fa-map-marker"></i>Cantonment Opp. Joyce Ababio</span>
                </div>
            </div>
            <div class="col-md-3 hidden-sm">
                <div class="single-footer-widget">
                    <h3>Useful Links</h3>
                    <ul class="footer-list">
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Request For Demo</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="single-footer-widget">
                    <h3>Instagram</h3>
                    <div class="instagram-image">
                        <div class="footer-img">
                            <a href="#"><img src="{{asset('img/footer/1.jpg')}}" alt=""></a>
                        </div>
                        <div class="footer-img">
                            <a href="#"><img src="{{asset('img/footer/2.jpg')}}" alt=""></a>
                        </div>
                        <div class="footer-img">
                            <a href="#"><img src="{{asset('img/footer/3.jpg')}}" alt=""></a>
                        </div>
                        <div class="footer-img">
                            <a href="#"><img src="{{asset('img/footer/4.jpg')}}" alt=""></a>
                        </div>
                        <div class="footer-img">
                            <a href="#"><img src="{{asset('img/footer/5.jpg')}}" alt=""></a>
                        </div>
                        <div class="footer-img">
                            <a href="#"><img src="{{asset('img/footer/6.jpg')}}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Footer Widget Area-->
<!--Footer Area Start-->
<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-7">
                <span>Copyright &copy; MyESchool 2019.All right reserved.Created by <a href="#">TecUnit GH</a></span>
            </div>
            <div class="col-md-6 col-sm-5">
                <div class="column-right">
                    <span>Privacy Policy , Terms &amp; Conditions</span>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--End of Footer Area-->