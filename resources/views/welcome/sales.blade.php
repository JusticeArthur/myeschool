@extends('layouts.welcome')

@push('css')

@endpush


@section('title','Sales Reps')


@section('content')
    <div class="breadcrumb-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-text">
                        <h1 class="text-center">SALES REPRESENTATIVES</h1>
                        <div class="breadcrumb-bar">
                            <ul class="breadcrumb text-center">
                                <li><a href="{{route('myeschool.index')}}">Home</a></li>
                                <li>SALES REPS</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Breadcrumb Banner Area-->
    <!--About Page Area Start-->
    <div class="about-page-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title-wrapper">
                        <div class="section-title">
                            <h3>Sweet Offers</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="about-text-container">
                        <p>
                            Sales Reps would get introduced to the software, and understand the purpose and features of the Software.
                        </p>
                        <p>
                            Flyers, brochures, Proposals and presentation materials would be giving to the rep for professional presentation.
                        </p>
                        <p>
                            Sales Reps would be providing us with identification card (Password, National Health Insurance) and address information.
                        </p>
                        <p>
                            Sales Reps would be given identification cards to be easily identified.
                        </p>
                        <p>
                            Sales Reps can help/assist the school or recommend PC (Hardware specs) or Internet for good service to be provided
                        </p>
                        <p>
                            When a school subscribes the school would select the name of the sales reps name in the registration form by asking the Sales rep for hi ID Number.
                        </p>
                        <p>
                            Sales reps get a commission when a school signs up and also get commission when He brings five (5) Schools on board.
                        </p>
                        <p>
                            Sales Reps can work anywhere, anytime, using a laptop or smartphones and also would be supported with data or credit when the needs arrive.
                        </p>
                        <p>
                            All financial Agreement would be discussed between Management and Sales Reps
                        </p>
                        <p>
                            A document would be sent to all sales rep to understand their profit they would make when they sign up.
                        </p>
                        <p>
                            If you are interested to become a Sales Representative for
                            myeschoolghana you can contact us now on +233 30 700 1081 or email us at
                            <a href="">info@tecunitgh.com</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="skill-image">
                        <img src="img/banner/6.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="newsletter-content">
                        <h3>SUBSCRIBE</h3>
                        <h2>TO OUR NEWSLETTER</h2>
                    </div>
                </div>
                <div class="col-md-7 col-sm-7">
                    <div class="newsletter-form angle">
                        <form action="#" class="footer-newsletter fix">
                            <div class="subscribe-form">
                                <input type="email" name="email" placeholder="Enter your email address...">
                                <button type="submit">SUBSCRIBE</button>
                            </div>
                        </form>
                        <!-- mailchimp-alerts Start -->
                        <div class="mailchimp-alerts text-centre fix pull-right">
                            <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                            <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                            <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                        </div>
                        <!-- mailchimp-alerts end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

@endpush