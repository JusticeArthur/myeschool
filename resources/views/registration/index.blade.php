@extends('layouts.app')

@push('css')
    <style>
        body{
            background-image: linear-gradient(
                    to right bottom,
                    rgba(126,213,111,0.5),rgba(40,180,111,0.8)),url({{asset('img/register.jpeg')}});
            background-size: cover;
        }
    </style>
@endpush


@section('title','Register')


@section('content')
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <register-component></register-component>
        </div>
    </div>
@endsection


@push('scripts')

@endpush