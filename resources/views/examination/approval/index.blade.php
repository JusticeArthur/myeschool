@extends('admin.index')

@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-clock-o"></i>RESULTS APPROVALS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <approve-exam-results approvals="{{$approvals}}"></approve-exam-results>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')



    </div>
@endsection


