@extends('teacher.index')

@push('css')

@endpush


@section('title','Submissions')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-eye"></i>SUBMISSIONS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <div class="row">
                <examination-submissions classes="{{$classes}}"></examination-submissions>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush