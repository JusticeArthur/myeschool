@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-calculator"></i>SMS STATISTICS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <!--<sms-detail-selection></sms-detail-selection>-->
                    <div class="col-sm-12">
                        <div class="dash-item">
                            <h6 class="item-title"><i class="fa fa-sliders"></i>ALL SMS</h6>
                            <div class="inner-item first-dash-item">
                                <table class="table table-bordered table-striped table-responsive table-condensed">
                                    <thead>
                                    <tr>
                                        <td class="text-center"><strong>STATUS</strong></td>
                                        <td class="text-center"><strong>UNITS USED</strong></td>
                                        <td class="text-center"><strong>SENT BY</strong></td>
                                        <td class="text-center"><strong>TEXT</strong></td>
                                       {{-- <td class="text-center"><strong>ACTIONS</strong></td>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sms as $item)
                                        <tr>
                                            <td class="text-center">SENT</td>
                                            <td class="text-center"><strong>{{$item->count}}</strong></td>
                                            <td class="text-center"><strong>{{$item->user->name}}</strong></td>
                                            <td>{{$item->text}}</td>
                                           {{-- <td class="text-center">
                                                <a href="{{route('sms.details.index',['id'=>$item->message_id])}}" class="btn btn-primary">
                                                    VIEW
                                                </a>
                                            </td>--}}
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$sms->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection

@section('js')
@endsection

@push('scripts')

@endpush
