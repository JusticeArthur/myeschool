@extends('admin.index')

@push('css')

@endpush


@section('title','Admin')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-calculator"></i>SMS STATISTICS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <!--<sms-detail-selection></sms-detail-selection>-->
                    <div class="col-sm-12">
                        <div class="dash-item">
                            <h6 class="item-title"><i class="fa fa-sliders"></i>SENDER INFO</h6>
                            <div class="inner-item first-dash-item">
                                <table class="table table-bordered table-striped table-responsive table-condensed">
                                    <thead>
                                    <tr>
                                        <td class="text-center"><strong>SENT BY</strong></td>
                                        <td class="text-center"><strong>SENDER</strong></td>
                                        <td class="text-center"><strong>TO</strong></td>
                                        <td class="text-center"><strong>STATUS</strong></td>
                                        <td class="text-center"><strong>TIME</strong></td>
                                        <td class="text-center"><strong>UNITS</strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center"><strong>{{$sms->user->name}}</strong></td>
                                        <td class="text-center"><strong>Myeschool</strong></td>
                                        <td class="text-center"><strong>+{{$sms->to}}</strong></td>
                                        <td class="text-center"><strong>{{$status}}</strong></td>
                                        <td class="text-center"><strong>{{$time}}</strong></td>
                                        <td class="text-center"><strong>{{$sms->count}}</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="dash-item">
                            <h6 class="item-title"><i class="fa fa-sliders"></i>SMS MESSAGE</h6>
                            <div class="inner-item dash-form">
                                <div class="col-sm-12">
                                    <textarea readonly>
                                        {{trim($sms->data)}}
                                    </textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')

    </div>
@endsection

@section('js')
@endsection

@push('scripts')

@endpush
