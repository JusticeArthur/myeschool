<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

    <!-- favicon
    ============================================ -->
    <link rel="icon" type="image/ico" href="{{asset('img/logo.ico')}}">

    <!-- Google Fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- Fontawsome CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- Owl Carousel CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">

    <!-- jquery-ui CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">

    <!-- Meanmenu CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/meanmenu.min.css')}}">

    <!-- Animate CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <!-- Animated Headlines CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/animated-headlines.css')}}">

    <!-- Nivo slider CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('lib/nivo-slider/css/nivo-slider.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('lib/nivo-slider/css/preview.css')}}" type="text/css" media="screen" />

    <!-- Metarial Iconic Font CSS
    ============================================ -->
    {{--<link rel="stylesheet" href="{{asset('css/material-design-iconic-font.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/material-design-iconic-font.min.css')}}">

    <!-- Slick CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">

    <!-- Video CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/jquery.mb.YTPlayer.css')}}">

    <!-- Style CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- Color CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/color.css')}}">

    <!-- Responsive CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

    <!-- Modernizr JS
    ============================================ -->
    <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!--Main Wrapper Start-->
<div class="as-mainwrapper">
    <!--Bg White Start-->
    <div class="bg-white">
        <!--Header Area Start-->
        @include('welcome.partials.header')
        <!--End of Header Area-->
        @yield('content')
        @include('welcome.partials.footer')
    </div>
    <!--End of Bg White-->
</div>
<!--End of Main Wrapper Area-->


<!-- jquery
============================================ -->
<script src="{{asset('js/vendor/jquery-1.12.4.min.js')}}"></script>

<!-- bootstrap JS
============================================ -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<!-- nivo slider js
============================================ -->
<script src="{{asset('lib/nivo-slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
<script src="{{asset('lib/nivo-slider/home.js')}}" type="text/javascript"></script>

<!-- meanmenu JS
============================================ -->
<script src="{{asset('js/jquery.meanmenu.js')}}"></script>

<!-- wow JS
============================================ -->
<script src="{{asset('js/wow.min.js')}}"></script>

<!-- owl.carousel JS
============================================ -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>

<!-- scrollUp JS
============================================ -->
<script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>

<!-- Waypoints JS
============================================ -->
<script src="{{asset('js/waypoints.min.js')}}"></script>

<!-- Counterup JS
============================================ -->
<script src="{{asset('js/jquery.counterup.min.js')}}"></script>

<!-- Slick JS
============================================ -->
<script src="{{asset('js/slick.min.js')}}"></script>

<!-- Animated Headlines JS
============================================ -->
<script src="{{asset('js/animated-headlines.js')}}"></script>

<!-- Textilate JS
============================================ -->
<script src="{{asset('js/textilate.js')}}"></script>

<!-- Lettering JS
============================================ -->
<script src="{{asset('js/lettering.js')}}"></script>

<!-- Video Player JS
============================================ -->
<script src="{{asset('js/jquery.mb.YTPlayer.js')}}"></script>

<!-- AJax Mail JS
============================================ -->
<script src="{{asset('js/ajax-mail.js')}}"></script>

<!-- plugins JS
============================================ -->
<script src="{{asset('js/plugins.js')}}"></script>


<!-- main JS
============================================ -->
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>