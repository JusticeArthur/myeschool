<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="{{asset('assets/css/owl.carousel.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('assets/css/owl.theme.default.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" media="screen">
    <link rel="icon" type="image/ico" href="{{asset('img/logo.ico')}}">


    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" media="screen">
    <style>
        .footer-row{
            background-image: linear-gradient(
                    rgba(43, 59, 55, 0.5), rgba(0, 0, 0, 0.8));
        }
        .navbar-brand img{
            height: 120px;
            transition: transform .6s ease-in;
        }
        .navbar-brand img:hover{
            transform: rotate(-25deg);
        }
    </style>
    @stack('css')
</head>
<body>
@include('partials.alert')
@php
    $segment = Request::segment(1);
@endphp
<div id="app">
    <div class="row nav-row trans-menu">
        <div class="container nav-container">
            <div class = "clearfix"></div>
            <nav id="pathshalaNavbar" class="navbar navbar-default" data-spy="affix" data-offset="300" role="navigation" >
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#pathshalaNavbarCollapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href=""><img src="{{asset('image_home/svg/logo-1.svg')}}" alt=""></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="pathshalaNavbarCollapse">
                    <ul class="nav navbar-nav">
                        <li class="{{$segment == ''?'current':''}}"><a href="/">HOME</a></li>
                        <li class="{{$segment == 'sales-reps'?'current':''}}"><a href="{{route('home.sales_reps')}}">SALES REPS</a></li>
                        <li class="{{$segment == 'about'?'current':''}}"><a href="{{route('home.about')}}">ABOUT US</a></li>
                        <li class="{{$segment == 'contact'?'current':''}}"><a href="{{route('home.contact')}}">CONTACT</a></li>
                        <li class="login_style">  <a href="{{route('login.validate')}}">LOGIN</a></li>
                        <li class="register_style"><a href="{{route('school.register')}}"  data-target="#loginModal">REGISTER</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>
@yield('content')
    @include('partials.footer')
</div>


<!-- Scripts -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('assets/plugins/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/js.js')}}"></script>
@stack('js')
</body>
</html>
