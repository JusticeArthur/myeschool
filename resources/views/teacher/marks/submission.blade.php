@extends('teacher.index')

@push('css')

@endpush


@section('title','Student')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <assessment-submission-component classes="{{$classes}}"></assessment-submission-component>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush