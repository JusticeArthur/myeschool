@extends('teacher.index')

@push('css')

@endpush


@section('title','Student')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-eye"></i>VIEW MARKS</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <div class="row">
                <assessment-view-component classes="{{$classes}}"></assessment-view-component>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush