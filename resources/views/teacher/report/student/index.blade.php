@extends('teacher.index')

@push('css')

@endpush


@section('title','Student')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 clear-padding-xs">
                    <h5 class="page-title"><i class="fa fa-eye"></i>{{strtoupper($student->lname)}} {{strtoupper($student->mname)}} {{strtoupper($student->fname)}}</h5>
                    <div class="section-divider"></div>
                </div>
            </div>
            <div class="row">
                <student-report-component approved="{{$approved}}" admin="{{$admin}}" comments="{{$comments}}" remarks="{{$remarks}}" year="{{$year}}" term="{{$term}}" all_students="{{$all_students}}" overall_rank="{{$overall_rank}}" student_courses="{{$student_courses}}" settings="{{$settings}}" student="{{$student}}" courses="{{$courses}}"></student-report-component>
            </div>
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')
    </div>
@endsection


@push('scripts')

@endpush