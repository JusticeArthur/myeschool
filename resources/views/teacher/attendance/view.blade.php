@extends('admin.index')

@section('css')
    <style>
        .edit-submit{
            cursor: pointer;
        }
    </style>
@endsection


@section('title','Teacher')


@section('main')
    <div class="main-content" id="content-wrapper">
        <div class="container-fluid">
            <show-student-attendance-component classes="{{$classes}}"></show-student-attendance-component>
            @include('partials.alert')
        </div>
        <div class="menu-togggle-btn">
            <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        @include('admin.partials.footer')

    </div>
@endsection


@section('js')
    <script type="text/javascript">
        $(function () {
            $('.save-items').click(function (e) {
                e.preventDefault();
                $('#delete-form').submit();
            })
        })
    </script>
@endsection