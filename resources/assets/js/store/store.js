import Vue from 'vue';
import  Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        classes:[],
        courses:[],
        students:[],
        names:[],
        reports:[],
        dates:[],
        attendances:[],
        years:[],
        terms:[],
        roles:[],
        bill:{},
        tables:[],
        permissions:[],
        current_role:'',
        slots:[],
        classTimeTable:[],
        classSlots:[],
        class_name:null,
        Updatecourses:[],
        UpdateclassTimeTable:[],
        UpdateclassSlots:[],
        remarks:[],
        smsSelection:[],
        searchPlaceHolder:[],
        parents:[],
        staff:[],
        settings:[],
        expenseCategories:[],
        incomeCategories:[],
        expenses:[],
        all_years:[],
        teachers:[],
        pairings:[],
        items:[],breakdowns:[],approvals:[],bills:[],paymentMethods:[],
        transactions:[],countries:[],states:[],employees:[]
    }
});