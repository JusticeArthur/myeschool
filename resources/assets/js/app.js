/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
window.SmsHelper = require('smshelper');
window.Vue = require('vue');
import {store} from "./store/store";
import VueTelInput from 'vue-tel-input';
Vue.use(VueTelInput)
import ElementUI from 'element-ui';
Vue.use(ElementUI);
import VueDataTables from 'vue-data-tables';
window.moment = require('moment');
Vue.use(VueDataTables);

import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import Datetime from 'vue-datetime';
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'

Vue.use(Datetime)

locale.use(lang)
///register date time globally

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('register-component', require('./components/register.vue'));
Vue.component('login-component', require('./components/Login.vue'));
Vue.component('reset-component', require('./components/reset.vue'));
Vue.component('password-component', require('./components/passwordReset.vue'));
Vue.component('teacher-component', require('./admincomponents/addTeacher.vue'));
Vue.component('add-class-component', require('./admincomponents/classes/index.vue'));
Vue.component('add-student-component', require('./admincomponents/student.vue'));
Vue.component('add-student-guardian-component', require('./admincomponents/guardian.vue'));
Vue.component('add-student-existing-guardian-component', require('./admincomponents/choose_guardian.vue'));
Vue.component('breakdown-component', require('./admincomponents/feeItems/index.vue'));
Vue.component('academic-component', require('./admincomponents/details/academic_year/index.vue'));
Vue.component('term-component', require('./admincomponents/term.vue'));
Vue.component('fees-component', require('./admincomponents/feeDetails/index.vue'));
Vue.component('course-component', require('./admincomponents/classCourse/index.vue'));
Vue.component('class-courses-component', require('./admincomponents/courses/index.vue'));
Vue.component('sms-parent',require('./admincomponents/sms_parent.vue'));
Vue.component('add-fees',require('./accountant/addFee.vue'));
Vue.component('settings-component',require('./settings/institution.vue'));
Vue.component('assessment-index-component',require('./assessment/index.vue'));
Vue.component('assessment-view-component',require('./assessment/view.vue'));
Vue.component('assessment-submission-component',require('./assessment/submission.vue'));
Vue.component('reports-student-generate-component',require('./reports/index.vue'));
Vue.component('reports-generate-component',require('./reports/student.vue'));
Vue.component('student-report-component',require('./reports/studentReport.vue'));
Vue.component('mark-student-attendance-component',require('./attendance/attendance.vue'));
Vue.component('show-student-attendance-component',require('./attendance/view.vue'));
Vue.component('student-profile-component',require('./settings/student/student_fields.vue'));
//roles components
Vue.component('admin-role-component',require('./admincomponents/roles/index.vue'));
//time table components
Vue.component('timetable-component',require('./admincomponents/timetables/create.vue'));
//slot components
Vue.component('slot-component',require('./admincomponents/slots/index.vue'));
//remarks components
Vue.component('remark-component',require('./admincomponents/remarks/remark.vue'));
//sms components
Vue.component('sms-component',require('./sms/index.vue'));
//sms details
/*Vue.component('sms-detail-component',require('./sms/details/index.vue'));*/
//finance components
//expense categories
Vue.component('add-expense-component',require('./finance/expenses/categories/index.vue'));
//income categories
Vue.component('add-income-component',require('./finance/incomes/categories/index.vue'));
//expenses

Vue.component('expenses-component',require('./finance/expenses/expenses.vue'));
Vue.component('incomes-component',require('./finance/incomes/income.vue'));
//students components continuation
//fee settings
Vue.component('fee-settings-component',require('./settings/fee/index.vue'));
//bill classes
Vue.component('student-bill-component',require('./finance/bills/index.vue'));
//payment methods
Vue.component('payment-category-component',require('./finance/payments/index.vue'));
//fees
Vue.component('fee-collection-component',require('./finance/fee/index.vue'));
//transaction-history
Vue.component('transaction-history-component',require('./finance/history/index.vue'));
//notifications
Vue.component('notify-index-component',require('./notifications/index.vue'));
//manage employees
Vue.component('add-role-component',require('./admincomponents/employees/roles/index.vue'));
Vue.component('add-employee-component',require('./admincomponents/employees/add.vue'));
Vue.component('all-employees-component',require('./admincomponents/employees/all.vue'));
Vue.component('edit-employee-component',require('./admincomponents/employees/edit.vue'));
Vue.component('employee-profile-component',require('./admincomponents/employees/profile.vue'));

Vue.component('students-all-component',require('./admincomponents/students/allStudents.vue'));
Vue.component('edit-student-component',require('./admincomponents/editstudent.vue'));
Vue.component('student-detail-component',require('./admincomponents/students/details/index.vue'));
//report templates
Vue.component('report-template-component',require('./admincomponents/details/templates/reports.vue'))
//all guardians
Vue.component('all-guardians-component',require('./admincomponents/details/guardians/index.vue'));
Vue.component('finance-notifications-component',require('./finance/notifications/index.vue'));
//print bills
Vue.component('finance-bills-print',require('./finance/bills/print/index.vue'));
//examination components
Vue.component('approve-exam-results',require('./examination/approval/index.vue'));
Vue.component('examination-submissions',require('./examination/submisssions/index.vue'));

//search on the home
Vue.component('app-header-search',require('./home/home-search.vue'))

//directives
Vue.directive('click-outside',{
    bind(el,binding,vnode){
        el.clickOutsideEvent = function (event) {
            if(!(el == event.target) || el.contains(event.target)){
                vnode.context[binding.expression](event);
            }
        };
        document.body.addEventListener('click',el.clickOutsideEvent)
    },
    unbind(el){
        document.body.removeEventListener('click',el.clickOutsideEvent)
    },
    stopProp(event){
        event.stopPropagation()
    }
})
const app = new Vue({
    el: '#app',
    store,
});
