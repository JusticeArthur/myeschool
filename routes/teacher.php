<?php
Route::get('/','Teacher\HomeController@home')->name('teacher.home');
Route::resources([
    'assignments'=>'Teacher\AssController',
]);
//assignment routes
Route::get('/assignment-create','Teacher\AssignmentController@showCreate')->name('teacher.assignment.create');
Route::get('/assignment-download','Teacher\AssignmentController@showDownload')->name('teacher.assignment.download');

//attendance routes
Route::get('/attendance-view/data/{id}','Teacher\AttendanceController@attendanceInfo')->name('teacher.attendance.view.details');
Route::get('/attendance-mark','Teacher\AttendanceController@showMark')->name('teacher.attendance.mark');
Route::get('/attendance-mark/{id}','Teacher\AttendanceController@getStudentsToMarkAttendance');

Route::get('/attendance-dates/{id}','Teacher\AttendanceController@getAttendanceDates');
Route::post('/attendance-dates-student','Teacher\AttendanceController@getAttendanceDateStudent');
Route::post('/attendance-mark/save','Teacher\AttendanceController@markAttendance')->name('teacher.attendance.mark.save');
Route::get('/attendance-view','Teacher\AttendanceController@showView')->name('teacher.attendance.view');
Route::post('/attendance-view/update','Teacher\AttendanceController@updateAttendanceInfo');

//showMessage
Route::get('/teacher-messages','Teacher\NotificationController@showMessage')->name('teacher.messages');

//marks routes
Route::get('/marks-view','Teacher\MarkController@showView')->name('teacher.marks.view');
Route::get('/marks-add','Teacher\MarkController@showAdd')->name('teacher.marks.add');

//timetable
Route::get('/teacher-timetable','Teacher\TimetableController@showTimetable')->name('teacher.timetable');

//report routes
Route::get('/teacher-report-attendance','Teacher\ReportController@showAttendance')->name('teacher.report.attendance');
Route::get('/teacher-report-marks','Teacher\ReportController@showMarks')->name('teacher.report.marks');
Route::prefix('reports')->group(function (){
    Route::resources([
        'assessments'=>'Teacher\AssessmentController'
    ]);
    Route::get('teacher-class-courses/{id}','Teacher\MiscController@getClassTeacherCourses');
    Route::post('teacher-class-courses','Teacher\MiscController@getClassCourse');
    Route::post('teacher-class-courses-names','Teacher\MiscController@getClassCourseNames');
    Route::post('teacher-class-courses-names-students','Teacher\MiscController@getClassCourseNameStudents');
    Route::post('mass-update','Teacher\MiscController@massUpdate');
    Route::post('mass-delete','Teacher\MiscController@massDelete');
    Route::get('submissions','Teacher\MiscController@showSubmission')->name('teacher.reports.submissions');
    Route::post('submissions','Teacher\MiscController@submitAssessment');
    Route::get('generate','Teacher\MiscController@generateReportsForClass')->name('teacher.reports.generate');
    Route::get('generate/students','Teacher\MiscController@generateReportsForClass')->name('teacher.reports.generate.students');
    Route::get('generate/class/{id}','Teacher\MiscController@getReportsForClass');
    Route::get('generate/student/{id}','Teacher\MiscController@cacheStudentId');
    Route::post('submit-student-comment','Teacher\MiscController@saveStudentComment');
});


