<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@welcome')->name('myeschool.index');

Route::get('/register','HomeController@register')->name('school.register');

Route::get('/login','HomeController@login')->name('login.validate');
Route::get('/about','HomeController@about')->name('home.about');
Route::get('/contact','HomeController@contact')->name('home.contact');
Route::get('/sales-reps','HomeController@salesReps')->name('home.sales_reps');
Route::post('/register-school','Misc\MiscController@regSchool');
Route::post('/login-user','Misc\MiscController@login');
//verify user
Route::get('/user/verify/{token}', 'Misc\MiscController@verifyUser');
Route::post('/user/verify-sms','Misc\MiscController@verifySMS');
Route::post('/logout','Misc\MiscController@logout')->name('user.logout');

//password resets
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');



//////////////////////////protected with permissions ////////////////////////////
///
Route::group(['middleware'=>'authenticated'],function (){
    Route::resources([
        'timetables'=>'Admin\TimeTableController',
    ]);
    Route::get('/dashboard', 'HomeController@dashboard')->name('myeschool.local.dashboard');
    //endpoints with the authentication
    Route::get('/teachers/endpoint','Misc\EndPointController@names')->name('teacher.names.endpoint');
    Route::get('/teachers-classes/endpoint','Misc\EndPointController@teacherClass')->name('teacher.classes.endpoint');

//items endpoint
    Route::get('/items/endpoint','Misc\EndPointController@items')->name('items.fees.endpoint');
    Route::get('/date/generation','Misc\EndPointController@generateYears');
//academic year endpoint
    Route::get('/date/academic/years','Misc\EndPointController@showAcademicYears')->name('date.academic.years');
///term endpoints
    Route::get('/date/acade/terms','Misc\EndPointController@showFromYearsTable');
    Route::get('/date/academic/terms','Misc\EndPointController@makeDataTableTerms')->name('date.academic.terms');
    Route::get('/date/terms/{name}','Misc\EndPointController@showTerms');
//fees endpoint particulars
    Route::get('/date/items/show-all','Misc\EndPointController@showAllItems');
    Route::get('/date/classes/show-all','Misc\EndPointController@showAllClasses');
//fees endpoint
    Route::get('/date/fees/class-distinct','Misc\EndPointController@feesDistinctClass')->name('date.fees.class_distinct');

//courses endpoint
    Route::get('/date/courses/show-all','Misc\EndPointController@allCourses')->name('date.courses.show.all');
    Route::get('/date/class-courses/show-all','Misc\EndPointController@allClassCourses')->name('date.class_courses.show.all');

    Route::get('/date/parents/all','Misc\EndPointController@showParents');

    Route::post('/submit_comment','Misc\ContactUsController@store')->name('user.contact.us');

//fees payment
    Route::get('/date/students/all/{id}','Misc\EndPointController@showParticularStudent');
    Route::get('/date/students/all/previous/{id}','Misc\EndPointController@showStudentPrevious');
    Route::get('/date/all/universities','Misc\EndPointController@getAllUniversities');
    Route::get('/date/all/universities/years','Misc\EndPointController@allYearsPassed');
});

Route::get('/time-tables/getTeachers/{id}','Admin\TimeTableController@getTeachers')->middleware('authenticated');

///manage sms routes
Route::group(['prefix'=>'manage-sms','middleware'=>'authenticated'],function (){
    Route::get('index','SMS\SMSController@index')->name('manage_all_sms.sms');
    Route::get('send','SMS\SMSController@create')->name('manage_all_sms.sms.send');
    //set staff for sms
    Route::get('sms-for-staff','SMS\SMSController@smsForStaff');
    Route::get('sms-for-students','SMS\SMSController@smsForStudent');
    Route::get('sms-for-parents','SMS\SMSController@smsForParent');
    Route::get('sms-for-parents/{class_id}','SMS\SMSController@getStudentForParentClass');
    Route::get('sms-for-class/{class_id}','SMS\SMSController@getStudentForClass');
    Route::post('send/{id?}','SMS\SMSController@sendSMS');
    Route::post('send-single/{id?}','SMS\SMSController@sendSingleSMS');
    Route::post('callback','SMS\SMSController@smsCallback')->name('sms.callback');
    //get sms details
    Route::get('sms-details','SMS\SMSController@getSMSDetailsIndex')->name('sms.details');
    Route::get('sms-details/{id}','SMS\SMSController@getSMSDetailParticular')->name('sms.details.index');
});
//////////////finance routes///////////////////////////
Route::get('finances','Finance\IndexController@index')->name('manage_all.finances')->middleware('authenticated');
Route::group(['prefix'=>'finances','middleware'=>'authenticated'],function(){
    Route::resources([
        'expenses'=>'Finance\Expense\ExpenseController',
        'expenses-categories'=>'Finance\Expense\ExpenseCategoryController',
        'income-categories'=>'Finance\Income\IncomeCategoryController',
        'incomes'=>'Finance\Income\IncomeController',
        'student-bills'=>'Finance\Bills\BillController',
        'payments'=>'Finance\Payments\CategoryController',
        'student-fees'=>'Finance\Fees\FeeController'
    ]);
    Route::post('bulk-delete','Finance\Expense\ExpenseCategoryController@bulkDelete');
    Route::post('income-cat-bulk-delete','Finance\Income\IncomeCategoryController@bulkDelete');
    Route::prefix('student-fees')->group(function (){
        Route::get('students/{id}','Finance\Fees\FeeController@getStudentForClass');
        Route::get('students-wallet/{id}','Finance\Fees\FeeController@getStudentWallet');
    });
    //Finance\History\TransactionHistoryController
    Route::prefix('transaction-history')->group(function (){
        Route::get('index','Finance\History\TransactionHistoryController@index')
            ->name('transaction_history.index');
        Route::post('index','Finance\History\TransactionHistoryController@filterResults');
    });
    Route::get('owing-students','Finance\NotificationController@index')->name('finance.notifications.index');
    Route::post('owing-students/search','Finance\NotificationController@searchStudent');
    Route::get('manage-bills','Index\IndexController@getManageFinanceBills')->name('finance.manage.bills.home');
    //print bills
    Route::get('print-bills','Finance\Bills\PrintController@index')->name('finance.print.bills.index');
    Route::post('print-bills','Finance\Bills\PrintController@fetchBills');
});
//notifications
Route::group(['prefix'=>'notifications','middleware'=>'authenticated'],function (){
    Route::get('index','Notifications\NotificationController@index')->name('notifications.index');
});
//manage exams
Route::group(['prefix'=>'examination','middleware'=>'authenticated'],function(){
    Route::get('','Index\IndexController@getManageExams')->name('manage.exams.index');
    Route::resources([
        'exam-approvals'=>'Examination\ApproveController',
        'remarks'=>'Admin\RemarkController',
    ]);
    Route::get('report-template','Admin\DetailController@getReportCardTemplate')->name('get.report.template');
    Route::get('report-template-add/{id}','Admin\DetailController@setSchoolTemplate');
    Route::get('submissions','Examination\SubmissionController@index')->name('examinations.submissions.index');
    Route::get('class/{id}','Examination\SubmissionController@getClassCourses');
    Route::post('reports','Examination\SubmissionController@getSubmissions');
});

//search on the header
Route::post('search-header','Home\SearchController@search');

