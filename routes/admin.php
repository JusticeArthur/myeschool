<?php
Route::get('/','Admin\HomeController@home')->name('admin.home');

//notifications controller
Route::get('/messages','Admin\NotificationController@messages')->name('admin.messages');
Route::get('/annoucements','Admin\NotificationController@annoucements')->name('admin.annoucements');

Route::resources([
    'students'=>'Admin\StudentController',
    'teachers'=>'Admin\TeacherController',
    'classes'=>'Admin\ClassesController',
    'sections'=>'Admin\SectionController',
    'courses'=>'Admin\CourseController',
    'class_courses'=>'Admin\ClassCourseController',
    'accountants'=>'Admin\AccountantController',
    'slots'=>'Admin\SlotController',
    'roles'=>'Admin\RoleController',
]);
//manage employees
Route::prefix('manage-employees')->group(function (){
    Route::resources([
        'school-roles'=>'Admin\Employees\Roles\RoleController',
        'employees'=>'Admin\Employees\EmployeeController'
    ]);
    Route::get('index','Admin\Employees\IndexController@index')->name('manage.employees.index');
    Route::get('unassign-role/{user_id}/{role_id}','Admin\Employees\EmployeeController@unassignRole');
    Route::get('assign-role/{user_id}/{role_id}','Admin\Employees\EmployeeController@assignRole');
    Route::post('update-password','Admin\Employees\EmployeeController@changePassword');
});
//students routes continuation
Route::prefix('students')->group(function (){
    Route::post('/search','Admin\StudentController@searchStudent');
    Route::post('/update-password','Admin\MiscController@updateStudentPassword');
    Route::get('/unassign-guardian/{id}','Admin\MiscController@unassignGuardian');
});

//settings controllers
Route::prefix('configuration')->group(function () {
    Route::get('','Index\IndexController@adminSettings')->name('admin.configuration.index');
    Route::resources([
        'general_settings'=>'Admin\Settings\GeneralSettingsController',
        'institution_settings'=>'Admin\Settings\InstitutionSettingsController',
        'fee-approvals'=>'Admin\Settings\FeeSettingsController'
    ]);
    Route::get('student-profile-fields','Admin\Settings\StudentSettingsController@index')
        ->name('configuraton.student.profile.field');
    Route::post('student-profile-fields','Admin\Settings\StudentSettingsController@update');
});

//endpoints
Route::prefix('admin-endpoints')->group(function (){
    Route::get('states/{id}','Admin\MiscController@getStates');
});
Route::prefix('add-fields')->group(function (){
    Route::post('guardian','Admin\MiscController@addGuardian');
    Route::post('student','Admin\MiscController@addStudent');
    Route::post('search-guardian','Admin\MiscController@searchGuardian');
    Route::post('update-guardian','Admin\MiscController@getGuardian');
});


//report controllers
Route::get('/reports-attendance','Admin\ReportController@teacherAttendance')->name('admin.reports.attendance');
Route::get('/reports-marks','Admin\ReportController@teacherMarks')->name('admin.reports.marks');

//details
Route::get('/school-details','Admin\DetailController@index')->name('admin.details.index');
Route::get('/school-details/items','Admin\DetailController@itemsIndex')->name('admin.details.items');
Route::post('/school-details/items/save','Admin\DetailController@saveItem')->name('admin.details.item.save');
Route::get('/school-details/academic-years','Admin\DetailController@academicIndex')->name('admin.details.academic');
Route::post('/school-details/academic-years/save','Admin\DetailController@saveAcademicYear')->name('admin.details.academic.save');
Route::delete('/school-details/academic-years/delete/{id}','Admin\DetailController@deleteAcademicYear');
//terms routes
Route::get('/school-details/terms','Admin\DetailController@showTerms')->name('admin.details.terms');
Route::post('/school-details/terms/save','Admin\DetailController@saveTerms')->name('admin.details.terms.save');

//more items
Route::delete('/school-details/items/delete/{id}','Admin\DetailController@deleteItem');
Route::put('/school-details/items/update/{id}','Admin\DetailController@updateItem');

//fee details
Route::get('/school-details/fees','Admin\DetailController@showFees')->name('admin.details.fees');
Route::post('/school-details/fees/save','Admin\DetailController@saveFees')->name('admin.details.fees.save');
Route::delete('/school-details/fees/delete/{id}','Admin\DetailController@deleteBreakDownItem');
//filter fees
Route::post('/school-details/fees/filter','Admin\DetailController@filterFees');


///edit years
Route::get('/school-details/terms/{id}','Admin\DetailController@showEditAcademic')->name('admin.details.term.show');
Route::post('/school-details/terms/update','Admin\DetailController@updateEditAcademic')->name('admin.details.term.update');

//edit terms
Route::get('/school-details/terms-year/{id}','Admin\DetailController@showEditTerm')->name('admin.details.term.year.show');
Route::post('/school-details/terms-year/update','Admin\DetailController@updateEditTerm')->name('admin.details.term.year.update');
Route::delete('/school-details/terms-year/delete/{id}','Admin\DetailController@deleteTerm')
    ->name('admin.details.terms.delete');
Route::group(['prefix'=>'school-details'],function (){

    Route::get('all-guardians','Admin\DetailController@getAllGuardians')->name('admin.get.all.guardians');
    Route::put('all-guardians-edit/{id}','Admin\DetailController@updateGuardian');
    Route::post('all-guardians-delete','Admin\DetailController@deleteGuardian');
});
//sms routes
Route::get('sms/parents','Admin\SMSController@showParent')->name('admin.sms.parent');

//profile
Route::get('/profile','Admin\ProfileController@showProfile')->name('admin.profile.show');
Route::post('/profile','Admin\ProfileController@updateProfile')->name('admin.profile.update');
Route::get('/passwords','Admin\ProfileController@showPassword')->name('admin.passwords.show');
Route::post('/passwords','Admin\ProfileController@updatePassword')->name('admin.passwords.update');

//receipts
Route::get('/misc/receipt/print','Admin\ReceiptController@printStudentReceipt')->name('admin.student.receipt.print');

//manage unmanaged indexes
Route::get('manage-schedule','Index\IndexController@getScheduleIndex')->name('manage.schedule.home');
Route::get('manage-courses','Index\IndexController@getManageCourseIndex')->name('manage.courses.home');
Route::get('manage-students','Index\IndexController@getManageStudentIndex')->name('manage.students.home');
Route::get('manage-classes','Index\IndexController@getManageClassesIndex')->name('manage.classes.home');
Route::get('manage-all-fees','Index\IndexController@getManageFees')->name('manage.fees.home');