<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToReportsTableAndConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->string('school_id');
            $table->double('exam_score')->default(0);
            $table->double('class_score')->default(0);
            $table->double('total')->default(0);
            $table->unsignedInteger('teacher_id');
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('academic_year_id');
            $table->unsignedInteger('term_id');
            $table->unique(['school_id','class_id','subject_id','academic_year_id','term_id','student_id'],'all_unique_constraints_q');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->dropUnique('all_unique_constraints_q');
            $table->dropColumn('school_id');
            $table->dropColumn('exam_score');
            $table->dropColumn('class_score');
            $table->dropColumn('total');
            $table->dropColumn('teacher_id');
            $table->dropColumn('class_id');
            $table->dropColumn('student_id');
            $table->dropColumn('subject_id');
            $table->dropColumn('academic_year_id');
            $table->dropColumn('term_id');
        });
    }
}
