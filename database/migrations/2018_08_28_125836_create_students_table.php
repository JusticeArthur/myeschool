<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('lname');
            $table->date('admission_date');
            $table->string('school_id');
            $table->string('email')->nullable();
            $table->string('username');
            $table->date('birth_date')->nullable();
            $table->enum('gender',['M','F']);
            $table->string('phone')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('class_id');
            $table->string('ref_no');
            $table->string('religion')->nullable();
            $table->unique(['school_id','ref_no','username']);
            $table->unsignedInteger('student_category_id')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('genotype')->nullable();
            $table->longText('health_info')->nullable();
            $table->string('nationality')->nullable();
            $table->string('state_id')->nullable();
            $table->string('country_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
