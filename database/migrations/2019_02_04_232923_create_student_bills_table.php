<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('term_id');
            $table->unsignedInteger('academic_year_id');
            $table->unsignedInteger('class_id');
            $table->double('amount');
            $table->unique(['school_id','term_id','academic_year_id','class_id'],'all_unique_constraints');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_bills');
    }
}
