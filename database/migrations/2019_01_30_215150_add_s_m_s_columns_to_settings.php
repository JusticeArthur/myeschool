<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSMSColumnsToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('institution_settings', function (Blueprint $table) {
            $table->string('sms_username');
            $table->string('sms_password');
            $table->string('sms_display_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('institution_settings', function (Blueprint $table) {
            $table->dropColumn('sms_username');
            $table->dropColumn('sms_password');
            $table->dropColumn('sms_display_name');
        });
    }
}
