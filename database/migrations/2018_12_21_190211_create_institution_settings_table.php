<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institution_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('settlement_name')->nullable();
            $table->string('slogan')->nullable();
            $table->string('currency')->nullable();
            $table->string('country')->nullable();
            $table->string('logo')->nullable();
            $table->string('address')->nullable();
            $table->string('name')->nullable();
            $table->string('website')->nullable();
            $table->string('abbr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institution_settings');
    }
}
