<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTermColumnToAttendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendances', function (Blueprint $table) {
            $table->dropUnique(['school_id','date','student_id']);
            $table->unsignedInteger('academic_year_id');
            $table->unsignedInteger('term_id');
            $table->unique(['school_id','date','class_id','academic_year_id','term_id','student_id'],'all_unique_constraints');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendances', function (Blueprint $table) {
            $table->dropUnique('all_unique_constraints');
            $table->dropColumn('academic_year_id');
            $table->dropColumn('term_id');
        });
    }
}
