<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('term_id');
            $table->unsignedInteger('academic_year_id');
            $table->unsignedInteger('class_id');
            $table->longText('teacher_comment')->nullable();
            $table->unsignedInteger('teacher_id')->default(0);
            $table->longText('head_teacher_comment')->nullable();
            $table->unsignedInteger('head_teacher_id')->default(0);
            $table->unique(['school_id','academic_year_id','term_id','student_id','class_id'],'all_unique_contstraints');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_comments');
    }
}
