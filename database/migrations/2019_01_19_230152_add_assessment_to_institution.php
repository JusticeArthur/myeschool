<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssessmentToInstitution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('institution_settings', function (Blueprint $table) {
            $table->integer('examination')->default(70);
            $table->integer('assessment')->default(30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('institution_settings', function (Blueprint $table) {
            $table->dropColumn('examination');
            $table->dropColumn('assessment');
        });
    }
}
