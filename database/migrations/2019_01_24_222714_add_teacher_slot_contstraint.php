<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTeacherSlotContstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timetables', function (Blueprint $table) {
            $table->dropUnique(['school_id','class_id','day','slot_id']);
            $table->unique(['school_id','class_id','day','slot_id','academic_year_id','term_id'],'teacher_daily');
            $table->unique(['school_id','slot_id','teacher_id','day','academic_year_id','term_id'],'unique_teacher_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timetables', function (Blueprint $table) {
            $table->dropUnique('unique_teacher_day');
            $table->dropUnique('teacher_daily');
        });
    }
}
