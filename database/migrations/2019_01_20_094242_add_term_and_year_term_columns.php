<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTermAndYearTermColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class_courses', function (Blueprint $table) {
            $table->unsignedInteger('academic_year_id')->default(1);
            $table->unsignedInteger('term_id')->default(1);
            $table->dropUnique(['course_id','class_id','teacher_id','school_id']);
            $table->unique(['course_id','class_id','teacher_id','school_id','academic_year_id','term_id'],'unique_data_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class_courses', function (Blueprint $table) {
            $table->dropColumn('academic_year_id');
            $table->dropColumn('term_id');
            $table->dropUnique('unique_data_ids');
        });
    }
}
