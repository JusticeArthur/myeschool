<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->string('slug');
            $table->date('date');
            $table->string('transaction_no')->nullable();
            $table->unsignedInteger('payment_method_id');
            $table->double('amount');
            $table->unsignedInteger('user_id');
            $table->enum('category',['income','expense']);
            $table->unsignedInteger('term_id');
            $table->unsignedInteger('academic_year_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
