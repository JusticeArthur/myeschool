<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->unsignedInteger('user_id');
            $table->string('receipt_no');
            $table->unsignedInteger('term_id');
            $table->unsignedInteger('academic_year_id');
            $table->double('amount');
            $table->double('owing');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_receipts');
    }
}
