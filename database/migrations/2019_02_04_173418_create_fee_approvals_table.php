<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('academic_year_id');
            $table->unsignedInteger('term_id');
            $table->boolean('approved')->default(false);
            $table->unique(['school_id','class_id','academic_year_id','term_id'],'all_unique_contstraints');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_approvals');
    }
}
