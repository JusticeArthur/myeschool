<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_id');
            $table->unsignedInteger('expense_category_id');
            $table->double('amount');
            $table->date('date');
            $table->integer('quantity');
            $table->double('total');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('term_id');
            $table->unsignedInteger('academic_year_id');
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
