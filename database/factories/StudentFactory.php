<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Student::class, function (Faker $faker) {
    $data['lname'] = $faker->lastName;
    $data['mname'] = $faker->lastName;
    $data['fname'] = $faker->firstName;
    $data['admission_date'] = Carbon::now();
    $data['birth_date'] = '1996-03-26';
    $data['gender'] = 'M';
    $data['phone'] = '0243742099';
    $data['class_id'] = 12;
    $data['ref_no'] = random_int(4,6);
    $data['religion'] = 'Islam';
    $data['student_category_id'] = 1;
    $data['blood_group'] = 'A+';
    $data['genotype'] = 'AA';
    $data['health_info'] = 'not now';
    $data['country_id'] = 69;
    $data['state_id'] = 1025;
    $data['school_id'] = 'a039bca0-41c8-11e9-8288-937348634357';
    $data['city'] = 'Accra';
    $data['username'] = str_slug($data['fname']) .str_slug($data['lname']) . str_random(8);
    $data['password'] = bcrypt('password');
    return $data;
});
