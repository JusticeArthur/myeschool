<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class UserSlug extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();
        foreach ($users as $user){
            \App\User::find($user->id)
                ->update([
                    'slug'=>str_replace('-','',(string) Uuid::generate())
                ]);
        }

    }
}
