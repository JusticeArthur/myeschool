<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student_fields = ['admission_no','admission_date','lastname','firstname','middlename','photo','gender',
            'date_of_birth','email','contact','address','student_category','religion','nationality','lga_origin',
            'blood_group','genotype','health_info','guardian_info'];
        $guardian_fields = ['lastname','firstname','middlename','photo','title','email','gender','contact','address'];
        $types = ['student','guardian'];
        if(\App\Models\ProfileField::count() <= 0){
            foreach ($student_fields as $field){
                DB::table('profile_fields')->insert([
                    'name'=>$field,
                    'type'=>'student',
                    'display'=>title_case(str_replace('_', ' ', $field))

                ]);
            }
            foreach ($guardian_fields as $field){
                DB::table('profile_fields')->insert([
                    'name'=>$field,
                    'type'=>'guardian',
                    'display'=>title_case(str_replace('_', ' ', $field))
                ]);
            }
        }

    }
}
