<?php

use Illuminate\Database\Seeder;

class CountyStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = file_get_contents(public_path('files/countrystates.json'));
        $json = json_decode($data);
        if(\App\Models\Country::count() <= 0){
            foreach($json as $key=>$item){
                foreach ($item as $full){
                    $country = new \App\Models\Country();
                    $country->name = $full->country;
                    $country->save();
                    foreach ($full->states as $state){
                        $new_state = new \App\Models\State();
                        $new_state->country_id = $country->id;
                        $new_state->name = $state;
                        $new_state->save();
                    }
                }
            }
        }
    }
}
